<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FaqCategory extends Model
{
    public function faqs()
    {
        return $this->hasMany('App\Models\Faq',"faq_category_id");
    }

    public function faqsFrontendActive()
    {
        return $this->hasMany('App\Models\Faq',"faq_category_id")->where('active',1);
    }
}
