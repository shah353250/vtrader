<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    public function faqCategory()
    {
        return $this->belongsTo("App\Models\FaqCategory","faq_category_id")->where('active',1);
    }
}
