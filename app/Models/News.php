<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $fillable = ['title','description','image_name','active','created_by','updated_by','show_home'];
    protected $table = 'news';

    public function getImagePathAttribute()
    {
        return asset("/images/news/{$this->image_name}");
    }
}
