<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\FaqCategory;
use App\Models\FrontPage;
use App\Models\News;
use App\Models\ContactUs;
use App\Mail\ContactUsEmail;
use App\Mail\ContactUsResponse;
use GuzzleHttp\Cookie\CookieJar;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Response;
use Illuminate\Http\Client\ResponseSequence;
use Illuminate\Support\Facades\Cookie;

class HomeController extends Controller
{

    

    public function index(){
        $news = News::latest()->where('active',1)->limit(3)->get();

        $home  = FrontPage::where('slug','home')->first();
        if($home != null)
        {
            $content                = json_decode($home->content);
            $why_choose_us_detail   = @$content->why_choose_us_detail;
            $last_section           = @$content->last_section;
        }
        else{
            $content                = null;
            $why_choose_us_detail   = null;
            $last_section           = null;
        }

        return view('frontend.home.index',compact('news','home','content','why_choose_us_detail','last_section'));
    }
    public function market(){
        return view('frontend.home.market');
    }
    public function aboutUs(){
        $FrontPage  = FrontPage::where('slug','about-us')->first();
        if($FrontPage != null)
            $content    = json_decode($FrontPage->content);
        else
            $content = null;
        return view('frontend.home.aboutus',compact('FrontPage','content'));
    }
    public function pricing(){
        return view('frontend.home.pricing');
    }
    public function termsAndServices(){
        $termsAndServices  = FrontPage::where('slug','term_and_services')->first();
        if($termsAndServices != null){
            $content    = json_decode($termsAndServices->content);
        }
        else{
            $content = null;
        }
        return view('frontend.home.termsandservices',compact('termsAndServices','content'));
    }
    public function faq(){
        $data = FaqCategory::with(['faqsFrontendActive'])->where('active',1)->orderBy('id', 'ASC')->get();
        $FrontPage = FrontPage::where('slug','faq')->first();

        return view('frontend.home.faq')->with(['title' => 'Faq', 'data' => $data , 'FrontPage' => $FrontPage ]);
    }
    public function privacypolicy(){

        $privacyPolicy  = FrontPage::where('slug','privacy_policy')->first();
        if($privacyPolicy != null){
            $content    = json_decode($privacyPolicy->content);
        }
        else{
            $content = null;
        }
        return view('frontend.home.privacypolicy',compact('privacyPolicy','content'));
    }
    public function antimoney(){

        $antimoney  = FrontPage::where('slug','Anti_Money_Laundring')->first();
        if($antimoney != null){
            $content    = json_decode($antimoney->content);
        }
        else{
            $content = null;
        }
        return view('frontend.home.antimoney',compact('antimoney','content'));
    }
    public function profile(){
        return view('frontend.home.profile');
    }
    public function login(){
        return view('frontend.home.login');
    }
    public function signup(){
        return view('frontend.home.signup');
    }
    public function contact(){
        $FrontPage = FrontPage::where('slug','contact-us')->first();

        return view('frontend.home.contact',compact('FrontPage'));
    }

    public function AjaxCallForContactUsSubmitAndEmail(Request $request)
    {
        try {
            $ContactUs = new ContactUs;
            $ContactUs->name = $request->name;
            $ContactUs->email = $request->email;
            $ContactUs->message = $request->message;
            $ContactUs->save();

            Mail::to(env('CONTACT_US_MAIL'))->send(new ContactUsEmail($ContactUs));

            Mail::to($request->email)->send(new ContactUsResponse($ContactUs)); // contact us response mail


            return response()->json(["status"=>true,"message"=>"Your Request is successfully send."]);

        } catch (\Throwable $th) {
            return response()->json(["status"=>false,"message"=> "Woops! There is some issue please try it again later."]);
        }

    }

    public function AjaxCallForSaveThemeInCookie(Request $request)
    {
        $response = new Response();
        $response->withCookie(cookie()->forever('theme', $request->theme));
        // Cookie::queue('theme', $request->theme, time() + (86400 * 30));
        
        // dd(Cookie::queue('theme', $request->theme, time() + (86400 * 30)));
        return $response;
    }

    public function AjaxCallForGetThemeInCookie(Request $request)
    {
        $value = $request->cookie('theme');
        return $value;
    }
}
