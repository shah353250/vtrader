<?php

namespace App\Http\Controllers\Admin\CMS;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\News;
use Str;
use Auth;
use File;
use Image;
class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::all();
        return view("admin.cms.news.index",compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.cms.news.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'news_title'=>'required',
            'image' => 'required|image|max:2048',
            'news_description'=>'required'
        ]);
        $imageName = Str::random(5).'_'.time().'.'.$request->image->extension();

        try {
            if($request->hasFile('image'))
            {
                Image::make($request->image)->resize(800, 600, function ($constraint) {
                })->save(public_path('/images/news').'/'.$imageName);
                // $request->image->move(public_path('/images/news'), );
            }
            News::create([
                'title' => $request->news_title,
                'description' => $request->news_description,
                'image_name' => $imageName,
                'active' => $request->has('active')?1:0,
                'show_home' => 0,
                'created_by' => Auth::user()->id,
                'updated_by' => Auth::user()->id
            ]);
            return redirect()->route('news.index')->with('success','News Added Successfully.');
        } catch (\Throwable $th) {
            return redirect()->route('news.create')->with('error',$th);
            
        }

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $news = News::findOrFail($id);
        return view("admin.cms.news.edit",compact('news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $news = News::findOrFail($id);
        $request->validate([
            'news_title'=>'required',
            'image' => 'nullable|image|max:2048',
            'news_description'=>'required'
        ]);

        try {
                $imageName = $news->image_name;
            if($request->hasFile('image'))
            {
                $imageName = Str::random(5).'_'.time().'.'.$request->image->extension();
                Image::make($request->image)->resize(800, 600, function ($constraint) {
                })->save(public_path('/images/news').'/'.$imageName);
                File::delete(public_path('/images/news').'/'.$news->image_name);
            }
            $news->update([
                'title' => $request->news_title,
                'description' => $request->news_description,
                'image_name' => $imageName,
                'active' => $request->has('active')?1:0,
                'show_home' => 0,
                'updated_by' => Auth::user()->id
            ]);
            return redirect()->route('news.index')->with('success','News Updated Successfully.');
        } catch (\Throwable $th) {
            return redirect()->route('news.edit',$id)->with('error',$th);
            
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
