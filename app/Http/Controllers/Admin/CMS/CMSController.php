<?php

namespace App\Http\Controllers\Admin\CMS;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\ContactUs;
use App\Models\FrontPage;
use Auth;

class CMSController extends Controller
{
    public function AboutUs()
    {
        $FrontPage  = FrontPage::where('slug','about-us')->first();
        if($FrontPage != null)
            $content    = json_decode($FrontPage->content);
        else
            $content = null;
        return view('admin.cms.aboutUs.create',compact('FrontPage','content'));
    }

    public function AboutUsCreateOrUpdate(Request $request)
    {
        $request->validate([
            'title'                => 'required',
            'section1_title'            => 'required',
            'section1_description'      => 'required',
            'section2_title'            => 'required',
            'section2_description'      => 'required',
            'section3_title'            => 'required',
            'section3_description'      => 'required',
        ],[
            'title.required'               => 'This field is required',
            'section1_title.required'           => 'This field is required',
            'section1_description.required'     => 'This field is required',
            'section2_title.required'           => 'This field is required',
            'section2_description.required'     => 'This field is required',
            'section3_title.required'           => 'This field is required',
            'section3_description.required'     => 'This field is required',
        ]);

        $temp = array(
            'section1_title'           => $request->section1_title,
            'section1_description'     => $request->section1_description,
            'section2_title'           => $request->section2_title,
            'section2_description'     => $request->section2_description,
            'section3_title'           => $request->section3_title,
            'section3_description'     => $request->section3_description,
        );

        try {
            $page_detail    = json_encode($temp);
            $aboutUs        = FrontPage::where('slug','about-us')->first();

            
            $FrontPage                      = ($aboutUs != null)? FrontPage::find($aboutUs->id):new FrontPage;
            $FrontPage->title               = $request->title;
            $FrontPage->meta_title          = isset($request->meta_title)? $request->meta_title:'';
            $FrontPage->meta_keyword        = isset($request->meta_keyword)? $request->meta_keyword:'';
            $FrontPage->meta_description    = isset($request->meta_description)? $request->meta_description:'';
            $FrontPage->content             = $page_detail;
            $FrontPage->active              = 1;
            if($aboutUs == null)
            {
                $FrontPage->slug            = 'about-us'; 
                $FrontPage->created_by      = Auth::user()->id;
                $FrontPage->updated_by      = Auth::user()->id;

            }
            else{
                $FrontPage->updated_by      = Auth::user()->id;
            }
            $FrontPage->save();

            $messege = ($aboutUs != null)? "About Us Updated Successfully":"About Us Created Successfully";
            return redirect()->route('about-us.create')->with('success',$messege);
        } catch (\Throwable $th) {
            return redirect()->route('about-us.create')->with('error',$th);
        }
        
    }

    public function Home()
    {
        $home  = FrontPage::where('slug','home')->first();
        if($home != null)
        {
            $content                = json_decode($home->content);
            $why_choose_us_detail   = @$content->why_choose_us_detail;
            $last_section           = @$content->last_section;
        }
        else{
            $content                = null;
            $why_choose_us_detail   = null;
            $last_section           = null;
        }
        // dd((isset($why_choose_us_detail->section1_title)? $why_choose_us_detail->section1_title:''));
        return view('admin.cms.home.create',compact('home','content','why_choose_us_detail','last_section'));
    }

    public function HomeCreateOrUpdate(Request $request)
    {
        // dd($request);

        $request->validate([
            'title'                         => 'required',
            
            'main_title'                    => 'required',
            'main_description'              => 'required',
            'why_choose_us_title'           => 'required',
            'why_choose_us_description'     => 'required',
            'section1_title'                => 'required',
            'section1_description'          => 'required',
            'section2_title'                => 'required',
            'section2_description'          => 'required',
            'section3_title'                => 'required',
            'section3_description'          => 'required',

            'last_section_description'      => 'required',
            'android_link'                  => 'required',
            'ios_link'                      => 'required',
        ],[
            'title.required'                         => 'This field is required',
            'main_title.required'                    => 'This field is required',
            'main_description.required'              => 'This field is required',
            'why_choose_us_title.required'           => 'This field is required',
            'why_choose_us_description.required'     => 'This field is required',
            'section1_title.required'                => 'This field is required',
            'section1_description.required'          => 'This field is required',
            'section2_title.required'                => 'This field is required',
            'section2_description.required'          => 'This field is required',
            'section3_title.required'                => 'This field is required',
            'section3_description.required'          => 'This field is required',
            'last_section_description.required'      => 'This field is required',
            'android_link.required'                  => 'This field is required',
            'ios_link.required'                      => 'This field is required',

        ]);

        $temp = array(
            'main_title'                    => $request->main_title,
            'main_description'              => $request->main_description,
            'why_choose_us_detail'          => array(
                'why_choose_us_title'           => $request->why_choose_us_title,
                'why_choose_us_description'     => $request->why_choose_us_description,
                'section1_title'                => $request->section1_title,
                'section1_description'          => $request->section1_description,
                'section2_title'                => $request->section2_title,
                'section2_description'          => $request->section2_description,
                'section3_title'                => $request->section3_title,
                'section3_description'          => $request->section3_description,
            ),
            'last_section'                  => array(
                'last_section_description'     => $request->last_section_description,
                'android_link'                 => $request->android_link,
                'ios_link'                     => $request->ios_link,
            ),
            
        );
        // dd('bilal');

        try {
            $page_detail    = json_encode($temp);
            $home        = FrontPage::where('slug','home')->first();

            
            $FrontPage                      = ($home != null)? FrontPage::find($home->id):new FrontPage;
            $FrontPage->title               = $request->title;
            $FrontPage->meta_title          = isset($request->meta_title)? $request->meta_title:'';
            $FrontPage->meta_keyword        = isset($request->meta_keyword)? $request->meta_keyword:'';
            $FrontPage->meta_description    = isset($request->meta_description)? $request->meta_description:'';
            $FrontPage->content             = $page_detail;
            $FrontPage->active              = 1;
            if($home == null)
            {
                $FrontPage->slug            = 'home'; 
                $FrontPage->created_by      = Auth::user()->id;
                $FrontPage->updated_by      = Auth::user()->id;

            }
            else{
                $FrontPage->updated_by      = Auth::user()->id;
            }
            $FrontPage->save();

            $messege = ($home != null)? "Home Updated Successfully":"Home Created Successfully";

            return redirect()->route('home.create')->with('success',$messege);
        } catch (\Throwable $th) {
            return redirect()->route('home.create')->with('error',$th);
        }
        

    }

    public function Pricing()
    {
        return view('admin.cms.pricing.create');   
    }

    public function PricingCreateOrUpdate(Request $request)
    {

    }

    public function AntiMoneyLaundring()
    {
        $antiMoneyLaundring  = FrontPage::where('slug','Anti_Money_Laundring')->first();
        if($antiMoneyLaundring != null)
        {
            $content = json_decode($antiMoneyLaundring->content);
        }
        else{
            $content = null;
        }
        return view('admin.cms.antiMoneyLaundring.create',compact('antiMoneyLaundring','content'));
    }

    public function AntiMoneyLaundringCreateOrUpdate(Request $request)
    {
        $request->validate([
            'title'                         => 'required',
            
            'main_title'                    => 'required',
            'main_description'              => 'required',
        ],[
            'title.required'                         => 'This field is required',
            'main_title.required'                    => 'This field is required',
            'main_description.required'              => 'This field is required',
        ]);

        $temp = array(
            'main_title'                    => $request->main_title,
            'main_description'              => $request->main_description,
        );

        try {
            $page_detail    = json_encode($temp);
            $antiMoneyLaundring        = FrontPage::where('slug','Anti_Money_Laundring')->first();

            
            $FrontPage                      = ($antiMoneyLaundring != null)? FrontPage::find($antiMoneyLaundring->id):new FrontPage;
            $FrontPage->title               = $request->title;
            $FrontPage->meta_title          = isset($request->meta_title)? $request->meta_title:'';
            $FrontPage->meta_keyword        = isset($request->meta_keyword)? $request->meta_keyword:'';
            $FrontPage->meta_description    = isset($request->meta_description)? $request->meta_description:'';
            $FrontPage->content             = $page_detail;
            $FrontPage->active              = 1;
            if($antiMoneyLaundring == null)
            {
                $FrontPage->slug            = 'Anti_Money_Laundring'; 
                $FrontPage->created_by      = Auth::user()->id;
                $FrontPage->updated_by      = Auth::user()->id;

            }
            else{
                $FrontPage->updated_by      = Auth::user()->id;
            }
            $FrontPage->save();

            $messege = ($antiMoneyLaundring != null)? "Anti Money Laundring Updated Successfully":"Anti Money Laundring Created Successfully";

            return redirect()->route('antiMoneyLaundring.create')->with('success',$messege);
        } catch (\Throwable $th) {
            return redirect()->route('antiMoneyLaundring.create')->with('error',$th);
        }
        

    }

    public function PrivacyPolicy()
    {
        $privacyPolicy  = FrontPage::where('slug','privacy_policy')->first();
        if($privacyPolicy != null)
        {
            $content = json_decode($privacyPolicy->content);
        }
        else{
            $content = null;
        }
        return view('admin.cms.privacyPolicy.create',compact('privacyPolicy','content'));   
        
    }

    public function PrivacyPolicyCreateOrUpdate(Request $request)
    {
        $request->validate([
            'title'                         => 'required',
            
            'main_title'                    => 'required',
            'main_description'              => 'required',
        ],[
            'title.required'                         => 'This field is required',
            'main_title.required'                    => 'This field is required',
            'main_description.required'              => 'This field is required',
        ]);

        $temp = array(
            'main_title'                    => $request->main_title,
            'main_description'              => $request->main_description,
        );

        try {
            $page_detail    = json_encode($temp);
            $privacyPolicy        = FrontPage::where('slug','privacy_policy')->first();

            
            $FrontPage                      = ($privacyPolicy != null)? FrontPage::find($privacyPolicy->id):new FrontPage;
            $FrontPage->title               = $request->title;
            $FrontPage->meta_title          = isset($request->meta_title)? $request->meta_title:'';
            $FrontPage->meta_keyword        = isset($request->meta_keyword)? $request->meta_keyword:'';
            $FrontPage->meta_description    = isset($request->meta_description)? $request->meta_description:'';
            $FrontPage->content             = $page_detail;
            $FrontPage->active              = 1;
            if($privacyPolicy == null)
            {
                $FrontPage->slug            = 'privacy_policy'; 
                $FrontPage->created_by      = Auth::user()->id;
                $FrontPage->updated_by      = Auth::user()->id;

            }
            else{
                $FrontPage->updated_by      = Auth::user()->id;
            }
            $FrontPage->save();

            $messege = ($privacyPolicy != null)? "Privacy Policy Updated Successfully":"Privacy Policy Created Successfully";

            return redirect()->route('privacyPolicy.create')->with('success',$messege);
        } catch (\Throwable $th) {
            return redirect()->route('privacyPolicy.create')->with('error',$th);
        }
        
    }

    public function TermsAndServices()
    {
        $termsAndServices  = FrontPage::where('slug','term_and_services')->first();
        if($termsAndServices != null)
        {
            $content = json_decode($termsAndServices->content);
        }
        else{
            $content = null;
        }
        return view('admin.cms.termsAndServices.create',compact('termsAndServices','content'));
    }
    
    public function TermsAndServicesCreateOrUpdate(Request $request)
    {
        $request->validate([
            'title'                         => 'required',
            
            'main_title'                    => 'required',
            'main_description'              => 'required',
        ],[
            'title.required'                         => 'This field is required',
            'main_title.required'                    => 'This field is required',
            'main_description.required'              => 'This field is required',
        ]);

        $temp = array(
            'main_title'                    => $request->main_title,
            'main_description'              => $request->main_description,
        );

        try {
            $page_detail    = json_encode($temp);
            $termsAndServices        = FrontPage::where('slug','term_and_services')->first();

            
            $FrontPage                      = ($termsAndServices != null)? FrontPage::find($termsAndServices->id):new FrontPage;
            $FrontPage->title               = $request->title;
            $FrontPage->meta_title          = isset($request->meta_title)? $request->meta_title:'';
            $FrontPage->meta_keyword        = isset($request->meta_keyword)? $request->meta_keyword:'';
            $FrontPage->meta_description    = isset($request->meta_description)? $request->meta_description:'';
            $FrontPage->content             = $page_detail;
            $FrontPage->active              = 1;
            if($termsAndServices == null)
            {
                $FrontPage->slug            = 'term_and_services'; 
                $FrontPage->created_by      = Auth::user()->id;
                $FrontPage->updated_by      = Auth::user()->id;

            }
            else{
                $FrontPage->updated_by      = Auth::user()->id;
            }
            $FrontPage->save();

            $messege = ($termsAndServices != null)? "Terms And Services Updated Successfully":"Terms And Services Created Successfully";

            return redirect()->route('termsAndServices.create')->with('success',$messege);
        } catch (\Throwable $th) {
            return redirect()->route('termsAndServices.create')->with('error',$th);
        }
        
    }

    public function ContactUs()
    {
        $contactUs  = ContactUs::all();
        $FrontPage  = FrontPage::where('slug','contact-us')->first();
        return view('admin.cms.contactUs.index',compact('contactUs','FrontPage'));
    }

    public function AjaxCallForViewContactUs(Request $request)
    {
        $contactUs = ContactUs::find($request->id);
        return view('admin.cms.contactUs.viewModal',compact('contactUs'));
    }

    public function ContactUsDelete(Request $request)
    {
        // dd($request->all());
        ContactUs::findOrFail($request->contactUs_id)->delete();
        // dd("ahmed");

        return redirect()->route('contactUs.index')->with('success','Contact Us Details deleted successfully!');
    }

    public function AjaxCallForSaveFaqMetaData(Request $request)
    {
        try {
            $page_detail    = json_encode("There is no data of faq in this table.");
            $faq        = FrontPage::where('slug','faq')->first();

            
            $FrontPage                      = ($faq != null)? FrontPage::find($faq->id):new FrontPage;
            $FrontPage->title               = $request->title;
            $FrontPage->meta_title          = isset($request->meta_title)? $request->meta_title:'';
            $FrontPage->meta_keyword        = isset($request->meta_keyword)? $request->meta_keyword:'';
            $FrontPage->meta_description    = isset($request->meta_description)? $request->meta_description:'';
            $FrontPage->content             = $page_detail;
            $FrontPage->active              = 1;
            if($faq == null)
            {
                $FrontPage->slug            = 'faq'; 
                $FrontPage->created_by      = Auth::user()->id;
                $FrontPage->updated_by      = Auth::user()->id;

            }
            else{
                $FrontPage->updated_by      = Auth::user()->id;
            }
            $FrontPage->save();

            $messege = ($faq != null)? "FAQ Details Updated Successfully":"FAQ Details Created Successfully";

            return response()->json(['status' => true,'msg' => $messege]);
        } catch (\Throwable $th) {
            return response()->json(['status' => false,'msg' => $th]);

        }
    }

    public function AjaxCallForSaveContactUsMetaData(Request $request)
    {
        try {
            $page_detail    = json_encode("There is no data of contact us in this table.");
            $contactUs        = FrontPage::where('slug','contact-us')->first();

            
            $FrontPage                      = ($contactUs != null)? FrontPage::find($contactUs->id):new FrontPage;
            $FrontPage->title               = $request->title;
            $FrontPage->meta_title          = isset($request->meta_title)? $request->meta_title:'';
            $FrontPage->meta_keyword        = isset($request->meta_keyword)? $request->meta_keyword:'';
            $FrontPage->meta_description    = isset($request->meta_description)? $request->meta_description:'';
            $FrontPage->content             = $page_detail;
            $FrontPage->active              = 1;
            if($contactUs == null)
            {
                $FrontPage->slug            = 'contact-us'; 
                $FrontPage->created_by      = Auth::user()->id;
                $FrontPage->updated_by      = Auth::user()->id;

            }
            else{
                $FrontPage->updated_by      = Auth::user()->id;
            }
            $FrontPage->save();

            $messege = ($contactUs != null)? "Contact Us Details Updated Successfully":"Contact Us Details Created Successfully";

            return response()->json(['status' => true,'msg' => $messege]);
        } catch (\Throwable $th) {
            return response()->json(['status' => false,'msg' => $th]);

        }
    }
}
