<?php

namespace App\Http\Controllers\Admin\CMS;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\News;
use App\Models\Faq;
use App\Models\FaqCategory;
use App\Models\FrontPage;

use Str;
use Auth;
use File;
use Image;
class FaqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = FaqCategory::with(['faqs'])->orderBy('id', 'DESC')->get();
        $FrontPage = FrontPage::where('slug','faq')->first();
        return view("admin.cms.faq.view")->with(['title' => 'Faq', 'data' => $data , 'FrontPage' => $FrontPage ]); ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request){
    	$data = "";
    	if(isset($request->id)){
    		$data = FaqCategory::with(['faqs'])->where('id', $request->id)->first();
    	}
        return view('admin.cms.faq.add')->with(['title' => 'Faq', 'data' => $data]);
    }

    public function post(Request $request){

        if(isset($request->faq_id))
        {
            $request->validate([
                'name'          => 'required',
                "question"      => "required|array|min:1",
                "question.*"    => "required",
                "answer.*"      => "required",
            ],[
                "question.*.required"    => "Question field is required",
                "answer.*.required"      => "Answer field is required",
            ]);
        }
        else{
            $request->validate([
                'name'          => 'required|unique:faq_categories,name',
                "question"      => "required|array|min:1",
                "question.*"    => "required",
                "answer.*"      => "required",
            ],[
                "question.*.required"    => "Question field is required",
                "answer.*.required"      => "Answer field is required",
            ]);
        }

        


        try {
            if(isset($request->id) && !empty($request->id)){
                //Update
                $data = FaqCategory::where('id', $request->id)->update([
                    'name' => $request->name,
                    'active' => $request->has('active')?1:0,
                    'updated_by' => Auth::user()->id,
                ]);
                
                Faq::where('faq_category_id', $request->id)->delete();
                if(isset($request->question)){
                    $questions = $request->question;
                    for($k = 0; $k< sizeof($questions); $k++)
                    {
                        $detail_data = new Faq();
                        $detail_data->faq_category_id           = $request->id;
                        $detail_data->question           = $request->question[$k];
                        $detail_data->answer           = $request->answer[$k];
                        $detail_data->active = (isset($request->question_active[$k])?1:0);
                        $detail_data->created_by           = Auth::user()->id;
                        $detail_data->updated_by           = Auth::user()->id;
                        $detail_data->save();
                    }
                }
                return redirect()->route('faq.view')->with('success','FAQ Updated Successfully');
    
            }else{
                //Add
                $data = new FaqCategory();
                $data->name = $request->name;
                $data->active = $request->has('active')?1:0;
                $data->created_by           = Auth::user()->id;
                $data->updated_by           = Auth::user()->id;
                $data->save();
    
                if(isset($request->question)){
                    $questions = $request->question;
                    for($k = 0; $k< sizeof($questions); $k++)
                    {
                        $detail_data = new Faq();
                        $detail_data->faq_category_id           = $data->id;
                        $detail_data->question           = $request->question[$k];
                        $detail_data->answer           = $request->answer[$k];
                        $detail_data->active = (isset($request->question_active[$k])?1:0);
                        $detail_data->created_by           = Auth::user()->id;
                        $detail_data->updated_by           = Auth::user()->id;
                        $detail_data->save();
                    }
                }
                return redirect()->route('faq.view')->with('success','FAQ Added Successfully');
            }
        } catch (\Throwable $th) {
            return redirect()->route('faq.add')->with('error',$th);

        }
    }
    
    public function delete(Request $request)
    {
        try {
            Faq::where('faq_category_id', $request->id)->delete();
            FaqCategory::where('id', $request->id)->delete();
            return redirect()->route('faq.view')->with('success','FAQ Deleted Successfully');

        } catch (\Throwable $th) {
            return redirect()->route('faq.view')->with('error',$th);
            
        }
        
    }
}
