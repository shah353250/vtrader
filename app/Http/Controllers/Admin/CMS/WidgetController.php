<?php

namespace App\Http\Controllers\Admin\CMS;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Widget;
use Str;
use Auth;
use File;
use Image;
class WidgetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $widgets = Widget::all();
        return view("admin.cms.widget.index",compact('widgets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.cms.widget.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'name'=>'required',
            'minWidth'=>'required',
            'minHeight'=>'required',
        ]);
        

        try {
            
            
            widget::create([
                'name' => $request->name,
                'minWidth' => $request->minWidth,
                'minHeight' => $request->minHeight,
                'active' => $request->has('active')?1:0,
                'isresizeable' => $request->has('is_resizeable')?1:0,
                'isdragable' => $request->has('is_dragable')?1:0,
                'created_by' => Auth::user()->id,
                'updated_by' => Auth::user()->id
            ]);
            
            return redirect()->route('widget.index')->with('success','Widget Added Successfully.');
        } catch (\Throwable $th) {
            return redirect()->route('widget.create')->with('error',$th);
            
        }

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $widget = widget::findOrFail($id);
        return view("admin.cms.widget.edit",compact('widget'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $widget = widget::findOrFail($id);
        $request->validate([
            'name'=>'required',
            'minWidth'=>'required',
            'minHeight'=>'required',
        ]);

        try {
            
            $widget->update([
                'name' => $request->name,
                'minWidth' => $request->minWidth,
                'minHeight' => $request->minHeight,
                'active' => $request->has('active')?1:0,
                'isresizeable' => $request->has('is_resizeable')?1:0,
                'isdragable' => $request->has('is_dragable')?1:0,
                'updated_by' => Auth::user()->id
            ]);
            return redirect()->route('widget.index')->with('success','News Updated Successfully.');
        } catch (\Throwable $th) {
            return redirect()->route('widget.edit',$id)->with('error',$th);
            
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
