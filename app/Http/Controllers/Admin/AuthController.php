<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Hash;

use App\Admin;
use App\Mail\ForgotPasswordEmail;
use Illuminate\Support\Facades\Mail;


class AuthController extends Controller
{
    public function loginShow()
    {
        return view('admin.auth.login');
    }
    public function loginPost(Request $request)
    {
        $this->validate($request, [
            'email'   => 'required|email',
            'password' => 'required|min:6'
        ]);

        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {

            return redirect()->intended('/admin/home');
        }
        return back()->withInput($request->only('email', 'remember'))->withMessage('Invalid username or password');
    }
    public function logout(){
        Auth::logout();
        return redirect()->route('admin.login');
    }

    public function EmailResetPasswordPage()
    {
        return view('admin.auth.forgotPassword.resetEmail');
    }

    public function AjaxCallForForgotPasswordEmail(Request $request)
    {
        $to_email       = $request->email;
        $email_encode   = $to_email;
        for ($i=0; $i < 3; $i++) { 
            $email_encode = base64_encode($email_encode);
        }
        $detail = array();
        $Admin  = Admin::where('email',$to_email)->first();
        if($Admin != null)
        {
            Mail::to($to_email)->send(new ForgotPasswordEmail($email_encode));
            $detail[0] = true;
            $detail[1] = "Email is Succesfully send!";
            return $detail;
        }
        else{
            $detail[0] = false;
            $detail[1] = "Invalid Email!";
            return $detail;
        }
    }

    public function ResetPassword($email_encode)
    {
        $email_decode = $email_encode;
        for ($i=0; $i < 3; $i++) { 
            $email_decode = base64_decode($email_decode);
        }
        return view('admin.auth.forgotPassword.resetPassword',compact('email_decode'));
    }

    public function SetNewPassword(Request $request)
    {
        $request->validate([
            'new_password'              => 'min:8|required_with:confirm_new_password|same:confirm_new_password',
            'confirm_new_password'      => 'min:8',
        ],[
            'new_password.required'              => 'This field is required',
            'confirm_new_password.required'      => 'This field is required',
        ]);

        try {
            
            $Admin  = Admin::where('email',$request->email)->first();

            if($Admin != null)
            {
                $Admin->password = Hash::make($request->new_password);
                $Admin->save();
            }
            // $request->user()->fill([
            //     'password' => Hash::make($request->new_password)
            // ])->save();
            
        } 
        catch (\Throwable $th) {
            return redirect()->back()->with('message',$th);
        }
       
        return view('admin.auth.login')->with('message','Password is Successfully updated please login!');
        // return redirect()->back()->with('success','Password Change Successfully!');
    }
}
