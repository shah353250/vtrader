<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Auth;
use Hash;

class SettitngController extends Controller
{
    public function index()
    {
        return view('admin.settingPage.index');
    }

    public function ChangePassword(Request $request)
    {
        $request->validate([
            'current_password'          => 'required',
            'new_password'              => 'min:8|required_with:confirm_new_password|same:confirm_new_password',
            'confirm_new_password'      => 'min:8',
        ],[
            'current_password.required'          => 'This field is required',
            'new_password.required'              => 'This field is required',
            'confirm_new_password.required'      => 'This field is required',
        ]);

        try {
            if(Hash::check($request->current_password,Auth::user()->password))
            {
                $request->user()->fill([
                    'password' => Hash::make($request->new_password)
                ])->save();
            }
            else{
                return redirect()->back()->with('error','Current Password is Incorrect!');
            }
        } 
        catch (\Throwable $th) {
            return redirect()->back()->with('error',$th);
        }
       

        return redirect()->back()->with('success','Password Change Successfully!');

    }
}
