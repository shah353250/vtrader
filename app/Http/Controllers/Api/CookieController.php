<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CookieController extends Controller
{
    public function GetTheme()
    {
        try {
            $cookie_name = "theme";

            $data = array(
                'cookie_name'   => $cookie_name,
                'cookie_value'  => $_COOKIE[$cookie_name],
            );

        } 
        catch (\Throwable $th) 
        {
            $data = "Cookie not created!";
            return response()->json(["status" => false,"data"=> $data]);
        }

        return response()->json(["status" => true,"data"=> $data]);

    }
}
