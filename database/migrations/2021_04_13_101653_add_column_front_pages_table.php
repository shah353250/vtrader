<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnFrontPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('front_pages', function (Blueprint $table)
        {
           $table->string('meta_title')->after('slug')->nullable();
           $table->string('meta_keyword')->after('meta_title')->nullable();
        //    $table->renameColumn('description','meta_description');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('front_pages',function(Blueprint $table)
        {
            $table->dropColumn('meta_title');
            $table->dropColumn('meta_keyword');
        //    $table->renameColumn('meta_description','description');

        });
    }
}
