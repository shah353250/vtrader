<?php
    return [
        "app_login" => env('TRADE_APP_URL')."/login",
        "app_register" => env('TRADE_APP_URL')."/register",
        "app_trade" => env('TRADE_APP_URL')."/customizeBoard",
        
    ];
?>