@extends('admin.layouts.app')

@push('custom-css')
{{-- <link rel="stylesheet" href="{{asset('backend/plugins/summernote/summernote-bs4.min.css')}}"> --}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.15.5/sweetalert2.min.css"
    integrity="sha512-gX6K9e/4ewXjtn8Q/oePzgIxs2KPrksR4S2NNMYLxenvF7n7eNon9XbqQxb+5jcqYBVCcncIxqF6fXJYgQtoAg=="
    crossorigin="anonymous" />

@endpush

@section('content')


<div class="content-wrapper">
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <form id="quickForm" method="POST" action="{{route('ChangePassword')}}">
                        @csrf
                        <div class="card card-primary mt-4">
                            <div class="card-header">
                                <h3 class="card-title">Change Password</h3>
                            </div>

                            

                            @if (Session::has('success'))
                            <div class="alert alert-success alert-dismissible m-2">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h5><i class="icon fas fa-check"></i> Success!</h5>
                                {{Session::get('success')}}
                            </div>
                            @elseif (Session::has('error'))
                            <div class="alert alert-danger alert-dismissible m-2">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h5><i class="icon fas fa-ban"></i> Error!</h5>
                                {{Session::get('error')}}
                              </div>
                            @endif
                            

                            <div class="card-body row">



                                <div class="form-group col-md-12">
                                    <label for="">Current Password <span style="color: red">*</span></label>
                                    <input type="password" name="current_password"
                                        class="form-control  @error('current_password') is-invalid @enderror" value="">
                                    @error('current_password')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="">New Password <span style="color: red">*</span></label>
                                    <input type="password" name="new_password"
                                        class="form-control  @error('new_password') is-invalid @enderror" value="">
                                    @error('new_password')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>

                                <div class="form-group col-md-12">
                                    <label for="">Confirm New Password <span style="color: red">*</span></label>
                                    <input type="password" name="confirm_new_password"
                                        class="form-control  @error('confirm_new_password') is-invalid @enderror"
                                        value="">
                                    @error('confirm_new_password')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>

                            </div>



                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary" id="submit_btn">Submit</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </section>
</div>
@endsection





@push('custom-script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.15.5/sweetalert2.min.js"
    integrity="sha512-+uGHdpCaEymD6EqvUR4H/PBuwqm3JTZmRh3gT0Lq52VGDAlywdXPBEiLiZUg6D1ViLonuNSUFdbL2tH9djAP8g=="
    crossorigin="anonymous"></script>
<script>
    $('#submit_btn').click(function (e) {
        e.preventDefault();

        Swal.fire({
            title: 'Do you want to save the changes?',
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: `Save`,
            denyButtonText: `Don't save`,
        }).then((result) => {
            if (result.isConfirmed) {
                $('#quickForm').submit();
            } else if (result.isDenied) {
                Swal.fire('Changes are not saved', '', 'info')
            }
        })
    })

</script>
@endpush
