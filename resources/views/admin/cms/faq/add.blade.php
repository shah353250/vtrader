@extends('admin.layouts.app')
@push('custom-css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.15.5/sweetalert2.min.css"
    integrity="sha512-gX6K9e/4ewXjtn8Q/oePzgIxs2KPrksR4S2NNMYLxenvF7n7eNon9XbqQxb+5jcqYBVCcncIxqF6fXJYgQtoAg=="
    crossorigin="anonymous" />
@endpush

@section('content')
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary mt-4">
                        <div class="card-header">
                            <h3 class="card-title">Add {{ @$title}}</h3>
                        </div>
                        @if (Session::has('success'))
                        <div class="alert alert-info alert-dismissible m-3">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h5><i class="icon fas fa-info"></i> Success!</h5>
                            {{Session::get('success')}}
                        </div>
                        @elseif (Session::has('error'))
                        <div class="alert alert-danger alert-dismissible m-3">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h5><i class="icon fas fa-info"></i> Error!</h5>
                            {{Session::get('error')}}
                        </div>
                        @endif
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form id="quickForm" action="{{route('faq.post')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            @if ($data != "")
                                <input type="hidden" name="faq_id" value="{{$data->id}}">
                            @endif
                            <div class="card-body row">
                                <div class="form-group col-md-6">
                                    <label for="name">Name</label>
                                    <input type="text" name="name" class="form-control" value="{{@$data->name}}"
                                        id="name" required>
                                    @error('name')
                                    <p class="text-danger text-sm">{{$message}}</p>
                                    @enderror
                                </div>

                                <div class="form-check col-12">
                                    <input type="checkbox" class="" value="1" name="active" id="active" checked>
                                    <label class="" for="active">Is Active</label>
                                </div>
                                <div class="col-12">


                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Question</th>
                                                <th>Answer</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>

                                        <tbody id="question_body">
                                            <tr class="txtMult">
                                                <td class="text-center"><a href="javascript:void(0);"
                                                        class="add_question">Add</a></td>
                                                <td colspan="3"></td>
                                            </tr>
                                            @php($k = 1)
                                            @if (old('question') != null)
                                            @foreach(old('question') as $key => $value)
                                            <tr>
                                                <td class="text-center" style="width:60px;"><a
                                                        href="javascript:void(0);" class="remove_question">Remove</a>
                                                </td>
                                                <td><input type="text"
                                                        class="form-control @error('question.'.$key) is-invalid @enderror"
                                                        name="question[]" value="{{ old('question.'.$key) }}" required>
                                                    @error('question.*'.$key)
                                                    <p class="text-danger text-sm">{{$message}}</p>
                                                    @enderror
                                                </td>
                                                <td><input type="text"
                                                        class="form-control @error('answer.'.$key) is-invalid @enderror"
                                                        name="answer[]" value="{{ old('answer.'.$key) }}" required>
                                                    @error('answer.*'.$key)
                                                    <p class="text-danger text-sm">{{$message}}</p>
                                                    @enderror
                                                </td>
                                                <td><input type="checkbox" class="" value="1" name="question_active[]"
                                                        id="active{{ $k}}"
                                                        {{ (@$v->active == '1')?'checked':'' }}><label
                                                        class="form-check-label" for="active{{ $k }}">Is Active</label>
                                                </td>
                                            </tr>
                                            @php($k++)
                                            @endforeach

                                            @elseif(!empty(@$data->id) && @$data->id != null)
                                            @foreach(@$data->faqs as $v)
                                            <tr>
                                                <td class="text-center" style="width:60px;"><a
                                                        href="javascript:void(0);" class="remove_question">Remove</a>
                                                </td>
                                                <td><input type="text" class="form-control" name="question[]"
                                                        value="{{ @$v->question }}" required>
                                                </td>
                                                <td><input type="text" class="form-control" name="answer[]"
                                                        value="{{ @$v->answer }}" required></td>
                                                <td><input type="checkbox" class="" value="1" name="question_active[]"
                                                        id="active{{ $k}}"
                                                        {{ (@$v->active == '1')?'checked':'' }}><label
                                                        class="form-check-label" for="active{{ $k }}">Is Active</label>
                                                </td>
                                            </tr>
                                            @php($k++)
                                            @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <input name="id" type="hidden" value="{{ @$data->id }}">
                                <button type="submit" class="btn btn-primary" id="submit_btn">Save</button>
                                <a href="{{route('faq.view')}}" class="btn btn-default">Back</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection

@push('custom-script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/10.15.5/sweetalert2.min.js"
    integrity="sha512-+uGHdpCaEymD6EqvUR4H/PBuwqm3JTZmRh3gT0Lq52VGDAlywdXPBEiLiZUg6D1ViLonuNSUFdbL2tH9djAP8g=="
    crossorigin="anonymous"></script>
<script>
    $(document).ready(function () {
        var x = 1;
        $(document).on('click', ".add_question", function () {
            var temp = '<tr>';
            temp +=
                '<td class="text-center" style="width:60px;"><a href="javascript:void(0);" class="remove_question">Remove</a></td>';
            temp += '<td><input type="text" class="form-control" name="question[]"></td>';
            temp += '<td><input type="text" class="form-control" name="answer[]"></td>';
            temp +=
                '<td><input type="checkbox" class="" value="1" name="question_active[]" id="active' +
                x + '" checked> <label class="form-check-label" for="active' + x +
                '"> Is Active</label></td>';
            temp += '</tr>';
            $('#question_body').append(temp);
            x++;
        });

        $("#question_body").on('click', '.remove_question', function () {
            $(this).parent().parent().remove();
        });

    });

    $('#submit_btn').click(function (e) {
        e.preventDefault();

        Swal.fire({
            title: 'Do you want to save the changes?',
            showDenyButton: true,
            showCancelButton: true,
            confirmButtonText: `Save`,
            denyButtonText: `Don't save`,
        }).then((result) => {
            if (result.isConfirmed) {
                $('#quickForm').submit();
            } else if (result.isDenied) {
                Swal.fire('Changes are not saved', '', 'info')
            }
        })
    })

</script>
@endpush
