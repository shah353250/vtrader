
<div class="form-group row">
    <label for="staticEmail" class="col-sm-2 col-form-label">Name</label>
    <div class="col-sm-10">
        <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$contactUs->name}}">
    </div>
</div>

<div class="form-group row">
    <label for="staticEmail" class="col-sm-2 col-form-label">Email</label>
    <div class="col-sm-10">
        <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{{$contactUs->email}}">
    </div>
</div>

<div class="form-group row">
    <label for="staticEmail" class="col-sm-2 col-form-label">Message</label>
    <div class="col-sm-10">
        <textarea type="text" readonly class="form-control-plaintext" id="staticEmail" rows="10">{{$contactUs->message}}</textarea>
    </div>
</div>