@extends('admin.layouts.app')
@push('custom-css')
<link rel="stylesheet" href="{{asset('backend/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('backend/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
<link rel="stylesheet" href="{{asset('backend/plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">
@endpush
@section('content')
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card card-primary mt-4">
                        <div class="card-header">
                            <h3 class="card-title">View News</h3>
                        </div>
                        @if (Session::has('success'))
                        <div class="alert alert-info alert-dismissible m-3">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h5><i class="icon fas fa-info"></i> Success!</h5>
                            {{Session::get('success')}}
                        </div>
                        @elseif (Session::has('error'))
                        <div class="alert alert-danger alert-dismissible m-3">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h5><i class="icon fas fa-info"></i> Error!</h5>
                            {{Session::get('error')}}
                        </div>
                        @endif
                        <!-- /.card-header -->
                        <div class="card-body">
                            <a href="{{route('news.create')}}" class="btn btn-primary mb-3">Add News</a>
                            <table class="table table-bordered table-striped" id="data-table">
                                <thead>
                                    <tr>
                                        <th>Sr.No</th>
                                        <th>News Title</th>
                                        <th>News Description</th>
                                        <th>Image</th>
                                        <th>Active</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($news as $key => $item)
                                    <tr>
                                        <td>{{++$key}}</td>
                                        <td>{{$item->title}}</td>
                                        <td>{{$item->description}}</td>
                                        <td><img src="{{$item->image_path}}" height="60" width="auto" alt="" srcset="">
                                        </td>
                                        <td>{{$item->active == 1?'active':'deactive'}}</td>
                                        <td><a href="{{route('news.edit',$item->id)}}" class="btn btn-primary btn-sm"><i
                                                    class="fa fa-edit"></i></a></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection
@push('custom-script')
<script src="{{asset('backend/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('backend/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{asset('backend/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('backend/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<script>
    $(function () {
        $("#data-table").DataTable();
    });

</script>
@endpush
