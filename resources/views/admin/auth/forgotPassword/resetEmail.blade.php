<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>AdminLTE 3 | Log in</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('backend/plugins/fontawesome-free/css/all.min.css')}}">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{asset('backend/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('backend/dist/css/adminlte.min.css')}}">
</head>

<body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-logo">
            <a href="{{route('admin.login')}}"><b>VTrader</b></a>
        </div>
        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg">Send Email to rest your password</p>

                <form action="" method="post">
                    @if (Session::has('message'))
                    <p class="text-danger">{{Session::get('message')}}</p>
                    @endif

                    <div id="success_msg"> </div>
                    <div id="error_msg"> </div>


                    @csrf
                    <div class="input-group mb-2">
                        <input type="email" class="form-control" value="{{old('email')}}" name="email" id="email"
                            placeholder="Enter Your Email">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                    </div>
                    @error('email')
                    <p class="text-danger text-sm">{{$message}}</p>
                    @enderror

                    <div class="row" id="loader" style="display: none">
                        <div class="col-12 d-flex justify-content-center">
                            <div class="spinner-border text-info" role="status">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-12">
                            <button type="submit" class="btn btn-primary mt-3 btn-block"
                                style="background:#08c9c9;border: transparent;" id="btn_email">Send Email</button>

                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" style="text-align: center;">
                <a href="{{route('admin.login')}}" style="color:grey;font-size:20px"> Go Back!</a>

            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="{{asset('backend/plugins/jquery/jquery.min.js')}}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('backend/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('backend/dist/js/adminlte.min.js')}}"></script>

    <script>
        $('#btn_email').click(function (e) {
            e.preventDefault();
            let email = $('#email').val();
            $.ajax({
                type: "POST",
                url: "{{route('AjaxCallForForgotPasswordEmail')}}",
                data: {
                    _token: '{{ csrf_token() }}',
                    email: email,
                },
                beforeSend: function () {
                    // setInterval( function () { console.log('abc') } ,1000);
                    // loader
                    $('#loader').show();
                },
                success: function (response) {
                    // setInterval( function () { console.log('success') } ,1000);

                    // console.log('response',response);
                    if (response[0]) {
                        // success_msg
                        $('#loader').hide();
                        $('#success_msg').append(`<p class="text-success">` + response[1] + `</p>`);
                    } else {
                        $('#loader').hide();
                        // error_msg
                        $('#error_msg').append(`<p class="text-danger">` + response[1] + `</p>`);

                    }
                }

            });

        })

    </script>
</body>

</html>
