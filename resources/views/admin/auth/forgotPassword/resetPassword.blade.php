<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>AdminLTE 3 | Log in</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('backend/plugins/fontawesome-free/css/all.min.css')}}">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{asset('backend/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('backend/dist/css/adminlte.min.css')}}">
</head>

<body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-logo">
            <a href="{{route('admin.login')}}"><b>VTrader</b></a>
        </div>
        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg">Send Email to rest your password</p>

                <form action="{{route('SetNewPassword')}}" method="post">
                    @if (Session::has('message'))
                    <p class="text-danger">{{Session::get('message')}}</p>
                    @endif

                    @csrf
                    <div class="input-group mb-2">
                        <input type="email" class="form-control" value="{{$email_decode}}" name="email" id="email"
                            placeholder="Enter Your Email" readonly>
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>

                    </div>
                    <div class="input-group mb-2">
                        <input type="password" class="form-control @error('new_password') is-invalid @enderror" value="" name="new_password">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                        @error('new_password')
                            <p class="text-danger text-sm">{{$message}}</p>
                        @enderror
                    </div>
                    <div class="input-group mb-2">
                        <input type="password" class="form-control @error('confirm_new_password') is-invalid @enderror" value="" name="confirm_new_password">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                        @error('confirm_new_password')
                            <p class="text-danger text-sm">{{$message}}</p>
                        @enderror
                    </div>
                    
                    
                    <div class="row">
                        
                        <div class="col-12">
                            <button type="submit" class="btn btn-primary mt-3 btn-block" style="background:#08c9c9;border: transparent;" id="btn_email">Submit</button>
                          
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="{{asset('backend/plugins/jquery/jquery.min.js')}}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{asset('backend/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('backend/dist/js/adminlte.min.js')}}"></script>


</body>

</html>
