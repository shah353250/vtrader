 
 <!-- Main Sidebar Container -->
 <aside class="main-sidebar sidebar-dark-primary elevation-4">
     <!-- Brand Logo -->
     <a href="#" class="brand-link">
         {{-- <img src="{{asset('backend/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo"
             class="brand-image img-circle elevation-3" style="opacity: .8"> --}}
             <img src="{{asset('frontend/img/logo.png')}}" alt="V-Trader Logo"
             class="" style="opacity: .8;width:50px">
         <span class="brand-text font-weight-light">V-Trader</span>
     </a>

     <!-- Sidebar -->
     <div class="sidebar">
         <!-- Sidebar user panel (optional) -->
         <div class="user-panel mt-3 pb-3 mb-3 d-flex">
             <div class="image">
                 {{-- <img src="{{asset('backend/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2"
                     alt="User Image"> --}}
                     <div  id="profileImage" class="img-circle"></div>
             </div>
             <div class="info">
                 <a href="#" class="d-block">{{Auth::user()->name}}</a>
                 <small><i class="fas fa-cog" style="color: #c2c7d0;"></i> <a href="{{route('setting')}}">Settings</a></small>
             </div>
         </div>

         <!-- SidebarSearch Form -->
         {{-- <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div> --}}

         <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                 data-accordion="false">
                 <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
			   	<li class="nav-item">
					<a href="{{route('home')}}" class="nav-link">
						<i class="nav-icon fas fa-chart-pie"></i>
						<p>
							Dashboard
							{{-- <i class="right fas fa-angle-left"></i> --}}
						</p>
					</a>
			   	</li>
                   <li class="nav-item">
                    <a href="{{route('widget.index')}}" class="nav-link {{ ( Route::currentRouteName() == 'widget.index' || Route::currentRouteName() == 'widget.create' || Route::currentRouteName() == 'widget.edit') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-chart-pie"></i>
                        <p>Widget</p>
                    </a>
                </li>
                   {{-- {{ ( Route::currentRouteName() == 'faq.view' || Route::currentRouteName() == 'faq.add') ? 'menu-is-opening menu-open' : '' }} --}}
                   @php
                        $arr = array("termsAndServices.create", "privacyPolicy.create", "antiMoneyLaundring.create", "home.create", "about-us.create", "faq.view", "faq.add", "pricing.create", "news.edit", "news.create", "news.index","contactUs.index","widget.edit", "widget.create", "widget.index");
                   @endphp
				<li class="nav-item {{ in_array(Route::currentRouteName(),$arr)? 'menu-is-opening menu-open' : '' }}" id="CMS_dropDown">
					<a href="#" class="nav-link" data-value="CMS">
						<i class="nav-icon fas fa-chart-pie"></i>
						<p>
							CMS
							<i class="right fas fa-angle-left"></i>
						</p>
					</a>
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="{{route('home.create')}}" class="nav-link {{ ( Route::currentRouteName() == 'home.create' ) ? 'active' : '' }}" ">
								<i class="far fa-circle nav-icon"></i>
								<p>Home</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="{{route('about-us.create')}}" class="nav-link {{ ( Route::currentRouteName() == 'about-us.create' ) ? 'active' : '' }}" ">
								<i class="far fa-circle nav-icon"></i>
								<p>About Us</p>
							</a>
						</li>

						<li class="nav-item">
							<a href="{{route('news.index')}}" class="nav-link {{ ( Route::currentRouteName() == 'news.index' || Route::currentRouteName() == 'news.create' || Route::currentRouteName() == 'news.edit') ? 'active' : '' }}">
								<i class="far fa-circle nav-icon"></i>
								<p>News</p>
							</a>
						</li>

                        <li class="nav-item">
							<a href="{{route('faq.view')}}" class="nav-link {{ ( Route::currentRouteName() == 'faq.view' || Route::currentRouteName() == 'faq.add') ? 'active' : '' }}" ">
								<i class="far fa-circle nav-icon"></i>
								<p>Faq</p>
							</a>
						</li>

                        <li class="nav-item">
							<a href="{{route('pricing.create')}}" class="nav-link {{ ( Route::currentRouteName() == 'pricing.create' ) ? 'active' : '' }}">
								<i class="far fa-circle nav-icon"></i>
								<p>Pricing</p>
							</a>
						</li>

                        <li class="nav-item">
							<a href="{{route('antiMoneyLaundring.create')}}" class="nav-link {{ ( Route::currentRouteName() == 'antiMoneyLaundring.create' ) ? 'active' : '' }}">
								<i class="far fa-circle nav-icon"></i>
								<p>Anti Money Laundering</p>
							</a>
						</li>

                        <li class="nav-item">
							<a href="{{route('privacyPolicy.create')}}" class="nav-link {{ ( Route::currentRouteName() == 'privacyPolicy.create' ) ? 'active' : '' }}" >
								<i class="far fa-circle nav-icon"></i>
								<p>Privacy Policy</p>
							</a>
						</li>

                        <li class="nav-item">
							<a href="{{route('termsAndServices.create')}}" class="nav-link {{ ( Route::currentRouteName() == 'termsAndServices.create' ) ? 'active' : '' }}">
								<i class="far fa-circle nav-icon"></i>
								<p>Terms of Service</p>
							</a>
						</li>

                        

                        <li class="nav-item">
							<a href="{{route('contactUs.index')}}" class="nav-link {{ ( Route::currentRouteName() == 'contactUs.index' ) ? 'active' : '' }}">
								<i class="far fa-circle nav-icon"></i>
								<p>Contact Us</p>
							</a>
						</li>
						
					</ul>
				</li>
                 {{-- <li class="nav-item">
                     <a href="#" class="nav-link">
                         <i class="nav-icon fas fa-chart-pie"></i>
                         <p>
                             Charts
                             <i class="right fas fa-angle-left"></i>
                         </p>
                     </a>
                     <ul class="nav nav-treeview">
                         <li class="nav-item">
                             <a href="pages/charts/chartjs.html" class="nav-link">
                                 <i class="far fa-circle nav-icon"></i>
                                 <p>ChartJS</p>
                             </a>
                         </li>
                         <li class="nav-item">
                             <a href="pages/charts/flot.html" class="nav-link">
                                 <i class="far fa-circle nav-icon"></i>
                                 <p>Flot</p>
                             </a>
                         </li>
                         <li class="nav-item">
                             <a href="pages/charts/inline.html" class="nav-link">
                                 <i class="far fa-circle nav-icon"></i>
                                 <p>Inline</p>
                             </a>
                         </li>
                         <li class="nav-item">
                             <a href="pages/charts/uplot.html" class="nav-link">
                                 <i class="far fa-circle nav-icon"></i>
                                 <p>uPlot</p>
                             </a>
                         </li>
                     </ul>
                 </li>
                 <li class="nav-item">
                     <a href="#" class="nav-link">
                         <i class="nav-icon fas fa-tree"></i>
                         <p>
                             UI Elements
                             <i class="fas fa-angle-left right"></i>
                         </p>
                     </a>
                     <ul class="nav nav-treeview">
                         <li class="nav-item">
                             <a href="pages/UI/general.html" class="nav-link">
                                 <i class="far fa-circle nav-icon"></i>
                                 <p>General</p>
                             </a>
                         </li>
                         <li class="nav-item">
                             <a href="pages/UI/icons.html" class="nav-link">
                                 <i class="far fa-circle nav-icon"></i>
                                 <p>Icons</p>
                             </a>
                         </li>
                     </ul>
                 </li> --}}
             </ul>
         </nav>
         <!-- /.sidebar-menu -->
     </div>
     <!-- /.sidebar -->
 </aside>

 @push('custom-script')
    <script>
         $(document).ready(function () {
            
            var name = '<?= Auth::user()->name ?>';
            var intials = name.charAt(0).toUpperCase(); 
            
            var profileImage = $('#profileImage').text(intials);

        })
    </script>
 @endpush