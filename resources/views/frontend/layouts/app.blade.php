<!DOCTYPE html>
<html>
	<head><meta charset="windows-1252">
	  <meta name="viewport" content="width=device-width, initial-scale=1">
	  <link href="{{asset('frontend/css/4.1.1/bootstrap.min.css')}}" rel="stylesheet" id="bootstrap-css">
	  <link rel="stylesheet" href="{{asset('frontend/font-awesome/css/font-awesome.min.css')}}">
	  <link href="{{asset('frontend/css/custom.css')}}" rel="stylesheet">
	  <link href="{{asset('frontend/css/dynamic-combine.css')}}" rel="stylesheet">
	  <link href="{{asset('frontend/css/toggle-btn.css')}}" rel="stylesheet">
	  @stack('styles')
	  <title>VTrader | {{ (isset($title)? @$title:'') }}</title>
	  <link rel="shortcut icon" href="{{asset('frontend/img/favicon.png')}}" type="image/png">
	  
	</head>
	@php
		// dd(Cookie::get('theme'));
		\Cookie::forget('theme');
	@endphp
	<body class="{{Cookie::get('theme')}}">
		@yield('content')
		<script src="{{asset('frontend/js/3.2.1/jquery.min.js')}}"></script> 
	  	<script src="{{asset('frontend/js/4.5.2/bootstrap.min.js')}}"></script>
		<script src="{{asset('frontend/js/custom.js')}}"></script>
	   <script src="{{asset('frontend/js/custom_jquery.js')}}"></script>
	   {{-- <script src="{{asset('frontend/js/themeAjax.js')}}"></script> --}}

	   @stack('scripts')


	   <script>


		   $(document).ready(function () {
				$('body').toggleClass(getCookie("theme"));

			//    $.ajax({
			// 	   type: "GET",
			// 	   url: "{{ route('AjaxCallForGetThemeInCookie')}}",
			// 	   data: { _token: "{{ csrf_token() }}"},
			// 	   success: function (response) {

			// 		$('body').toggleClass('lite-theme');
			// 		//    if(response == '')
			// 		//    {
			// 		// 		$('body').addClass('lite-theme');
			// 		//    }
			// 		//    else{
			// 		// 		$('body').toggleClass('lite-theme');
			// 		//    }
			// 	   }
			//    });
		   });

		   function getCookie(cname) {
			var name = cname + "=";
			var decodedCookie = decodeURIComponent(document.cookie);
			var ca = decodedCookie.split(';');
			for(var i = 0; i < ca.length; i++) {
				var c = ca[i];
				while (c.charAt(0) == ' ') {
				c = c.substring(1);
				}
				if (c.indexOf(name) == 0) {
				return c.substring(name.length, c.length);
				}
			}
			return "";
			}

			function setCookie(cname,cvalue,exdays) {
				var d = new Date();
				d.setTime(d.getTime() + (exdays*24*60*60*1000));
				var expires = "expires=" + d.toGMTString();
				document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
			}
		   
			$('.can-toggle__switch').on('click', function () {
				let theme = '';
				if($('body').hasClass('lite-theme'))
				{  
					theme = 'lite-theme';
					setCookie("theme", theme, 30);
				}
				else{
					theme = '';
					setCookie("theme", theme, 30);

				}
				// $.ajax({
				// 	type: "POST",
				// 	url: "{{ route('AjaxCallForSaveThemeInCookie') }}",
				// 	data: { _token: "{{ csrf_token() }}", theme:theme},
				// 	success: function (response) {
				// 		if(theme == 'lite-theme')
				// 		{
  				// 			$('body').removeClass('lite-theme');
				// 		}
				// 		else{
				// 		  	$('body').addClass('lite-theme');
				// 		}
				// 	}
				// });
			});
	   </script>
	</body>
</html>