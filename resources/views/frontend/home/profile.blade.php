@extends('frontend.layouts.app')
@push('styles')
<title>Profile</title>
<meta name="description" content="{{@$termsAndServices->description}}"> 
<link href="{{asset('frontend/css/profile-sidebar.css')}}" rel="stylesheet">

    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>
@endpush
@section('content')
<div class="container-fluid pd-0" style="background-color:#000e14;">
    
<div class="main-bg">
	<div class="wrapper">
        <!-- Sidebar Holder -->
        @include('frontend.partials.profile_sidebar')

        <!-- Page Content Holder -->
        <div id="content">

            @include('frontend.partials.profile_navbar')
            <div>
            <div class="about-profile container">
                <div class="profile-inner-row-1 row">
                    <div class="about-profile-left col-sm">
                        <h1>Alan Smith</h1>
                        <p><span>Nick Name:</span><span>Enter Username</span></p>
                        <p><span>Phone Number:</span><span>+123-456-7890</span></p>
                        <p><span>Email:</span><span>kazmi.hx@gmail.com</span></p>
                        <p><span>Total</span><span>$0.00</span></p>
                        <p><span>24 hours withdrawl limit:</span><span>1BTC</span></p>
                        <p><span>Last Login:</span><span>2400:abc:1b5c:1900:19b5</span></p>
                    </div>
                    <div class="about-profile-right col-sm">
                        <div class="row profile-pic-row">
                            <div class="col-sm">
                                <img class="user-img-responsive" src="{{asset('frontend/img/user.png')}}">
                            </div>
                            <div class="col-sm">
                                
                            </div>
                        </div>
                        <div class="profile-details-table">
                            <table class="table">
                             
                              <tbody>
                                
                                <tr>
                                  <td>Tiered fee schedule</td>
                                  <td>Regular user</td>
                                </tr>
                                <tr>    
                                  <td>Trading volume</td>
                                  <td>0.00000000 USDT</td>
                                  
                                </tr>
                                <tr>
                                  
                                  <td>Lock position</td>
                                  <td>0.00000000 DFT</td>
                                  
                                </tr>
                                <tr>
                                  <td>Maker fees</td>
                                  <td>0.15%</td> 
                                </tr>
                                <tr>
                                  <td>Taker fees</td>
                                  <td>0.15%</td> 
                                </tr>
                              </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="profile-inner-row-2 row">
                    <p>Last login: 2021-01-03 16:45:43 IP address: 2400 : adc1 : 12f : 2200 : e4ac : 5109 : 9a1d : 6be9</p>
                </div>
                <div class="profile-inner-row-3">
                    <div class="row">
                        <div class="col-sm-4 cl-1">Identity Verification</div>
                        <div class="col-sm-4 cl-2">Submit Verification</div>
                        <div class="col-sm-4 cl-3"><img src="{{asset('frontend/img/warning-icon.png')}}"/><span>Unverified</span></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 cl-1">2 Factor Verification</div>
                        <div class="col-sm-4 cl-2">To Protect your account Security</div>
                        <div class="col-sm-4 cl-3"><img src="{{asset('frontend/img/warning-icon.png')}}"/><span>Off</span></div>
                    </div>
                </div>
                
            </div>
        </div>
        </div>
    </div>
    
@include('frontend.partials.footer')
</div>
@endsection
@push('scripts')

	<!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- Popper.JS -->
<!--     <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script> -->
    <!-- Bootstrap JS -->
    <!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script> --> -->

    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
                $(this).toggleClass('active');
            });
        });
        $(document).ready(function(){
        	$('body').addClass("profile-pg");
        })
    </script>


@endpush
</div>
