@extends('frontend.layouts.app')
@push('styles')
<link href="{{asset('frontend/css/pricing.css')}}" rel="stylesheet">
<style type="text/css">
    .main-bg{
        background-image: url({{asset('frontend/img/bg.jpg')}});
        background-position: center center;
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-size: cover;
        background-color: #464646;
        height:100vh;
    }
  .nav-a-margin{
      margin-left: 10px;
      margin-right: 10px;
  }
  .dark-light-btn{
      
      position: absolute;
  }
  .pd-0
  {
      padding-right: 0px;
      padding-left: 0px;
  }
  .red {
      border: 3px solid red;
  }
</style>
@endpush
@section('content')
@include('frontend.partials.darklight')
<div class="container-fluid pd-0">
    <div class="pricing-head-bg">
        @include('frontend.partials.navbar')
    </div>
    <div class="first-pricing-cont row financial-schedule-bg">
      <div class="container">
          <div class="row">
            <div class="col-lg-12 col-sm-hidden col-xs-hidden">
              <div class="space-padding"></div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-8 col-lg-8">
              <h1 class="pricing-h1-text">Financial Fee Schedule</h1>
            </div>
          </div>
          <div class="row">
            <div class="col-md-8 col-lg-8">
              <p class="pricing-p1-text">VTrader believes that technology empowers traders to be more profitable helping to grow our user’s economic freedom. Technology also enables us to provide transparent and straightforward prices with 0 commission trades and no deposit minimums.</p>
                <p class="pricing-p2-text">We make money the same way every other broker makes money, but with one less revenue line item: commissions. In order to keep the lights on, we optimize the back-end revenue streams that every other broker (traditional or non) utilize to generate revenue. Simply put, these are stock loans, interest on free credit balances, margin interest and payment for order flow. More information on this can be found in our SEC Rule 606 disclosure.</p>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-12 col-sm-hidden col-xs-hidden">
              <div class="space-padding-two"></div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 col-lg-12">
              <button type="button" class="btn btn-primary open-account">Open an Account</button>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-12 col-sm-hidden col-xs-hidden">
              <div class="space-padding-three"></div>
            </div>
          </div>
      </div>
    </div>
    <div class="second-pricing-cont row interest-rates-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-hidden col-xs-hidden">
                    <div class="space-padding"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-lg-8">
                    <h1 class="interest-rates-h1-text">Tiered Margin Interest Rates</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-11 col-lg-11">
                    <p class="interest-rates-p1-text">Annual Margin Rate</p>
                    <p class="interest-rates-p2-text">VTrader provides up to 4x day-trade buying power and 2x overnight buying power with a margin account. You must have at least $2,000 to qualify.</p>
                    <p class="interest-rates-p3-text">Interest on margin trading is calculated on a daily basis and paid on a monthly basis. The margin rate is variable and is determined by the size of the margin loan</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-lg-4">
                    <div class="responsive-table">
                      <table class="table interest-rates-table mt-5">
                        <thead>
                          <tr>
                            <th class="border-0" align="" scope="col">Debit Balance  </th>
                            <th class="border-0" align="" scope="col">Annual Margin Rate</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td class="border-top-0" align="left">$0~25,000.00</td>
                            <td class="border-top-0" align="center">6.99%</td>
                          </tr>
                          <tr>
                            <td align="left">$25,000.01~100,000.00 </td>
                            <td align="center">6.99%</td>
                          </tr>
                          <tr>
                            <td align="left">$100,000.01~250,000.00</td>
                            <td align="center">5.99%</td>
                          </tr>
                          <tr>
                            <td align="left">$250.000.01~500,000.00 </td>
                            <td align="center">5.49%</td>
                          </tr>
                          <tr>
                            <td align="left">$500,000.01~1,000,000.00</td>
                            <td align="center">4.99%</td>
                          </tr>
                          <tr>
                            <td align="left">$1,000,000.01~3,000,000.00</td>
                            <td align="center">4.49%</td>
                          </tr>
                          <tr>
                            <td align="left">$3,000,000.00</td>
                            <td align="center">3.99%</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-sm-hidden col-xs-hidden">
                    <div class="space-padding-four"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="first-pricing-cont row">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-sm-hidden col-xs-hidden">
                    <div class="space-padding"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-lg-8">
                    <h1 class="fees-charged-h1-text">Fees Charged By Regulatory Agencies & Exchanges</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-11 col-lg-11">
                    <p class="fees-charged-p1-text">VTrader does not charge commissions for trading stocks, ETFs and options listed on U.S. exchanges. However, fees are still applied by the SEC, FINRA and OCC, the regulatory agencies. VTrader does not profit from these fees.</p>
                    <div class="responsive-table">
                      <table class="table fees-charged-table mb-5">
                        <thead>
                          <tr>
                            <th class="border-0 td-width-30" style="text-align:left;" scope="col">Trading Privileges</th>
                            <th class="border-0 td-width-15" style="text-align:center;" scope="col">Charged By</th>
                            <th class="border-0 td-width-15" style="text-align:center;" scope="col">Types</th>
                            <th class="border-0 td-width-15" style="text-align:center;" scope="col">Fees</th>
                            <th class="border-0 td-width-15" style="text-align:center;" scope="col">Rule</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr class="tr-brd-btm">
                            <td class="border-top-0 td-width-30" rowspan="2" align="left" style="vertical-align:middle;">Stock/ETF</td>
                            <td class="border-top-0 td-width-15" align="center">The U.S. Securities and Exchange Commission (SEC)</td>
                            <td class="border-top-0 td-width-15" align="center">Transaction Fee</td>
                            <td class="border-top-0 td-width-15" align="center">$0.0000051*Total $ Trade Amount (Min $0.01)</td>
                            <td class="border-top-0 td-width-15" align="center">Sells only</td>
                          </tr>
                          <tr class="tr-brd-btm">
                            <!--<td class="border-top-0" align="center">$25,000.01~100,000.00 </td>-->
                            <td class="border-top-0 td-width-15" align="center">Financial Industry Regulatory Authority (FINRA)</td>
                            <td class="border-top-0 td-width-15" align="center">Regulatory Fee  </td>
                            <td class="border-top-0 td-width-15" align="center">$0.000119 * Total Trade Volume Min $0.01 per - Max $5.95 per</td>
                            <td class="border-top-0 td-width-15" align="center">Sells only copy</td>
                          </tr>
                          <tr>
                            <td class="border-top-0 td-width-30" rowspan="3" align="left" style="vertical-align:middle;">Options</td>
                            <td class="border-top-0 td-width-15" align="center">The U.S. Securities and Exchange Commission (SEC)</td>
                            <td class="border-top-0 td-width-15" align="center">Transaction Fee</td>
                            <td class="border-top-0 td-width-15" align="center">$0.0000051*Total $ Trade Amount (Min $0.01)</td>
                            <td class="border-top-0 td-width-15" align="center">Sells only</td>
                          </tr>
                          <tr class="tr-brd-btm tr-brd-top">
                            <td class="border-top-0 td-width-15" align="center">Financial Industry Regulatory Authority (FINRA)</td>
                            <td class="border-top-0 td-width-15" align="center">Trading Activity Fee</td>
                            <td class="border-top-0 td-width-15" align="center">$0.002 * No. of Contracts (Min $0.01)</td>
                            <td class="border-top-0 td-width-15" align="center">Sells only</td>
                          </tr>
                          <tr>
                            <!--<td class="border-top-0" align="center">$500,000.01~1,000,000.00</td>-->
                            <td class="border-top-0 td-width-15" align="center">Options Exchanges</td>
                            <td class="border-top-0 td-width-15" align="center">Options Regulatory Fee  </td>
                            <td class="border-top-0 td-width-15" align="center">$0.0388 * No. of Contracts </td>
                            <td class="border-top-0 td-width-15" align="center">Buy and Sells </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <p class="fees-charged-p2-text">The Transaction Fee is charged by the U.S. Securities and Exchange Commission (SEC), an independent agency of the United States federal government.<br> <a href="#" class="link hover-7">Learn more ></a> </p>
                    <p class="fees-charged-p3-text">Financial Industry Regulatory Authority (FINRA) requires each member to pay trading activity fees for the sale of covered securities.<br> <a href="#" class="link hover-7">Learn more ></a> </p>
                    <p class="fees-charged-p4-text">The Options Regulatory Fee is a fee assessed by exchanges on their members. It is collected by The Options Clearing Corp (OCC) on behalf of the U.S. options Exchanges.<br> <a href="#" class="link hover-7">Learn more ></a> </p>
                    <p class="fees-charged-p5-text">The Clearing Fee is charged by The Options Clearing Corp (OCC) who provides central counterparty (CCP) clearing and settlement services to 16 exchanges.<br> <a href="#" class="link hover-7">Learn more ></a> </p>
                    <p class="fees-charged-p6-text">Our clearing firm, Apex, will withhold the above regulatory fees where applicable. All fees in the table above are subject to change without notice as per our clearing partner, Apex Clearing.<br> <a href="#" class="link hover-7">Learn more ></a></p>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-sm-hidden col-xs-hidden">
                    <div class="space-padding"></div>
                </div>
            </div>
            <!-- <div class="row">
                <div class="col-lg-12 col-sm-hidden col-xs-hidden">
                    <div class="space-padding-two"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-sm-hidden col-xs-hidden">
                    <div class="space-padding-three"></div>
                </div>
            </div> -->
        </div>
    </div>

    <div class="sec-pricing-cont row stk-trnsf-bg">
      <div class="container">
        <div class="row">
            <div class="col-md-8 col-lg-8">
                <h1 class="fees-charged-h1-text">Stock Transfer Fees</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10 col-lg-10">
              <div class="responsive-table">
                <table class="table fees-charged-table mb-5">
                  <thead>
                    <tr>
                      <th class="border-0 td-width-30" style="text-align:left;" scope="col">Transfer direction</th>
                      <th class="border-0 td-width-15" style="text-align:left;" scope="col">Charged By</th>
                      <th class="border-0 td-width-15" style="text-align:left;" scope="col">Fees</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr class="tr-brd-btm">
                      <td class="border-top-0 td-width-30" rowspan="2" align="left" style="vertical-align:middle;font-weight: 600;">Transfer to Vtrader</td>
                      <td class="border-top-0 td-width-15 td-pd-top" align="center">Apex (Clearing Firm)</td>
                      <td class="border-top-0 td-width-15 td-pd-top" align="center">$0.00</td>
                    </tr>
                    <tr class="tr-brd-btm">
                      <td class="border-top-0 td-width-15 td-pd-top" align="center">Contra Broker</td>
                      <td class="border-top-0 td-width-15 td-pd-top" align="center">Please contact the contra Broker for details.</td>
                    </tr>
                    <tr>
                      <td class="border-top-0 td-width-30" rowspan="2" align="left" style="vertical-align:middle;font-weight: 600;">Transfer from Vtrader</td>
                      <td class="border-top-0 td-width-15 td-pd-top" align="center">Apex (Clearing Firm)</td>
                      <td class="border-top-0 td-width-15 td-pd-top" align="center">$75 per Outgoing Stock Transfer</td>
                    </tr>
                    <tr class="tr-brd-top">
                      <td class="border-top-0 td-width-15 td-pd-top" align="center">Contra Broker</td>
                      <td class="border-top-0 td-width-15 td-pd-top" align="center">Please contact the contra Broker for details.</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
        </div>
      </div>
    </div>


    @include('frontend.partials.footer')
</div>
@endsection