@extends('frontend.layouts.app')
@push('styles')
<title>{{@$privacyPolicy->title}}</title>

<meta name="title" content="{{@$privacyPolicy->meta_title}}">
<meta name="keywords" content="{{@$privacyPolicy->meta_keyword}}">
<meta name="description" content="{{@$privacyPolicy->meta_description}}">
<!-- <link href="{{asset('frontend/css/custom-market.css')}}" rel="stylesheet"> -->
<link href="{{asset('frontend/css/privacypolicy.css')}}" rel="stylesheet">
@endpush
@section('content')
@include('frontend.partials.darklight')
<div class="container-fluid pd-0">
    <div class="privacy-policy-bg">
        @include('frontend.partials.navbar')

        <div class="container">
            <div class="row privacy-policy-div">
                <div class="col-md-12 col-sm-12">
                    <h1 class="text-left privacy-policy-headng">{{@$content->main_title}}</h1>
                </div>
                <!-- {{-- <div class="col-md-10 col-sm-12">
                    <p class="privacy-policy-p1 mb-4">
                        Vtrader recognizes and respects the privacy concerns and expectations of our customers
                    </p>
                    <p class="privacy-policy-p2 mb-4">
                        Vtrader provides this notice to you so that you will know what kinds of information Webull
                        collects about you and the circumstances in which that information may be disclosed to third
                        parties who are not affiliates with Webull.
                    </p>
                    <p class="privacy-policy-p3 mb-4">
                        Collection of Customer Information
                    </p>
                    <p class="privacy-policy-p4 mb-4">
                        Vtrader may collect non-public personal information about our customers from the following
                        sources:
                    </p>
                    <p class="privacy-policy-p5 mb-4">
                        - Account application and other forms, which may include a customer’s name, address, social
                        security number, driver’s license and information about a customer’s income, net worth,
                        investment goals and risk tolerance;
                    </p>
                    <p class="privacy-policy-p6 mb-4">
                        - Account History, including information about transactions and positions in customer’s
                        accounts; and
                    </p>
                    <p class="privacy-policy-p7 mb-4">
                        - Correspondence, written, telephonic or electronic between a customer and Webull or service
                        providers to Webull.
                    </p>
                    <p class="privacy-policy-p7 mb-5">
                        Disclosure of Customer Information
                    </p>
                    <p class="privacy-policy-p8 mb-4">
                        Vtrader does not disclose nonpublic personal information about our customers or former customers
                        to anyone, except permitted by law. Vtrader may disclose nonpublic personal information about
                        our customers or former customers to non-affiliated third parties of Vtrader under one or more
                        of these circumstances.
                    </p>
                    <p class="privacy-policy-p9 mb-4">
                        – if you request or authorize the disclosure of the information.
                    </p>
                    <p class="privacy-policy-p10 mb-4">
                        – for example, to effect, administer or enforce a transaction, that a consumer requests, or
                        authorizes, or in connection with processing or servicing a financial product or service that a
                        customer requests or authorizes.
                    </p>
                </div> --}} -->
				<div class="col-md-12 col-sm-12 privacy-policy-p1 text-justify">
					{!!@$content->main_description!!}
				</div>
            </div>
        </div>
    </div>
    @include('frontend.partials.footer')
</div>
@endsection
