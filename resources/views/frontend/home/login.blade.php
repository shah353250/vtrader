@extends('frontend.layouts.app')
@push('styles')
<title>{{@$termsAndServices->title}}</title>
<meta name="description" content="{{@$termsAndServices->description}}"> 
<link href="{{asset('frontend/css/login.css')}}" rel="stylesheet">
@endpush
@section('content')
@include('frontend.partials.darklight')
<div class="container-fluid pd-0 login-main-bg">
@include('frontend.partials.navbar')
    <div class="login-bg">
        <div class="container">
          <div class="row">
            <div class="col-md-5">
                <div id="content">
                    <h1 class="h1-style">Welcome to <br/><span class="h1-bold">Vtrader</span></h1>
                </div>
            </div>
            <div class="col-md-7">
                <div class="login-box">
                <div class="tab-box">
  <div class="tabs">
    <button class="tabs__button tabs__button--active" type="button">Email Login</button>
    <button class="tabs__button" type="button">Mobile Login</button>
  </div>

  <div class="tab-content tab-content--active">
    <div>
        <div class="input-group">
            <span>
                <img src="{{asset('frontend/img/login-id.png')}}" />
            </span>
            <input type="text" class="form-control text-box" placeholder="Email-Address" />
        </div>
        <div class="input-group">
            <span>
                <img src="{{asset('frontend/img/login-password.png')}}" style="padding-top: 5px;" />
            </span>
            <input type="text" class="form-control text-box" placeholder="Password" />
        </div>
        <div>
            <a href="" class="forget-pass">Forget Password</a>
        </div>
        <div style="text-align: center;margin-top: 120px;">
            <a href="#" class="login-btn-one">Login</a>
        </div>
        <div class="mrg-top-txt-center">
            <p class="sign-in-opt"><strong>Sign up</strong> or <strong>Log in</strong> with</p>
        </div>
        <div class="mrg-top-txt-center">
            <a href="#"><img src="{{asset('frontend/img/gmail-icon.png')}}" class="login-icon" /></a>
            <a href="#" style="margin-left:30px;"><img src="{{asset('frontend/img/facebook-white.png')}}" class="login-icon" /></a>
        </div>
    </div>
  </div>

  <div class="tab-content">
  <div>
        <div class="input-group">
            <span>
                <img src="{{asset('frontend/img/login-id.png')}}" />
            </span>
            <input type="text" class="form-control text-box" placeholder="Mobile No." />
        </div>
        <div class="input-group">
            <span>
                <img src="{{asset('frontend/img/login-password.png')}}" style="padding-top: 5px;" />
            </span>
            <input type="text" class="form-control text-box" placeholder="Password" />
        </div>
        <div>
            <a href="" class="forget-pass">Forget Password</a>
        </div>
        <div style="text-align: center;margin-top: 120px;">
            <a href="#" class="login-btn-one">Login</a>
        </div>
        <div class="mrg-top-txt-center">
            <p class="sign-in-opt"><strong>Sign up</strong> or <strong>Log in</strong> with</p>
        </div>
        <div class="mrg-top-txt-center">
            <a href="#"><img src="{{asset('frontend/img/gmail-icon.png')}}" class="login-icon" /></a>
            <a href="#" style="margin-left:30px;"><img src="{{asset('frontend/img/facebook-white.png')}}" class="login-icon" /></a>
        </div>
    </div>
  </div>
</div>
                </div>
                <!-- <img src="{{asset('frontend/img/login-box.png')}}" class="">	 -->
            </div>
          </div>   
        </div>
    </div>
@include('frontend.partials.footer')
</div>
@endsection
@push('scripts')
<script type="text/javascript">
const btns = document.querySelectorAll(".tabs__button");
const tabContent = document.querySelectorAll(".tab-content");

for (let i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", () => {
    addClassFunc(btns[i], "tabs__button--active");
    clearClassFunc(i, btns, "tabs__button--active");

    addClassFunc(tabContent[i], "tab-content--active");
    clearClassFunc(i, tabContent, "tab-content--active");
  });
}

function addClassFunc(elem, elemClass) {
  elem.classList.add(elemClass);
}

function clearClassFunc(indx, elems, elemClass) {
  for (let i = 0; i < elems.length; i++) {
    if (i === indx) {
      continue;
    }
    elems[i].classList.remove(elemClass);
  }
}
</script>
@endpush
