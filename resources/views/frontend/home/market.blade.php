@extends('frontend.layouts.app')
@push('styles')
<link href="{{asset('frontend/css/custom-market.css')}}" rel="stylesheet">



@endpush
@section('content')
@include('frontend.partials.darklight')
    <!-- 1st Screen -->
		<div class="container-fluid pd-0">
			<div class="market-bg market-pg-header market-pg-common">
				@include('frontend.partials.navbar')
			</div>
			<div class="bg-one market-pg-sec-2 market-pg-common">
				<div class="container con-one">	
					<div class="row">
						<div class="col-md-12 col-lg-12">
							<div class="row">
								<div class="col-lg-4 col-md-4 pd-20">
									<div class="bx-shad">
										<h1 class="bx-h1">SPY <img class="bx-img" src="{{asset('frontend/img/arrow.png')}}" /></h1>
										<p class="bx-p">373.55</p>
										<p class="bx-p-2">+2.22 +0.60%</p>
										<img style="width:100%;" src="{{asset('frontend/img/grph.png')}}"/>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 pd-20">
									<div class="bx-shad">
										<h1 class="bx-h1">IXIA <img class="bx-img" src="{{asset('frontend/img/arrow.png')}}" /></h1>
										<p class="bx-p">373.55</p>
										<p class="bx-p-2">+2.22 +0.60%</p>
										<img style="width:100%;" src="{{asset('frontend/img/grph.png')}}"/>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 pd-20">
									<div class="bx-shad">
										<h1 class="bx-h1">DIC <img class="bx-img" src="{{asset('frontend/img/downarrow.png')}}" /></h1>
										<p class="bx-p-3">373.55</p>
										<p class="bx-p-4">-2.22 +0.60%</p>
										<img style="width:100%;" src="{{asset('frontend/img/grph.png')}}"/>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>		
			<!-- 1st Screen End -->
			<!-- 2nd Screen Start -->
			<div class="new-bg-1 market-pg-sec-3 market-pg-common">
				<div class="container">
					<div class="row">
						<div class="col-lg-12 col-md-12">
							<div class="row pd-60 mb-pd-20 mb-pd-20">
								<div class="col-lg-3 col-md-3 col-sm-6 market-sec-2" class="cl-3">
									<h4 class="h4-one">BSESN</h4>
									<p class="p-one">48,039.23</p>
									<p class="p-one">+80.475 -0.17%</p>		
								</div>
								<div class="col-lg-3 col-md-3 col-sm-6 market-sec-2 bd-left" class="cl-3">
									<h4 class="h4-one">FTMC</h4>
									<p class="p-two">48,039.23</p>
									<p class="p-two">+80.475 -0.17%</p>			
								</div>
								<div class="col-sm-12 d-none d-sm-block d-md-none">
							        <div style="padding:30px;">
							        
							        </div>
								</div>
								<div class="col-lg-3 col-md-3 col-sm-6 market-sec-2 bd-left" class="cl-3">
									<h4 class="h4-one">FTSC</h4>
									<p class="p-one">48,039.23</p>
									<p class="p-one">+80.475 -0.17%</p>			
								</div>
								<div class="col-lg-3 col-md-3 col-sm-6 market-sec-2 bd-left">
									<h4 class="h4-one">BSESN</h4>
									<p class="p-two">48,039.23</p>
									<p class="p-two">+80.475 -0.17%</p>		
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
			<!-- 2nd Screen End -->
			<!-- 3rd Screen Start -->
			
			<div class="mrk-overview-bg market-pg-sec-4 market-pg-common">
				<div class="container"  style="position: relative;">
					<div class="row">
						<div class="col-lg-12 col-md-12">
							<div class="row pd-60">
								<div class="col-lg-12 col-md-12" class="cl-3">
									<h2 class="sty-one">Market Overview</h2>
									<h4 class="sty-one-h4">Advancers & Decliners Distribution</h4>		
								</div>
							</div>
							<div class="row pd-60-40">
								<div class="col-lg-10 col-md-10 dv-bar-width">
									<img class="bar-width" src="{{asset('frontend/img/bar.png')}}">
								</div>
								<!-- <div class="col-lg-2 col-md-2"> -->
									<!-- <img src="{{asset('frontend/img/side-graph.png')}}" style="width: 100%;"> -->
								<!-- </div> -->
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<!-- 3rd Screen End -->
			<!-- 4th Screen Start -->
			<div class="new-bg-1 market-pg-sec-5 market-pg-common">
				<div class="container">
					<div class="row">
						<div class="col-lg-12 col-md-12">
							<div class="row pd-60">
								<div class="col-lg-12 col-md-12">
									<h2 class="sty-two">Hot Industries</h2>
								</div>
								<div class="col-lg-6 bg-c-6 p-blocks-dv">
									<div class="sty-three">
										<h5 class="h4-one">Renewable Energy Equipment</h5>
										<div class="box-main">
											<div class="b-r-1">
												<div class="box-one-one">
													<div class="p-blocks">
														<p class="fnt-12">Sunworks</p>
														<p class="fnt-12">+13.79%</p>
													</div>
												</div>
												<div class="box-one-one">
													<div class="p-blocks">
														<p class="fnt-12">Sunworks</p>
														<p class="fnt-12">+13.79%</p>
													</div>
												</div>
											</div>
											<div class="b-r-2 left-div-margin">
												<div class="box-three">
													<div class="p-blocks">
														<p class="fnt-12">Sunworks</p>
														<p class="fnt-12">+13.79%</p>
													</div>
												</div>
												<div class="box-three">
												<div class="p-blocks">
														<p class="fnt-12">Sunworks</p>
														<p class="fnt-12">+13.79%</p>
												</div>
												</div>
											</div>
											<div class="b-r-3">
												<div class="box-two">
													<div class="p-blocks">
														<p class="fnt-12">Sunworks</p>
														<p class="fnt-12">+13.79%</p>
													</div>
												</div>
											</div>
										</div>
									</div>		
								</div>
								<div class="col-lg-6 bg-c-6 p-blocks-dv">
									<div class="sty-three">
										<h5 class="h4-one">Auto Truck Manufacturer</h5>
										<div class="box-main">
											<div class="b-r-1">
												<div class="box-one" style="height: 200px;">
													<div class="p-blocks">
														<p class="fnt-12">Sunworks</p>
														<p class="fnt-12">+13.79%</p>
													</div>
												</div>
												<div class="box-one" style="height: 125px;">
													<div class="p-blocks">
														<p class="fnt-12">Sunworks</p>
														<p class="fnt-12">+13.79%</p>
													</div>
												</div>
											</div>
											<div class="b-r-3">
												<div class="box-two">
													<div class="p-blocks">
														<p class="fnt-12">Sunworks</p>
														<p class="fnt-12">+13.79%</p>
													</div>
												</div>
											</div>
											<div class="b-r-2  right-div-margin">
												<div class="box-three">
													<div class="p-blocks">
														<p class="fnt-12">Sunworks</p>
														<p class="fnt-12">+13.79%</p>
													</div>
												</div>
												<div class="box-three">
													<div class="p-blocks">
														<p class="fnt-12">Sunworks</p>
														<p class="fnt-12">+13.79%</p>
													</div>
												</div>
											</div>
										</div>
									</div>		
								</div>
								<div class="col-lg-12 col-md-12">
									<div style="padding: 25px;"></div>
								</div>
								<div class="col-lg-4 col-md-6 bg-c-6 exit-hours-dv">
									<div class="sty-seven">
										<h5 class="sty-eight">Exit Hour Rankings</h5>
										<div class="row sty-nine">
											<div class="col-md-12">
												<table class="table">
											      <thead>
											         <tr>
											            <th class="th-fnt">Symbol</th>
											            <th class="th-fnt">Last Price</th>
											            <th class="th-fnt">Change</th>
											            <th class="th-fnt">%Change</th>
											         </tr>
											      </thead>
											      <tbody>
											         <tr>
											            <td class="col-green">SOS</td>
											            <td class="col-green">OGEN</td>
											            <td class="col-green">OGEN</td>
											            <td class="col-green">OGEN</td>
											         </tr>
											         <tr>
											            <td class="col-green">1.720</td>
											            <td class="col-green">1.720</td>
											            <td class="col-green">1.720</td>
											            <td class="col-green">1.720</td>
											         </tr>
											         <tr>
											            <td class="col-green">+0.33</td>
											            <td class="col-green">+0.33</td>
											            <td class="col-green">+0.33</td>
											            <td class="col-green">+0.33</td>
											         </tr>
											         <tr>
											            <td class="col-green">+81.41%</td>
											            <td class="col-green">+81.41%</td>
											            <td class="col-green">+81.41%</td>
											            <td class="col-green">+81.41%</td>
											         </tr>
											      </tbody>
											   </table>
											</div>
										</div>
									</div>		
								</div>
								<div class="col-lg-4 col-md-6 bg-c-6 exit-hours-dv">
									<div class="sty-seven">
										<h5 class="sty-eight">Exit Hour Rankings</h5>
										<div class="row sty-nine">
											<div class="col-md-12">
												<table class="table">
											      <thead>
											         <tr>
													 	<th class="th-fnt">Symbol</th>
											            <th class="th-fnt">Last Price</th>
											            <th class="th-fnt">Change</th>
											            <th class="th-fnt">%Change</th>
											         </tr>
											      </thead>
											      <tbody>
											         <tr>
											            <td class="fnt-12">SOS</td>
											            <td class="col-red">1.720</td>
											            <td class="col-red">+0.33</td>
											            <td class="col-red">+81.41%</td>
											         </tr>
											         <tr>
											         	<td class="fnt-12">OGEN</td>
											            <td class="col-green">1.720</td>
											            <td class="col-green">1.720</td>
											            <td class="col-green">1.720</td>
											         </tr>
											         <tr>
											            
											            <td class="fnt-12">OGEN</td>
											            <td class="col-green">+0.33</td>
											            <td class="col-green">+0.33</td>
											            <td class="col-green">+0.33</td>
											         </tr>
											         <tr>
											         	<td class="col-red">OGEN</td>
											            <td class="col-red">+81.41%</td>
											            <td class="col-red">+81.41%</td>
											            <td class="col-red">+81.41%</td>
											         </tr>
											      </tbody>
											   </table>
											</div>
										</div>
									</div>		
								</div>
								<div class="d-none d-md-block d-lg-none col-md-12 ">
                                    <div style="padding: 10px;"></div>
                                </div>
								<div class="col-lg-4 col-md-12 bg-c-6 exit-hours-dv">
									<div class="sty-seven">
										<h5 class="sty-eight">Exit Hour Rankings</h5>
										<div class="row sty-nine">
											<div class="col-md-12">
												<table class="table">
											      <thead>
											         <tr>
													 	<th class="th-fnt">Symbol</th>
											            <th class="th-fnt">Last Price</th>
											            <th class="th-fnt">Change</th>
											            <th class="th-fnt">%Change</th>
											         </tr>
											      </thead>
											      <tbody>
											         <tr>
											            <td class="fnt-12">SOS</td>
											            <td class="col-red">1.720</td>
											            <td class="col-red">+0.33</td>
											            <td class="col-red">+81.41%</td>
											         </tr>
											         <tr>
											         	<td class="fnt-12">OGEN</td>
											            <td class="col-green">1.720</td>
											            <td class="col-green">1.720</td>
											            <td class="col-green">1.720</td>
											         </tr>
											         <tr>
											            
											            <td class="fnt-12">OGEN</td>
											            <td class="col-green">+0.33</td>
											            <td class="col-green">+0.33</td>
											            <td class="col-green">+0.33</td>
											         </tr>
											         <tr>
											         	<td class="col-red">OGEN</td>
											            <td class="col-red">+81.41%</td>
											            <td class="col-red">+81.41%</td>
											            <td class="col-red">+81.41%</td>
											         </tr>
											      </tbody>
											   </table>
											</div>
										</div>
									</div>		
								</div>
								<div class="col-lg-12 col-md-12">
									<div style="padding: 25px;"></div>
								</div>
								<div class="col-lg-6 col-md-6 bg-c-6">
									<div class="sty-seven">
										<h5 class="sty-eight">IPO-Filed</h5>
										<div class="row sty-nine">
											<div class="col-md-12">
												<table class="table show-tb-lg">
											      <thead>
											         <tr>
											            <th class="th-fnt" style="width:35%;">Name</th>
											            <th class="th-fnt">Offering Price</th>
											            <th class="th-fnt">Offering Shares</th>
											            <th class="th-fnt">Date</th>
											         </tr>
											      </thead>
											      <tbody>
											         <tr>
											            <td class="fnt-12 pd-10">Vickers Vantage Corp. I</td>
											            <td class="lt-bl fnt-12 pd-10">10.00</td>
											            <td class="lt-bl fnt-12 pd-10">15,000,000</td>
											            <td class="lt-bl fnt-12 pd-10">01/01/2021</td>
											         </tr>
											         <tr>
											            <td class="fnt-12 pd-10">Vickers Vantage Corp. I</td>
											            <td class="lt-bl fnt-12 pd-10">10.00</td>
											            <td class="lt-bl fnt-12 pd-10">15,000,000</td>
											            <td class="lt-bl fnt-12 pd-10">01/01/2021</td>
											         </tr>
											         <tr>
											            <td class="fnt-12 pd-10">Vickers Vantage Corp. I</td>
											            <td class="lt-bl fnt-12 pd-10">10.00</td>
											            <td class="lt-bl fnt-12 pd-10">15,000,000</td>
											            <td class="lt-bl fnt-12 pd-10">01/01/2021</td>
											         </tr>
											         <tr>
											            <td class="fnt-12 pd-10">Vickers Vantage Corp. I</td>
											            <td class="lt-bl fnt-12 pd-10">10.00</td>
											            <td class="lt-bl fnt-12 pd-10">15,000,000</td>
											            <td class="lt-bl fnt-12 pd-10">01/01/2021</td>
											         </tr>
											         <tr>
											            <td class="fnt-12 pd-10">Vickers Vantage Corp. I</td>
											            <td class="lt-bl fnt-12 pd-10">10.00</td>
											            <td class="lt-bl fnt-12 pd-10">15,000,000</td>
											            <td class="lt-bl fnt-12 pd-10">01/01/2021</td>
											         </tr>
											         <tr>
											            <td class="fnt-12 pd-10">Vickers Vantage Corp. I</td>
											            <td class="lt-bl fnt-12 pd-10">10.00</td>
											            <td class="lt-bl fnt-12 pd-10">15,000,000</td>
											            <td class="lt-bl fnt-12 pd-10">01/01/2021</td>
											         </tr>
											         <tr>
											            <td class="fnt-12 pd-10">Vickers Vantage Corp. I</td>
											            <td class="lt-bl fnt-12 pd-10">10.00</td>
											            <td class="lt-bl fnt-12 pd-10">15,000,000</td>
											            <td class="lt-bl fnt-12 pd-10">01/01/2021</td>
											         </tr>
											         <tr>
											            <td class="fnt-12 pd-10">Vickers Vantage Corp. I</td>
											            <td class="lt-bl fnt-12 pd-10">10.00</td>
											            <td class="lt-bl fnt-12 pd-10">15,000,000</td>
											            <td class="lt-bl fnt-12 pd-10">01/01/2021</td>
											         </tr>
											      </tbody>
											   </table>
											   <!-- Small Screen table -->
											   <table role="table" class="show-tb-sm">
                                                  <thead class="sm-resp-thead" role="rowgroup">
                                                    <tr class="sm-resp-tr" role="row">
                                                      <th class="sm-resp-th" role="columnheader">Name</th>
                                                      <th class="sm-resp-th" role="columnheader">Offering Price</th>
                                                      <th class="sm-resp-th" role="columnheader">Offering Shares</th>
                                                      <th class="sm-resp-th" role="columnheader">Date</th>
                                                    </tr>
                                                  </thead>
                                                  <tbody class="sm-resp-tbody" role="rowgroup">
                                                    <tr class="sm-resp-tr-1" role="row">
                                                      <td class="sm-resp-td" role="cell">Vickers Vantage Corp. I</td>
                                                      <td class="sm-resp-td" role="cell">10.00</td>
                                                      <td class="sm-resp-td" role="cell">15,000,000</td>
                                                      <td class="sm-resp-td" role="cell">01/01/2021</td>
                                                    </tr>
                                                    <tr class="sm-resp-tr-1" role="row">
                                                      <td class="sm-resp-td" role="cell">Vickers Vantage Corp. I</td>
                                                      <td class="sm-resp-td" role="cell">10.00</td>
                                                      <td class="sm-resp-td" role="cell">15,000,000</td>
                                                      <td class="sm-resp-td" role="cell">01/01/2021</td>
                                                    </tr>
                                                    <tr class="sm-resp-tr-1" role="row">
                                                      <td class="sm-resp-td" role="cell">Vickers Vantage Corp. I</td>
                                                      <td class="sm-resp-td" role="cell">10.00</td>
                                                      <td class="sm-resp-td" role="cell">15,000,000</td>
                                                      <td class="sm-resp-td" role="cell">01/01/2021</td>
                                                    </tr>
                                                    <tr class="sm-resp-tr-1" role="row">
                                                      <td class="sm-resp-td" role="cell">Vickers Vantage Corp. I</td>
                                                      <td class="sm-resp-td" role="cell">10.00</td>
                                                      <td class="sm-resp-td" role="cell">15,000,000</td>
                                                      <td class="sm-resp-td" role="cell">01/01/2021</td>
                                                    </tr>
                                                    <tr class="sm-resp-tr-1" role="row">
                                                      <td class="sm-resp-td" role="cell">Vickers Vantage Corp. I</td>
                                                      <td class="sm-resp-td" role="cell">10.00</td>
                                                      <td class="sm-resp-td" role="cell">15,000,000</td>
                                                      <td class="sm-resp-td" role="cell">01/01/2021</td>
                                                    </tr>
                                                    <tr class="sm-resp-tr-1" role="row">
                                                      <td class="sm-resp-td" role="cell">Vickers Vantage Corp. I</td>
                                                      <td class="sm-resp-td" role="cell">10.00</td>
                                                      <td class="sm-resp-td" role="cell">15,000,000</td>
                                                      <td class="sm-resp-td" role="cell">01/01/2021</td>
                                                    </tr>
                                                    <tr class="sm-resp-tr-1" role="row">
                                                      <td class="sm-resp-td" role="cell">Vickers Vantage Corp. I</td>
                                                      <td class="sm-resp-td" role="cell">10.00</td>
                                                      <td class="sm-resp-td" role="cell">15,000,000</td>
                                                      <td class="sm-resp-td" role="cell">01/01/2021</td>
                                                    </tr>
                                                    <tr class="sm-resp-tr-1" role="row">
                                                      <td class="sm-resp-td" role="cell">Vickers Vantage Corp. I</td>
                                                      <td class="sm-resp-td" role="cell">10.00</td>
                                                      <td class="sm-resp-td" role="cell">15,000,000</td>
                                                      <td class="sm-resp-td" role="cell">01/01/2021</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
											   <!-- Small Screen table End -->
											</div>
										</div>
									</div>		
								</div>
								<div class="col-lg-6 col-md-6 bg-c-6">
									<div class="sty-seven">
										<h5 class="sty-eight">IPO Offerings</h5>
										<div class="row sty-nine">
											<div class="col-md-12">
												<table class="table show-tb-lg">
											      <thead>
											         <tr>
											            <th class="th-fnt" style="width:35%;">Name</th>
											            <th class="th-fnt">Offering Price</th>
											            <th class="th-fnt">Offering Shares</th>
											            <th class="th-fnt">Date</th>
											         </tr>
											      </thead>
											      <tbody>
											         <tr>
											            <td class="fnt-12 pd-10">Vickers Vantage Corp. I</td>
											            <td class="lt-bl fnt-12 pd-10">10.00</td>
											            <td class="lt-bl fnt-12 pd-10">15,000,000</td>
											            <td class="lt-bl fnt-12 pd-10">01/01/2021</td>
											         </tr>
											         <tr>
											            <td class="fnt-12 pd-10">Vickers Vantage Corp. I</td>
											            <td class="lt-bl fnt-12 pd-10">10.00</td>
											            <td class="lt-bl fnt-12 pd-10">15,000,000</td>
											            <td class="lt-bl fnt-12 pd-10">01/01/2021</td>
											         </tr>
											         <tr>
											            <td class="fnt-12 pd-10">Vickers Vantage Corp. I</td>
											            <td class="lt-bl fnt-12 pd-10">10.00</td>
											            <td class="lt-bl fnt-12 pd-10">15,000,000</td>
											            <td class="lt-bl fnt-12 pd-10">01/01/2021</td>
											         </tr>
											         <tr>
											            <td class="fnt-12 pd-10">Vickers Vantage Corp. I</td>
											            <td class="lt-bl fnt-12 pd-10">10.00</td>
											            <td class="lt-bl fnt-12 pd-10">15,000,000</td>
											            <td class="lt-bl fnt-12 pd-10">01/01/2021</td>
											         </tr>
											         <tr>
											            <td class="fnt-12 pd-10">Vickers Vantage Corp. I</td>
											            <td class="lt-bl fnt-12 pd-10">10.00</td>
											            <td class="lt-bl fnt-12 pd-10">15,000,000</td>
											            <td class="lt-bl fnt-12 pd-10">01/01/2021</td>
											         </tr>
											         <tr>
											            <td class="fnt-12 pd-10">Vickers Vantage Corp. I</td>
											            <td class="lt-bl fnt-12 pd-10">10.00</td>
											            <td class="lt-bl fnt-12 pd-10">15,000,000</td>
											            <td class="lt-bl fnt-12 pd-10">01/01/2021</td>
											         </tr>
											         <tr>
											            <td class="fnt-12 pd-10">Vickers Vantage Corp. I</td>
											            <td class="lt-bl fnt-12 pd-10">10.00</td>
											            <td class="lt-bl fnt-12 pd-10">15,000,000</td>
											            <td class="lt-bl fnt-12 pd-10">01/01/2021</td>
											         </tr>
											         <tr>
											            <td class="fnt-12 pd-10">Vickers Vantage Corp. I</td>
											            <td class="lt-bl fnt-12 pd-10">10.00</td>
											            <td class="lt-bl fnt-12 pd-10">15,000,000</td>
											            <td class="lt-bl fnt-12 pd-10">01/01/2021</td>
											         </tr>
											      </tbody>
											   </table>
											   <!-- Small Screen table -->
											   <table role="table" class="show-tb-sm">
                                                  <thead class="sm-resp-thead" role="rowgroup">
                                                    <tr class="sm-resp-tr" role="row">
                                                      <th class="sm-resp-th" role="columnheader">Name</th>
                                                      <th class="sm-resp-th" role="columnheader">Offering Price</th>
                                                      <th class="sm-resp-th" role="columnheader">Offering Shares</th>
                                                      <th class="sm-resp-th" role="columnheader">Date</th>
                                                    </tr>
                                                  </thead>
                                                  <tbody class="sm-resp-tbody" role="rowgroup">
                                                    <tr class="sm-resp-tr-1" role="row">
                                                      <td class="sm-resp-td" role="cell">Vickers Vantage Corp. I</td>
                                                      <td class="sm-resp-td" role="cell">10.00</td>
                                                      <td class="sm-resp-td" role="cell">15,000,000</td>
                                                      <td class="sm-resp-td" role="cell">01/01/2021</td>
                                                    </tr>
                                                    <tr class="sm-resp-tr-1" role="row">
                                                      <td class="sm-resp-td" role="cell">Vickers Vantage Corp. I</td>
                                                      <td class="sm-resp-td" role="cell">10.00</td>
                                                      <td class="sm-resp-td" role="cell">15,000,000</td>
                                                      <td class="sm-resp-td" role="cell">01/01/2021</td>
                                                    </tr>
                                                    <tr class="sm-resp-tr-1" role="row">
                                                      <td class="sm-resp-td" role="cell">Vickers Vantage Corp. I</td>
                                                      <td class="sm-resp-td" role="cell">10.00</td>
                                                      <td class="sm-resp-td" role="cell">15,000,000</td>
                                                      <td class="sm-resp-td" role="cell">01/01/2021</td>
                                                    </tr>
                                                    <tr class="sm-resp-tr-1" role="row">
                                                      <td class="sm-resp-td" role="cell">Vickers Vantage Corp. I</td>
                                                      <td class="sm-resp-td" role="cell">10.00</td>
                                                      <td class="sm-resp-td" role="cell">15,000,000</td>
                                                      <td class="sm-resp-td" role="cell">01/01/2021</td>
                                                    </tr>
                                                    <tr class="sm-resp-tr-1" role="row">
                                                      <td class="sm-resp-td" role="cell">Vickers Vantage Corp. I</td>
                                                      <td class="sm-resp-td" role="cell">10.00</td>
                                                      <td class="sm-resp-td" role="cell">15,000,000</td>
                                                      <td class="sm-resp-td" role="cell">01/01/2021</td>
                                                    </tr>
                                                    <tr class="sm-resp-tr-1" role="row">
                                                      <td class="sm-resp-td" role="cell">Vickers Vantage Corp. I</td>
                                                      <td class="sm-resp-td" role="cell">10.00</td>
                                                      <td class="sm-resp-td" role="cell">15,000,000</td>
                                                      <td class="sm-resp-td" role="cell">01/01/2021</td>
                                                    </tr>
                                                    <tr class="sm-resp-tr-1" role="row">
                                                      <td class="sm-resp-td" role="cell">Vickers Vantage Corp. I</td>
                                                      <td class="sm-resp-td" role="cell">10.00</td>
                                                      <td class="sm-resp-td" role="cell">15,000,000</td>
                                                      <td class="sm-resp-td" role="cell">01/01/2021</td>
                                                    </tr>
                                                    <tr class="sm-resp-tr-1" role="row">
                                                      <td class="sm-resp-td" role="cell">Vickers Vantage Corp. I</td>
                                                      <td class="sm-resp-td" role="cell">10.00</td>
                                                      <td class="sm-resp-td" role="cell">15,000,000</td>
                                                      <td class="sm-resp-td" role="cell">01/01/2021</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
											   <!-- Small Screen table End -->
											</div>
										</div>
									</div>		
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- 4th Screen End -->
			<!-- 5th Screen Start -->
			<div class="new-bg-1 market-pg-sec-6 market-pg-common">
				<div class="container">
					<div class="row">
						
						<div class="col-lg-12 col-md-12">
							<div class="row pd-60">
								<h2 class="sty-one">Crypto</h2>
								<div class="col-md-12" style=padding-left:0px;>
									<div class="tabs" style="padding: 30px 0px;">
									  <div class="tab-button-outer">
									    <ul id="tab-button">
									      <li class="fourth-cont-three"><a style="text-align: left;" href="#tab01">Favorite</a></li>
									      <li class="fourth-cont-three"><a href="#tab02">USDT</a></li>
									      <li class="fourth-cont-three"><a href="#tab03">BTC</a></li>
									      <li class="fourth-cont-three"><a href="#tab04">ETC</a></li>
									      <li class="fourth-cont-three"><a href="#tab05">ALTS</a></li> 
									    </ul>
									  </div>
									  <div class="tab-select-outer">
									    <select id="tab-select">
									      <option value="#tab01">Favorite</option>
									      <option value="#tab02">USDT</option>
									      <option value="#tab03">BTC</option>
									      <option value="#tab04">ETC</option>
									      <option value="#tab05">ALTS</option>
									    </select>
									  </div>
									  <div id="tab01" class="tab-contents fourth-cont-four">
									    <div class="row">
									    	<div class="col-md-12 col-lg-12">
									    		<table class="table show-tb-lg">
											      <thead>
											         <tr class="tr-40">
											            <th class="lt-ft-wt">Pairs</th>
											            <th class="lt-ft-wt">Latest Price</th>
											            <th class="lt-ft-wt">24H Change</th>
											            <th class="lt-ft-wt">24H High</th>
											            <th class="lt-ft-wt">24H Low</th>
											            <th class="lt-ft-wt">24H Vol</th>
											         </tr>
											      </thead>
											      <tbody>
											      	<tr>
											      		<td class="td-to-flex"><img src="{{asset('frontend/img/btc-icon/xmr.png')}}" class="img-pd-5" /><span class="bold-tbl"> XMR/</span>USDT</td>
											      		<td class="lt-bl"><span class="bold-tbl">0.1507</span><span class="p-to-small">≈$0.1</span></td>
											      		<td class="col-red">-2.57%</td>
											      		<td class="lt-bl">0.15615300</td>
											      		<td class="lt-bl">0.14900</td>
											      		<td class="lt-bl">2014148559</td>
											      	</tr>
											      	<tr>
											      		<td colspan="6" class="one"></td>
											      	</tr>
											      	<tr>
											      		<td class="td-to-flex"><img src="{{asset('frontend/img/btc-icon/btc.png')}}" class="img-pd-5" /><span class="bold-tbl"> BTC/</span>USDT</td>
											      		<td class="lt-bl"><span class="bold-tbl">0.1507</span><span class="p-to-small">≈$0.1</span></td>
											      		<td class="col-green">-2.57%</td>
											      		<td class="lt-bl">0.15615300</td>
											      		<td class="lt-bl">0.14900</td>
											      		<td class="lt-bl">2014148559</td>
											      	</tr>
											      	<tr>
											      		<td colspan="6" class="one"></td>
											      	</tr>
											      	<tr>
											      		<td class="td-to-flex"><img src="{{asset('frontend/img/btc-icon/eth.png')}}" class="img-pd-5" /><span class="bold-tbl"> ETH/</span>USDT</td>
											      		<td class="lt-bl"><span class="bold-tbl">0.1507</span><span class="p-to-small">≈$0.1</span></td>
											      		<td class="col-green">-2.57%</td>
											      		<td class="lt-bl">0.15615300</td>
											      		<td class="lt-bl">0.14900</td>
											      		<td class="lt-bl">2014148559</td>
											      	</tr>
											      	<tr>
											      		<td colspan="6" class="one"></td>
											      	</tr>
											      	<tr>
											      		<td class="td-to-flex"><img src="{{asset('frontend/img/btc-icon/ltc.png')}}" class="img-pd-5" /><span class="bold-tbl"> LTC/</span>USDT</td>
											      		<td class="lt-bl"><span class="bold-tbl">0.1507</span><span class="p-to-small">≈$0.1</span></td>
											      		<td class="col-red">-2.57%</td>
											      		<td class="lt-bl">0.15615300</td>
											      		<td class="lt-bl">0.14900</td>
											      		<td class="lt-bl">2014148559</td>
											      	</tr>
											      	<tr>
											      		<td colspan="6" class="one"></td>
											      	</tr>
											      	<tr>
											      		<td class="td-to-flex"><img src="{{asset('frontend/img/btc-icon/eos.png')}}" class="img-pd-5" /><span class="bold-tbl"> EOS/</span>USDT</td>
											      		<td class="lt-bl"><span class="bold-tbl">0.1507</span><span class="p-to-small">≈$0.1</span></td>
											      		<td class="col-red">-2.57%</td>
											      		<td class="lt-bl">0.15615300</td>
											      		<td class="lt-bl">0.14900</td>
											      		<td class="lt-bl">2014148559</td>
											      	</tr>
											      	<tr>
											      		<td colspan="6" class="one"></td>
											      	</tr>
											      	<tr>
											      		<td class="td-to-flex"><img src="{{asset('frontend/img/btc-icon/xrp.png')}}" class="img-pd-5" /><span class="bold-tbl"> XRP/</span>USDT</td>
											      		<td class="lt-bl"><span class="bold-tbl">0.1507</span><span class="p-to-small">≈$0.1</span></td>
											      		<td class="col-green">-2.57%</td>
											      		<td class="lt-bl">0.15615300</td>
											      		<td class="lt-bl">0.14900</td>
											      		<td class="lt-bl">2014148559</td>
											      	</tr>
											      	<tr>
											      		<td colspan="6" class="one"></td>
											      	</tr>
											      	<tr>
											      		<td class="td-to-flex"><img src="{{asset('frontend/img/btc-icon/btc-1.png')}}" class="img-pd-5" /><span class="bold-tbl"> BCH/</span>USDT</td>
											      		<td class="lt-bl"><span class="bold-tbl">0.1507</span><span class="p-to-small">≈$0.1</span></td>
											      		<td class="col-green">-2.57%</td>
											      		<td class="lt-bl">0.15615300</td>
											      		<td class="lt-bl">0.14900</td>
											      		<td class="lt-bl">2014148559</td>
											      	</tr>
											      	<tr>
											      		<td colspan="6" class="one"></td>
											      	</tr>
											      	<tr>
											      		<td class="td-to-flex"><img src="{{asset('frontend/img/btc-icon/btc-2.png')}}" class="img-pd-5" /><span class="bold-tbl"> BSV/</span>USDT</td>
											      		<td class="lt-bl"><span class="bold-tbl">0.1507</span><span class="p-to-small">≈$0.1</span></td>
											      		<td class="col-green">-2.57%</td>
											      		<td class="lt-bl">0.15615300</td>
											      		<td class="lt-bl">0.14900</td>
											      		<td class="lt-bl">2014148559</td>
											      	</tr>
											      	<tr>
											      		<td colspan="6" class="one"></td>
											      	</tr>
											      	<tr>
											      		<td class="td-to-flex"><img src="{{asset('frontend/img/btc-icon/dot.png')}}" class="img-pd-5" /><span class="bold-tbl"> DOT/</span>USDT</td>
											      		<td class="lt-bl"><span class="bold-tbl">0.1507</span><span class="p-to-small">≈$0.1</span></td>
											      		<td class="col-red">-2.57%</td>
											      		<td class="lt-bl">0.15615300</td>
											      		<td class="lt-bl">0.14900</td>
											      		<td class="lt-bl">2014148559</td>
											      	</tr>
											      	<tr>
											      		<td colspan="6" class="one"></td>
											      	</tr>
											      	<tr>
											      		<td class="td-to-flex"><img src="{{asset('frontend/img/btc-icon/dash.png')}}" class="img-pd-5" /><span class="bold-tbl"> DASH/</span>USDT</td>
											      		<td class="lt-bl"><span class="bold-tbl">0.1507</span><span class="p-to-small">≈$0.1</span></td>
											      		<td class="col-red">-2.57%</td>
											      		<td class="lt-bl">0.15615300</td>
											      		<td class="lt-bl">0.14900</td>
											      		<td class="lt-bl">2014148559</td>
											      	</tr>
											      	<tr>
											      		<td colspan="6" class="one"></td>
											      	</tr>
											      </tbody>
											  	</table>
											  	<!-- Small Screen table -->
											   <table role="table" class="show-tb-sm">
                                                  <thead class="sm-resp-thead" role="rowgroup">
                                                    <tr class="sm-resp-tr" role="row">
                                                      <th class="sm-resp-th" role="columnheader">Pairs</th>
                                                      <th class="sm-resp-th" role="columnheader">Latest Price</th>
                                                      <th class="sm-resp-th" role="columnheader">24H Change</th>
                                                      <th class="sm-resp-th" role="columnheader">24H High</th>
                                                      <th class="sm-resp-th" role="columnheader">24H Low</th>
                                                      <th class="sm-resp-th" role="columnheader">24H Vol</th>
                                                    </tr>
                                                  </thead>
                                                  <tbody class="sm-resp-tbody" role="rowgroup">
                                                    <tr class="sm-resp-tr-1" role="row">
                                                        <td class="sm-resp-td-1" role="cell"><img src="{{asset('frontend/img/btc-icon/xmr.png')}}" class="img-pd-5" /><span class="bold-tbl"> XMR/</span>USDT</td>
											      		<td class="sm-resp-td-1" role="cell"><span class="bold-tbl">0.1507</span>≈$0.1</td>
											      		<td class="sm-resp-td-1 col-red" role="cell">-2.57%</td>
											      		<td class="sm-resp-td-1" role="cell">0.15615300</td>
											      		<td class="sm-resp-td-1" role="cell">0.14900</td>
											      		<td class="sm-resp-td-1" role="cell">2014148559</td>
                                                    </tr>
                                                    <tr class="sm-resp-tr-1">
											      		<td colspan="6" class="one"></td>
											      	</tr>
                                                    <tr class="sm-resp-tr-1" role="row">
                                                        <td class="sm-resp-td-1" role="cell"><img src="{{asset('frontend/img/btc-icon/btc.png')}}" class="img-pd-5" /><span class="bold-tbl"> XMR/</span>USDT</td>
											      		<td class="sm-resp-td-1" role="cell"><span class="bold-tbl">0.1507</span>≈$0.1</td>
											      		<td class="sm-resp-td-1 col-green" role="cell">-2.57%</td>
											      		<td class="sm-resp-td-1" role="cell">0.15615300</td>
											      		<td class="sm-resp-td-1" role="cell">0.14900</td>
											      		<td class="sm-resp-td-1" role="cell">2014148559</td>
                                                    </tr>
                                                    <tr class="sm-resp-tr-1">
											      		<td colspan="6" class="one"></td>
											      	</tr>
                                                    <tr class="sm-resp-tr-1" role="row">
                                                        <td class="sm-resp-td-1" role="cell"><img src="{{asset('frontend/img/btc-icon/eth.png')}}" class="img-pd-5" /><span class="bold-tbl"> XMR/</span>USDT</td>
											      		<td class="sm-resp-td-1" role="cell"><span class="bold-tbl">0.1507</span>≈$0.1</td>
											      		<td class="sm-resp-td-1 col-green" role="cell">-2.57%</td>
											      		<td class="sm-resp-td-1" role="cell">0.15615300</td>
											      		<td class="sm-resp-td-1" role="cell">0.14900</td>
											      		<td class="sm-resp-td-1" role="cell">2014148559</td>
                                                    </tr>
                                                    <tr class="sm-resp-tr-1">
											      		<td colspan="6" class="one"></td>
											      	</tr>
                                                    <tr class="sm-resp-tr-1" role="row">
                                                        <td class="sm-resp-td-1" role="cell"><img src="{{asset('frontend/img/btc-icon/ltc.png')}}" class="img-pd-5" /><span class="bold-tbl"> XMR/</span>USDT</td>
											      		<td class="sm-resp-td-1" role="cell"><span class="bold-tbl">0.1507</span>≈$0.1</td>
											      		<td class="sm-resp-td-1 col-red" role="cell">-2.57%</td>
											      		<td class="sm-resp-td-1" role="cell">0.15615300</td>
											      		<td class="sm-resp-td-1" role="cell">0.14900</td>
											      		<td class="sm-resp-td-1" role="cell">2014148559</td>
                                                    </tr>
                                                    <tr class="sm-resp-tr-1">
											      		<td colspan="6" class="one"></td>
											      	</tr>
                                                    <tr class="sm-resp-tr-1" role="row">
                                                        <td class="sm-resp-td-1" role="cell"><img src="{{asset('frontend/img/btc-icon/eos.png')}}" class="img-pd-5" /><span class="bold-tbl"> XMR/</span>USDT</td>
											      		<td class="sm-resp-td-1" role="cell"><span class="bold-tbl">0.1507</span>≈$0.1</td>
											      		<td class="sm-resp-td-1 col-green" role="cell">-2.57%</td>
											      		<td class="sm-resp-td-1" role="cell">0.15615300</td>
											      		<td class="sm-resp-td-1" role="cell">0.14900</td>
											      		<td class="sm-resp-td-1" role="cell">2014148559</td>
                                                    </tr>
                                                    <tr class="sm-resp-tr-1">
											      		<td colspan="6" class="one"></td>
											      	</tr>
                                                    <tr class="sm-resp-tr-1" role="row">
                                                        <td class="sm-resp-td-1" role="cell"><img src="{{asset('frontend/img/btc-icon/xrp.png')}}" class="img-pd-5" /><span class="bold-tbl"> XMR/</span>USDT</td>
											      		<td class="sm-resp-td-1" role="cell"><span class="bold-tbl">0.1507</span>≈$0.1</td>
											      		<td class="sm-resp-td-1 col-red" role="cell">-2.57%</td>
											      		<td class="sm-resp-td-1" role="cell">0.15615300</td>
											      		<td class="sm-resp-td-1" role="cell">0.14900</td>
											      		<td class="sm-resp-td-1" role="cell">2014148559</td>
                                                    </tr>
                                                    <tr class="sm-resp-tr-1">
											      		<td colspan="6" class="one"></td>
											      	</tr>
                                                    <tr class="sm-resp-tr-1" role="row">
                                                        <td class="sm-resp-td-1" role="cell"><img src="{{asset('frontend/img/btc-icon/btc-1.png')}}" class="img-pd-5" /><span class="bold-tbl"> XMR/</span>USDT</td>
											      		<td class="sm-resp-td-1" role="cell"><span class="bold-tbl">0.1507</span>≈$0.1</td>
											      		<td class="sm-resp-td-1 col-green" role="cell">-2.57%</td>
											      		<td class="sm-resp-td-1" role="cell">0.15615300</td>
											      		<td class="sm-resp-td-1" role="cell">0.14900</td>
											      		<td class="sm-resp-td-1" role="cell">2014148559</td>
                                                    </tr>
                                                    <tr class="sm-resp-tr-1">
											      		<td colspan="6" class="one"></td>
											      	</tr>
                                                    <tr class="sm-resp-tr-1" role="row">
                                                        <td class="sm-resp-td-1" role="cell"><img src="{{asset('frontend/img/btc-icon/btc-2.png')}}" class="img-pd-5" /><span class="bold-tbl"> XMR/</span>USDT</td>
											      		<td class="sm-resp-td-1" role="cell"><span class="bold-tbl">0.1507</span>≈$0.1</td>
											      		<td class="sm-resp-td-1 col-green" role="cell">-2.57%</td>
											      		<td class="sm-resp-td-1" role="cell">0.15615300</td>
											      		<td class="sm-resp-td-1" role="cell">0.14900</td>
											      		<td class="sm-resp-td-1" role="cell">2014148559</td>
                                                    </tr>
                                                    <tr class="sm-resp-tr-1">
											      		<td colspan="6" class="one"></td>
											      	</tr>
                                                    <tr class="sm-resp-tr-1" role="row">
                                                        <td class="sm-resp-td-1" role="cell"><img src="{{asset('frontend/img/btc-icon/dot.png')}}" class="img-pd-5" /><span class="bold-tbl"> XMR/</span>USDT</td>
											      		<td class="sm-resp-td-1" role="cell"><span class="bold-tbl">0.1507</span>≈$0.1</td>
											      		<td class="sm-resp-td-1 col-red" role="cell">-2.57%</td>
											      		<td class="sm-resp-td-1" role="cell">0.15615300</td>
											      		<td class="sm-resp-td-1" role="cell">0.14900</td>
											      		<td class="sm-resp-td-1" role="cell">2014148559</td>
                                                    </tr>
                                                    <tr class="sm-resp-tr-1">
											      		<td colspan="6" class="one"></td>
											      	</tr>
                                                    <tr class="sm-resp-tr-1" role="row">
                                                        <td class="sm-resp-td-1" role="cell"><img src="{{asset('frontend/img/btc-icon/dash.png')}}" class="img-pd-5" /><span class="bold-tbl"> XMR/</span>USDT</td>
											      		<td class="sm-resp-td-1" role="cell"><span class="bold-tbl">0.1507</span>≈$0.1</td>
											      		<td class="sm-resp-td-1 col-green" role="cell">-2.57%</td>
											      		<td class="sm-resp-td-1" role="cell">0.15615300</td>
											      		<td class="sm-resp-td-1" role="cell">0.14900</td>
											      		<td class="sm-resp-td-1" role="cell">2014148559</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
											   <!-- Small Screen table End -->
									    	</div>
									    </div>
									  </div>
									  <div id="tab02" class="tab-contents fourth-cont-four">
									    <div class="row">
									    	<div class="col-md-12 col-lg-12">
									    		<table class="table show-tb-lg">
											      <thead>
											         <tr class="tr-40">
											            <th>Pairs</th>
											            <th>Latest Price</th>
											            <th>24H Change</th>
											            <th>24H High</th>
											            <th>24H Low</th>
											            <th>24H Vol</th>
											         </tr>
											      </thead>
											      <tbody>
											      	<tr>
											      		<td><img src="{{asset('frontend/img/btc-icon/eos.png')}}" class="img-pd-5" /><span class="bold-tbl"> EOS/</span>USDT</td>
											      		<td class="lt-bl"><span class="bold-tbl">0.1507</span>≈$0.1</td>
											      		<td class="col-red">-2.57%</td>
											      		<td class="lt-bl">0.15615300</td>
											      		<td class="lt-bl">0.14900</td>
											      		<td class="lt-bl">2014148559</td>
											      	</tr>
											      	<tr>
											      		<td colspan="6" class="one"></td>
											      	</tr>
											      	<tr>
											      		<td><img src="{{asset('frontend/img/btc-icon/xrp.png')}}" class="img-pd-5" /> <span class="bold-tbl">XRP/</span>USDT</td>
											      		<td class="lt-bl"><span class="bold-tbl">0.1507</span>≈$0.1</td>
											      		<td class="col-green">-2.57%</td>
											      		<td class="lt-bl">0.15615300</td>
											      		<td class="lt-bl">0.14900</td>
											      		<td class="lt-bl">2014148559</td>
											      	</tr>
											      	<tr>
											      		<td colspan="6" class="one"></td>
											      	</tr>
											      	<tr>
											      		<td><img src="{{asset('frontend/img/btc-icon/btc-1.png')}}" class="img-pd-5" /><span class="bold-tbl"> BCH/</span>USDT</td>
											      		<td class="lt-bl"><span class="bold-tbl">0.1507</span>≈$0.1</td>
											      		<td class="col-green">-2.57%</td>
											      		<td class="lt-bl">0.15615300</td>
											      		<td class="lt-bl">0.14900</td>
											      		<td class="lt-bl">2014148559</td>
											      	</tr>
											      	<tr>
											      		<td colspan="6" class="one"></td>
											      	</tr>
											      	<tr>
											      		<td><img src="{{asset('frontend/img/btc-icon/btc-2.png')}}" class="img-pd-5" /><span class="bold-tbl"> BSV/</span>USDT</td>
											      		<td class="lt-bl"><span class="bold-tbl">0.1507</span>≈$0.1</td>
											      		<td class="col-green">-2.57%</td>
											      		<td class="lt-bl">0.15615300</td>
											      		<td class="lt-bl">0.14900</td>
											      		<td class="lt-bl">2014148559</td>
											      	</tr>
											      	<tr>
											      		<td colspan="6" class="one"></td>
											      	</tr>
											      	<tr>
											      		<td><img src="{{asset('frontend/img/btc-icon/dot.png')}}" class="img-pd-5" /><span class="bold-tbl"> DOT/</span>USDT</td>
											      		<td class="lt-bl"><span class="bold-tbl">0.1507</span>≈$0.1</td>
											      		<td class="col-red">-2.57%</td>
											      		<td class="lt-bl">0.15615300</td>
											      		<td class="lt-bl">0.14900</td>
											      		<td class="lt-bl">2014148559</td>
											      	</tr>
											      	<tr>
											      		<td colspan="6" class="one"></td>
											      	</tr>
											      	<tr>
											      		<td><img src="{{asset('frontend/img/btc-icon/dash.png')}}" class="img-pd-5" /><span class="bold-tbl"> DASH/</span>USDT</td>
											      		<td class="lt-bl"><span class="bold-tbl">0.1507</span>≈$0.1</td>
											      		<td class="col-red">-2.57%</td>
											      		<td class="lt-bl">0.15615300</td>
											      		<td class="lt-bl">0.14900</td>
											      		<td class="lt-bl">2014148559</td>
											      	</tr>
											      	<tr>
											      		<td colspan="6" class="one"></td>
											      	</tr>
											      </tbody>
											  	</table>
											  	<!-- Small Screen table -->
											   <table role="table" class="show-tb-sm">
                                                  <thead class="sm-resp-thead" role="rowgroup">
                                                    <tr class="sm-resp-tr" role="row">
                                                      <th class="sm-resp-th" role="columnheader">Pairs</th>
                                                      <th class="sm-resp-th" role="columnheader">Latest Price</th>
                                                      <th class="sm-resp-th" role="columnheader">24H Change</th>
                                                      <th class="sm-resp-th" role="columnheader">24H High</th>
                                                      <th class="sm-resp-th" role="columnheader">24H Low</th>
                                                      <th class="sm-resp-th" role="columnheader">24H Vol</th>
                                                    </tr>
                                                  </thead>
                                                  <tbody class="sm-resp-tbody" role="rowgroup">
                                                    <tr class="sm-resp-tr-1" role="row">
                                                        <td class="sm-resp-td-1" role="cell"><img src="{{asset('frontend/img/btc-icon/ltc.png')}}" class="img-pd-5" /><span class="bold-tbl"> XMR/</span>USDT</td>
											      		<td class="sm-resp-td-1" role="cell"><span class="bold-tbl">0.1507</span>≈$0.1</td>
											      		<td class="sm-resp-td-1 col-red" role="cell">-2.57%</td>
											      		<td class="sm-resp-td-1" role="cell">0.15615300</td>
											      		<td class="sm-resp-td-1" role="cell">0.14900</td>
											      		<td class="sm-resp-td-1" role="cell">2014148559</td>
                                                    </tr>
                                                    <tr class="sm-resp-tr-1">
											      		<td colspan="6" class="one"></td>
											      	</tr>
                                                    <tr class="sm-resp-tr-1" role="row">
                                                        <td class="sm-resp-td-1" role="cell"><img src="{{asset('frontend/img/btc-icon/eos.png')}}" class="img-pd-5" /><span class="bold-tbl"> XMR/</span>USDT</td>
											      		<td class="sm-resp-td-1" role="cell"><span class="bold-tbl">0.1507</span>≈$0.1</td>
											      		<td class="sm-resp-td-1 col-green" role="cell">-2.57%</td>
											      		<td class="sm-resp-td-1" role="cell">0.15615300</td>
											      		<td class="sm-resp-td-1" role="cell">0.14900</td>
											      		<td class="sm-resp-td-1" role="cell">2014148559</td>
                                                    </tr>
                                                    <tr class="sm-resp-tr-1">
											      		<td colspan="6" class="one"></td>
											      	</tr>
                                                    <tr class="sm-resp-tr-1" role="row">
                                                        <td class="sm-resp-td-1" role="cell"><img src="{{asset('frontend/img/btc-icon/xrp.png')}}" class="img-pd-5" /><span class="bold-tbl"> XMR/</span>USDT</td>
											      		<td class="sm-resp-td-1" role="cell"><span class="bold-tbl">0.1507</span>≈$0.1</td>
											      		<td class="sm-resp-td-1 col-red" role="cell">-2.57%</td>
											      		<td class="sm-resp-td-1" role="cell">0.15615300</td>
											      		<td class="sm-resp-td-1" role="cell">0.14900</td>
											      		<td class="sm-resp-td-1" role="cell">2014148559</td>
                                                    </tr>
                                                    <tr class="sm-resp-tr-1">
											      		<td colspan="6" class="one"></td>
											      	</tr>
                                                    <tr class="sm-resp-tr-1" role="row">
                                                        <td class="sm-resp-td-1" role="cell"><img src="{{asset('frontend/img/btc-icon/btc-1.png')}}" class="img-pd-5" /><span class="bold-tbl"> XMR/</span>USDT</td>
											      		<td class="sm-resp-td-1" role="cell"><span class="bold-tbl">0.1507</span>≈$0.1</td>
											      		<td class="sm-resp-td-1 col-green" role="cell">-2.57%</td>
											      		<td class="sm-resp-td-1" role="cell">0.15615300</td>
											      		<td class="sm-resp-td-1" role="cell">0.14900</td>
											      		<td class="sm-resp-td-1" role="cell">2014148559</td>
                                                    </tr>
                                                    <tr class="sm-resp-tr-1">
											      		<td colspan="6" class="one"></td>
											      	</tr>
                                                    <tr class="sm-resp-tr-1" role="row">
                                                        <td class="sm-resp-td-1" role="cell"><img src="{{asset('frontend/img/btc-icon/btc-2.png')}}" class="img-pd-5" /><span class="bold-tbl"> XMR/</span>USDT</td>
											      		<td class="sm-resp-td-1" role="cell"><span class="bold-tbl">0.1507</span>≈$0.1</td>
											      		<td class="sm-resp-td-1 col-green" role="cell">-2.57%</td>
											      		<td class="sm-resp-td-1" role="cell">0.15615300</td>
											      		<td class="sm-resp-td-1" role="cell">0.14900</td>
											      		<td class="sm-resp-td-1" role="cell">2014148559</td>
                                                    </tr>
                                                    <tr class="sm-resp-tr-1">
											      		<td colspan="6" class="one"></td>
											      	</tr>
                                                    <tr class="sm-resp-tr-1" role="row">
                                                        <td class="sm-resp-td-1" role="cell"><img src="{{asset('frontend/img/btc-icon/dot.png')}}" class="img-pd-5" /><span class="bold-tbl"> XMR/</span>USDT</td>
											      		<td class="sm-resp-td-1" role="cell"><span class="bold-tbl">0.1507</span>≈$0.1</td>
											      		<td class="sm-resp-td-1 col-red" role="cell">-2.57%</td>
											      		<td class="sm-resp-td-1" role="cell">0.15615300</td>
											      		<td class="sm-resp-td-1" role="cell">0.14900</td>
											      		<td class="sm-resp-td-1" role="cell">2014148559</td>
                                                    </tr>
                                                    <tr class="sm-resp-tr-1">
											      		<td colspan="6" class="one"></td>
											      	</tr>
                                                    <tr class="sm-resp-tr-1" role="row">
                                                        <td class="sm-resp-td-1" role="cell"><img src="{{asset('frontend/img/btc-icon/dash.png')}}" class="img-pd-5" /><span class="bold-tbl"> XMR/</span>USDT</td>
											      		<td class="sm-resp-td-1" role="cell"><span class="bold-tbl">0.1507</span>≈$0.1</td>
											      		<td class="sm-resp-td-1 col-green" role="cell">-2.57%</td>
											      		<td class="sm-resp-td-1" role="cell">0.15615300</td>
											      		<td class="sm-resp-td-1" role="cell">0.14900</td>
											      		<td class="sm-resp-td-1" role="cell">2014148559</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
											   <!-- Small Screen table End -->
									    	</div>
									    </div>
									  </div>
									  <div id="tab03" class="tab-contents fourth-cont-four">
									    <div class="row">
									    	<div class="col-md-12 col-lg-12">
									    		<table class="table show-tb-lg">
											      <thead>
											         <tr class="tr-40">
											            <th>Pairs</th>
											            <th>Latest Price</th>
											            <th>24H Change</th>
											            <th>24H High</th>
											            <th>24H Low</th>
											            <th>24H Vol</th>
											         </tr>
											      </thead>
											      <tbody>
											      	<tr>
											      		<td><img src="{{asset('frontend/img/btc-icon/eos.png')}}" class="img-pd-5" /><span class="bold-tbl"> EOS/</span>USDT</td>
											      		<td class="lt-bl"><span class="bold-tbl">0.1507</span>≈$0.1</td>
											      		<td class="col-red">-2.57%</td>
											      		<td class="lt-bl">0.15615300</td>
											      		<td class="lt-bl">0.14900</td>
											      		<td class="lt-bl">2014148559</td>
											      	</tr>
											      	<tr>
											      		<td colspan="6" class="one"></td>
											      	</tr>
											      	<tr>
											      		<td><img src="{{asset('frontend/img/btc-icon/xrp.png')}}" class="img-pd-5" /><span class="bold-tbl"> XRP/</span>USDT</td>
											      		<td class="lt-bl"><span class="bold-tbl">0.1507</span>≈$0.1</td>
											      		<td class="col-green">-2.57%</td>
											      		<td class="lt-bl">0.15615300</td>
											      		<td class="lt-bl">0.14900</td>
											      		<td class="lt-bl">2014148559</td>
											      	</tr>
											      	<tr>
											      		<td colspan="6" class="one"></td>
											      	</tr>
											      	<tr>
											      		<td><img src="{{asset('frontend/img/btc-icon/btc-1.png')}}" class="img-pd-5" /><span class="bold-tbl"> BCH/</span>USDT</td>
											      		<td class="lt-bl"><span class="bold-tbl">0.1507</span>≈$0.1</td>
											      		<td class="col-green">-2.57%</td>
											      		<td class="lt-bl">0.15615300</td>
											      		<td class="lt-bl">0.14900</td>
											      		<td class="lt-bl">2014148559</td>
											      	</tr>
											      	<tr>
											      		<td colspan="6" class="one"></td>
											      	</tr>
											      	<tr>
											      		<td><img src="{{asset('frontend/img/btc-icon/dash.png')}}" class="img-pd-5" /><span class="bold-tbl"> DASH/</span>USDT</td>
											      		<td class="lt-bl"><span class="bold-tbl">0.1507</span>≈$0.1</td>
											      		<td class="col-red">-2.57%</td>
											      		<td class="lt-bl">0.15615300</td>
											      		<td class="lt-bl">0.14900</td>
											      		<td class="lt-bl">2014148559</td>
											      	</tr>
											      	<tr>
											      		<td colspan="6" class="one"></td>
											      	</tr>
											      </tbody>
											  	</table>
											  	<!-- Small Screen table -->
											   <table role="table" class="show-tb-sm">
                                                  <thead class="sm-resp-thead" role="rowgroup">
                                                    <tr class="sm-resp-tr" role="row">
                                                      <th class="sm-resp-th" role="columnheader">Pairs</th>
                                                      <th class="sm-resp-th" role="columnheader">Latest Price</th>
                                                      <th class="sm-resp-th" role="columnheader">24H Change</th>
                                                      <th class="sm-resp-th" role="columnheader">24H High</th>
                                                      <th class="sm-resp-th" role="columnheader">24H Low</th>
                                                      <th class="sm-resp-th" role="columnheader">24H Vol</th>
                                                    </tr>
                                                  </thead>
                                                  <tbody class="sm-resp-tbody" role="rowgroup">
                                                    <tr class="sm-resp-tr-1" role="row">
                                                        <td class="sm-resp-td-1" role="cell"><img src="{{asset('frontend/img/btc-icon/ltc.png')}}" class="img-pd-5" /><span class="bold-tbl"> XMR/</span>USDT</td>
											      		<td class="sm-resp-td-1" role="cell"><span class="bold-tbl">0.1507</span>≈$0.1</td>
											      		<td class="sm-resp-td-1 col-red" role="cell">-2.57%</td>
											      		<td class="sm-resp-td-1" role="cell">0.15615300</td>
											      		<td class="sm-resp-td-1" role="cell">0.14900</td>
											      		<td class="sm-resp-td-1" role="cell">2014148559</td>
                                                    </tr>
                                                    <tr class="sm-resp-tr-1">
											      		<td colspan="6" class="one"></td>
											      	</tr>
                                                    <tr class="sm-resp-tr-1" role="row">
                                                        <td class="sm-resp-td-1" role="cell"><img src="{{asset('frontend/img/btc-icon/eos.png')}}" class="img-pd-5" /><span class="bold-tbl"> XMR/</span>USDT</td>
											      		<td class="sm-resp-td-1" role="cell"><span class="bold-tbl">0.1507</span>≈$0.1</td>
											      		<td class="sm-resp-td-1 col-green" role="cell">-2.57%</td>
											      		<td class="sm-resp-td-1" role="cell">0.15615300</td>
											      		<td class="sm-resp-td-1" role="cell">0.14900</td>
											      		<td class="sm-resp-td-1" role="cell">2014148559</td>
                                                    </tr>
                                                    <tr class="sm-resp-tr-1">
											      		<td colspan="6" class="one"></td>
											      	</tr>
                                                    <tr class="sm-resp-tr-1" role="row">
                                                        <td class="sm-resp-td-1" role="cell"><img src="{{asset('frontend/img/btc-icon/xrp.png')}}" class="img-pd-5" /><span class="bold-tbl"> XMR/</span>USDT</td>
											      		<td class="sm-resp-td-1" role="cell"><span class="bold-tbl">0.1507</span>≈$0.1</td>
											      		<td class="sm-resp-td-1 col-red" role="cell">-2.57%</td>
											      		<td class="sm-resp-td-1" role="cell">0.15615300</td>
											      		<td class="sm-resp-td-1" role="cell">0.14900</td>
											      		<td class="sm-resp-td-1" role="cell">2014148559</td>
                                                    </tr>
                                                    <tr class="sm-resp-tr-1">
											      		<td colspan="6" class="one"></td>
											      	</tr>
                                                    <tr class="sm-resp-tr-1" role="row">
                                                        <td class="sm-resp-td-1" role="cell"><img src="{{asset('frontend/img/btc-icon/btc-1.png')}}" class="img-pd-5" /><span class="bold-tbl"> XMR/</span>USDT</td>
											      		<td class="sm-resp-td-1" role="cell"><span class="bold-tbl">0.1507</span>≈$0.1</td>
											      		<td class="sm-resp-td-1 col-green" role="cell">-2.57%</td>
											      		<td class="sm-resp-td-1" role="cell">0.15615300</td>
											      		<td class="sm-resp-td-1" role="cell">0.14900</td>
											      		<td class="sm-resp-td-1" role="cell">2014148559</td>
                                                    </tr>
                                                    <tr class="sm-resp-tr-1">
											      		<td colspan="6" class="one"></td>
											      	</tr>
                                                    <tr class="sm-resp-tr-1" role="row">
                                                        <td class="sm-resp-td-1" role="cell"><img src="{{asset('frontend/img/btc-icon/btc-2.png')}}" class="img-pd-5" /><span class="bold-tbl"> XMR/</span>USDT</td>
											      		<td class="sm-resp-td-1" role="cell"><span class="bold-tbl">0.1507</span>≈$0.1</td>
											      		<td class="sm-resp-td-1 col-green" role="cell">-2.57%</td>
											      		<td class="sm-resp-td-1" role="cell">0.15615300</td>
											      		<td class="sm-resp-td-1" role="cell">0.14900</td>
											      		<td class="sm-resp-td-1" role="cell">2014148559</td>
                                                    </tr>
                                                    <tr class="sm-resp-tr-1">
											      		<td colspan="6" class="one"></td>
											      	</tr>
                                                    <tr class="sm-resp-tr-1" role="row">
                                                        <td class="sm-resp-td-1" role="cell"><img src="{{asset('frontend/img/btc-icon/dot.png')}}" class="img-pd-5" /><span class="bold-tbl"> XMR/</span>USDT</td>
											      		<td class="sm-resp-td-1" role="cell"><span class="bold-tbl">0.1507</span>≈$0.1</td>
											      		<td class="sm-resp-td-1 col-red" role="cell">-2.57%</td>
											      		<td class="sm-resp-td-1" role="cell">0.15615300</td>
											      		<td class="sm-resp-td-1" role="cell">0.14900</td>
											      		<td class="sm-resp-td-1" role="cell">2014148559</td>
                                                    </tr>
                                                    <tr class="sm-resp-tr-1">
											      		<td colspan="6" class="one"></td>
											      	</tr>
                                                    <tr class="sm-resp-tr-1" role="row">
                                                        <td class="sm-resp-td-1" role="cell"><img src="{{asset('frontend/img/btc-icon/dash.png')}}" class="img-pd-5" /><span class="bold-tbl"> XMR/</span>USDT</td>
											      		<td class="sm-resp-td-1" role="cell"><span class="bold-tbl">0.1507</span>≈$0.1</td>
											      		<td class="sm-resp-td-1 col-green" role="cell">-2.57%</td>
											      		<td class="sm-resp-td-1" role="cell">0.15615300</td>
											      		<td class="sm-resp-td-1" role="cell">0.14900</td>
											      		<td class="sm-resp-td-1" role="cell">2014148559</td>
                                                    </tr>
                                                  </tbody>
                                                </table>
											   <!-- Small Screen table End -->
									    	</div>
									    </div>
									  </div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- 5th Screen End -->
			<!-- 6th Screen Start -->
			
			<!-- 6th Screen End -->

			<!-- 7th Screen Start -->
			
			<!-- 7th Screen End -->

			<!-- 8th Screen Start -->
			
			<!-- 8th Screen End -->

			@include('frontend.partials.footer')

		</div>
@endsection
@push('scripts')
<script type="text/javascript">
	$(document).ready(function() {

   
   // inspired by http://jsfiddle.net/arunpjohny/564Lxosz/1/
   $('.table-responsive-stack').each(function (i) {
      var id = $(this).attr('id');
      //alert(id);
      $(this).find("th").each(function(i) {
         $('#'+id + ' td:nth-child(' + (i + 1) + ')').prepend('<span class="table-responsive-stack-thead">'+             $(this).text() + ':</span> ');
         $('.table-responsive-stack-thead').hide();
         
      });
   });   
$( '.table-responsive-stack' ).each(function() {
  var thCount = $(this).find("th").length; 
   var rowGrow = 100 / thCount + '%';
   //console.log(rowGrow);
   $(this).find("th, td").css('flex-basis', rowGrow);   
});
function flexTable(){
   if ($(window).width() < 768) {
      
   $(".table-responsive-stack").each(function (i) {
      $(this).find(".table-responsive-stack-thead").show();
      $(this).find('thead').hide();
   });
      
    
   // window is less than 768px   
   } else {
      
      
   $(".table-responsive-stack").each(function (i) {
      $(this).find(".table-responsive-stack-thead").hide();
      $(this).find('thead').show();
   });

   }
// flextable   
}      
 
flexTable();
   
window.onresize = function(event) {
    flexTable();
};
  
// document ready  
});

$(document).ready(function(){
	$('body').addClass("market-page");

});

</script>
@endpush