@extends('frontend.layouts.app')
@push('styles')
<title>{{@$FrontPage->title}}</title>
<meta name="title" content="{{@$FrontPage->meta_title}}">
<meta name="keywords" content="{{@$FrontPage->meta_keyword}}">
<meta name="description" content="{{@$FrontPage->meta_description}}">
<link href="{{asset('frontend/css/about-us.css')}}" rel="stylesheet">
<style type="text/css">
    .main-bg{
        background-image: url({{asset('frontend/img/bg.jpg')}});
        background-position: center center;
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-size: cover;
        background-color: #464646;
        height:100vh;
    }
  .nav-a-margin{
      margin-left: 10px;
      margin-right: 10px;
  }
  .dark-light-btn{
      
      position: absolute;
  }
  .pd-0
  {
      padding-right: 0px;
      padding-left: 0px;
  }
  .red {
      border: 3px solid red;
  }
</style>
@endpush
@section('content')
@include('frontend.partials.darklight')
<div class="container-fluid pd-0">
    <div class="about-us-bg">
        @include('frontend.partials.navbar')
        <div class="second-cont row">
            <div class="col-lg-4 col-md-12 p-0">
                <img class="whoweare-img" src="{{asset('frontend/img/how-we-are-img.png')}}">
            </div>
            <div class="col-lg-8 col-md-12 whoweare-content">
                <h3 class="whoweare-headng">{{@$content->section1_title}}</h3>
                <p class="whoweare-p1">{!! @$content->section1_description !!}</p>
                {{-- <p class="whoweare-p1">We are a financial company with the customer at heart, the internet as our foundation, and technology as our lifeblood.</p>
                <p class="whoweare-p2">Our leadership has extensive experience in both the internet and financial industries. We are committed to synergizing technology with finance by providing reliable, professional, intelligent and efficient products and services. Enjoy Tech. Enjoy Investing.</p> --}}
            </div>
        </div>
        <div class="third-cont row">
            <div class="col-lg-8 col-md-12 ourbelief-content">
                <h3 class="ourbelief-headng">{{@$content->section2_title}}</h3>
                <p class="ourbelief-p1">{!! @$content->section2_description !!}</p>
                {{-- <p class="ourbelief-p1">Individuals are an important part of the market and should not be ignored. They should be empowered with better information, tools, services, opportunities, and lower costs. Respecting the investor is respecting the market.</p>
                <p class="ourbelief-p2">Technology is the investor’s best friend. It vastly expands the human’s trading capabilities in terms of time, scale, and technique. Technology is the future.</p> --}}
            </div>
            <div class="col-lg-4 col-md-12 p-0">
                <img class="ourbelief-img" src="{{asset('frontend/img/ourbelief-img.png')}}">
            </div>
        </div>
        <div class="fourth-cont row">
            <div class="col-lg-4 col-md-12 p-0">
                <img class="whatweoffer-img" src="{{asset('frontend/img/whatweoffer-img.png')}}">
            </div>
            <div class="col-lg-8 col-md-12 whatweoffer-content">
                <h3 class="whatweoffer-headng">{{@$content->section3_title}}</h3>
                <p class="whatweoffer-p1">{!! @$content->section3_description !!}</p>
                {{-- <p class="whatweoffer-p1">
                    As a financial company driven by technology, we aim to offer:
                    An all-in-one self-directed investment platform that provides excellent user experience
                    Advanced and intelligent tools and services
                    <br>
                    Advanced and intelligent tools and services
                </p>
                <p class="whatweoffer-list-headng">Key Features:</p>
                <ul class="whatweoffer-lists">
                    <li>Zero Commission</li>
                    <li>Free Real-Time Quotes*</li>
                    <li>Multi-platform Accessibility</li>
                    <li>Full Extended Hours Trading</li>
                    <li>24/7 Online Help</li>
                </ul>
                <p class="whatweoffer-p3">*Free real-time quotes provided are Nasdaq Last Sale</p> --}}
            </div>
        </div>
    </div>

<!-- 1st Screen End -->


@include('frontend.partials.footer')
</div>
@endsection