@extends('frontend.layouts.app')
@push('styles')
<link href="{{asset('frontend/css/custom-faq.css')}}" rel="stylesheet">
<link href="{{asset('frontend/css/custom-market.css')}}" rel="stylesheet">



@endpush
@section('content')
<div class="container-fluid pd-0">
	<div class="btn-group-vertical dark-light-btn h-100 justify-content-center align-items-center hide-sm">
		<!--<button type="button" class="btn btn-primary lite-theme-cls">Light</button>-->
		<!--<button type="button" class="btn btn-primary dark-theme-cls">Dark</button>-->
		<div class="can-toggle demo-rebrand-1">
			<input id="d" type="checkbox">
			<label for="d">
				<div class="can-toggle__switch" data-checked="Light" data-unchecked="Dark"></div>
			</label>
		</div>
	</div>
	<div class="main-bg">
		@include('frontend.partials.navbar')

		<div class="container">


			<div class="row">
				<div class="can-toggle demo-rebrand-1 show-sm">
					<input id="d" type="checkbox">
					<label for="d">
						<div class="can-toggle__switch" data-checked="Light" data-unchecked="Dark"></div>
					</label>
				</div>
			</div>

		</div>
	</div>
	<div class="faq section-row" id="faq-section-row">
		<div class="container">
			<h2>FAQ</h2>
			<div class="tab">


				@php($k = 1)
				<?php
				$html = '';
				$faq_count = 1;
				?>
				@if(!empty(@$data) && @$data != null)
				@foreach(@$data as $v)

				<button class="tablinks" onclick="openCity(event, 'FAQ{{ @$v->id }}')" id="defaultOpen{{ $k }}">{{ $v->name }}</button>
				<?php
				$html .= '<div id="FAQ' . @$v->id . '" class="tabcontent" style="';
				if ($k != 1) {
					$html .= 'display:none';
				}
				$html .= '" >';
				$html .= '<h2>' . $v->name . '</h2>';
				foreach (@$v->faqs as $v2) {

					$html .= '
							<div class="card">
								<div class="card-header" id="headingOne' . $faq_count . '">
									<h5 class="mb-0">
										<button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne' . $faq_count . '" aria-expanded="true" aria-controls="collapseOne' . $faq_count . '">
										' . $v2->question . '
										</button>
									</h5>
								</div>

								<div id="collapseOne' . $faq_count . '" class="collapse" aria-labelledby="headingOne' . $faq_count . '" data-parent="#FAQ' . @$v->id . '">
									<div class="card-body">
										' . $v2->answer . '
									</div>
								</div>
							</div>	
							';
					$faq_count++;
				}
				$html .= '</div>';
				?>
				@php($k++)
				@endforeach
				@endif


			</div>

			<?= @$html ?>


		</div>

	</div>

	

@include('frontend.partials.footer')
</div>
<script>
	function openCity(evt, cityName) {
		var i, tabcontent, tablinks;
		tabcontent = document.getElementsByClassName("tabcontent");
		for (i = 0; i < tabcontent.length; i++) {
			tabcontent[i].style.display = "none";
		}
		tablinks = document.getElementsByClassName("tablinks");
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" active", "");
		}
		document.getElementById(cityName).style.display = "block";
		evt.currentTarget.className += " active";
	}

	// Get the element with id="defaultOpen" and click on it
	// document.getElementById("defaultOpen").click();
</script>

@endsection

@push('custom-script')
<script>
	$(document).ready(function() {

	});
</script>
@endpush