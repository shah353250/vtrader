@extends('frontend.layouts.app')
@push('styles')
<title>{{@$home->title}}</title>
<meta name="title" content="{{@$home->meta_title}}">
<meta name="keywords" content="{{@$home->meta_keyword}}">
<meta name="description" content="{{@$home->meta_description}}">
<style type="text/css">
	.main-bg{
		background-image: url({{asset('frontend/img/bg.jpg')}});
		background-position: center center;
		background-repeat: no-repeat;
		background-attachment: initial;
		background-size: cover;
		background-color: #464646;
		height:100vh;
	}
	
  .nav-a-margin{
	  margin-left: 10px;
	  margin-right: 10px;
  }
  .dark-light-btn{
	  
	  position: absolute;
  }
  .pd-0
  {
	  padding-right: 0px;
	  padding-left: 0px;
  }
  .red {
	  border: 3px solid red;
  }
  
</style>
@endpush
@section('content')
@include('frontend.partials.darklight')
    <!-- 1st Screen -->
		<div class="container-fluid pd-0">
		    
			<div class="main-bg">
				@include('frontend.partials.navbar')
				
				<div class="container">
					<!-- <div class="row">
						<div class="col-lg-12 col-sm-hidden col-xs-hidden">
							<div class="space-padding"></div>
						</div>
					</div> -->
					<div class="ban-text">
						<div class="row">
							<div class="col-md-8 col-lg-6 col-xl-3">
								{{-- <h1 class="main-h1-text">Lorem ipsum  dolor sit  amet</h1> --}}
								<h1 class="main-h1-text">{{@$content->main_title }}</h1>

							</div>
						</div>
						<div class="row">
							<div class="col-md-8 col-lg-6 col-xl-3">
								{{-- <p class="main-p-text">labore et dolore magna aliqua. Ut enim ad minim veniam, quis no labore et dolore magna aliqua. Ut enim ad minim veniam, quis no veniam, quis no</p> --}}
								<p class="main-p-text">{!!@$content->main_description !!}</p>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12 col-sm-hidden col-xs-hidden">
								<div class="space-padding-two"></div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 col-lg-12">
								<a href="{{config('url.app_register')}}" target="_blank"><button type="button" class="btn btn-primary reg">Register</button></a>
								<a href="{{config('url.app_login')}}" target="_blank"><button type="button" class="btn btn-primary login-btn">Login</button></a>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12 col-sm-hidden col-xs-hidden">
								<div class="space-padding-three"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		
		<!-- 1st Screen End -->
		<!-- 2nd Screen Start -->
		<div class="second-cont">
			<div class="container">
				<div class="row">
					<div class="col-lg-6">
						<p style="margin:0px;" class="sm-font"><i class="fa fa-volume-up"></i>labore et dolore magna aliqua. Ut enim ad</p>		
					</div>
					<div class="col-md-6" style="float: right; text-align:right">
						<p style="margin:0px;" class="sm-font"><span class="pd-left"><i class="fa fa-arrow-up" aria-hidden="true"></i> New Listings:</span> <span class="pd-left"><img src="{{asset('frontend/img/cca.png')}}"/></span>CCA up <span class="pd-left"><img src="{{asset('frontend/img/1inch.png')}}"/></span>1INCH up <span class="pd-left"><img src="{{asset('frontend/img/grt-up.png')}}"/></span>GRT up</p>
					</div>
				</div>
			</div>
		</div>
		<!-- 2nd Screen End -->
		<!-- 3rd Screen Start -->
		<div class="third-cont">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<h3 class="third-cont-h">Reprehenderit in voluptate</h3>
						<div class="row third-cont-one">
							<div class="col-md-3 third-cont-two">
							    <div class="right-border hide-sm"></div>
								<div class="row">
									<div class="col-md-6">
										<h6 class="third-cont-three">UDWO</h6>
									</div>
									<div class="col-md-6">
										<img src="{{asset('frontend/img/btc.png')}}" class="third-cont-four">
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<h6 class="third-cont-five">Dow  Jones</h6>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4 col-lg-4">
										<h5 class="mgb-2">UDWO</h5>
										<h6 class="mgb-2">27526.82</h6>
										<p>$275262.77</p>
									</div>
									<div class="col-md-8 col-lg-8">
										<!--<div class="graph-songs"></div>	-->
										<img src="{{asset('frontend/img/grph.png')}}" style="width: 100%;height: 75%;" />
									</div>
								</div>
								
							</div>
							<div class="col-md-3 third-cont-two">
							    <div class="right-border hide-sm"></div>
								<div class="row">
									<div class="col-md-6">
										<h6 class="third-cont-three">UPRO</h6>
									</div>
									<div class="col-md-6">
										<img src="{{asset('frontend/img/btc.png')}}" class="third-cont-four">
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<h6 class="third-cont-five">S & P </h6>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4 col-lg-4">
										<h5 class="mgb-2">UPRO</h5>
										<h6 class="mgb-2">723.62</h6>
										<p>$716.25</p>
									</div>
									<div class="col-md-8 col-lg-8">
										<img src="{{asset('frontend/img/grph.png')}}" style="width: 100%;height: 75%;" />	
									</div>
								</div>
								
							</div>
							<div class="col-md-3 third-cont-two">
							    <div class="right-border hide-sm"></div>
								<div class="row">
									<div class="col-md-6">
										<h6 class="third-cont-three">UDWO</h6>
									</div>
									<div class="col-md-6">
										<img src="{{asset('frontend/img/btc.png')}}" class="third-cont-four">
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<h6 class="third-cont-five">Dow  Jones</h6>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4 col-lg-4">
										<h5 class="mgb-2">UDWO</h5>
										<h6 class="mgb-2">27526.82</h6>
										<p>$275262.77</p>
									</div>
									<div class="col-md-8 col-lg-8">
										<img src="{{asset('frontend/img/grph.png')}}" style="width: 100%;height: 75%;" />	
									</div>
								</div>
								
							</div>
							<div class="col-md-3 third-cont-six">
								<div class="row">
									<div class="col-md-6">
										<h6 class="third-cont-three">UDWO</h6>
									</div>
									<div class="col-md-6">
										<img src="{{asset('frontend/img/btc.png')}}" class="third-cont-four">
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<h6 class="third-cont-five">Dow  Jones</h6>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4 col-lg-4">
										<h5 class="mgb-2">UDWO</h5>
										<h6 class="mgb-2">27526.82</h6>
										<p>$275262.77</p>
									</div>
									<div class="col-md-8 col-lg-8">
										<!--<div class="graph-songs"></div>	-->
										<img src="{{asset('frontend/img/grph.png')}}" style="width: 100%;height: 75%;" />	
									</div>
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- 3rd Screen End -->
		<!-- 4th Screen Start -->
		<div class="bg-stock-market fourth-cont">
			<div class="container">
			
					<div class="row">
						<div class="col-md-12 col-lg-12 fourth-cont-two">
							
						</div>
					</div>
					<div class="row">
						
						<div class="col-md-12">
							<div class="tabs">
							  <div class="tab-button-outer">
							    <ul id="tab-button">
							      <li class="fourth-cont-three"><a style="text-align: left;" href="#tab01">Recommended</a></li>
							      <li class="fourth-cont-three"><a href="#tab02">Top</a></li>
							      <li class="fourth-cont-three"><a href="#tab03">New Defi</a></li>
							      <li class="fourth-cont-three"><a href="#tab04">Polkadot</a></li>
							      <li class="fourth-cont-three"><a href="#tab05">ECO</a></li>
							      <li><a href="#tab06">Grayscale</a></li>
							    </ul>
							  </div>
							  <div class="tab-select-outer">
							    <select id="tab-select" class="select-drop-down">
							      <option value="#tab01">Recommended</option>
							      <option value="#tab02">Top</option>
							      <option value="#tab03">New Defi</option>
							      <option value="#tab04">Polkadot</option>
							      <option value="#tab05">ECO</option>
							      <option value="#tab06">Grayscale</option>
							    </select>
							  </div>

							  <div id="tab01" class="tab-contents fourth-cont-four">
							    <div class="row">
							    	<div class="col-lg-6 col-md-12 sm-12-full">
							    		<div class="row fourth-cont-five">
							    			<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 col-1 fourth-cont-six">
							    				<img src="{{asset('frontend/img/btc.png')}}" class="fourth-cont-seven">
							    			</div>
							    			<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 col-5 fourth-cont-eight">
							    				<p>CCA/USDT
							    				<br/>
							    				<span>102245996622.255954001488</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-4 fourth-cont-nine">
							    				<p class="fourth-cont-ten">0.55692<br/>
							    				<span>$0.55</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 col-2 fourth-cont-eleven">
							    				<p style="color: #77cb02;">+66.45</p>
							    			</div>
							    		</div>
							    	</div>
							    	<div class="col-lg-6 col-md-12 sm-12-full">
							    		<div class="row fourth-cont-five">
							    			<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 col-1 fourth-cont-six">
							    				<img src="{{asset('frontend/img/btc.png')}}" class="fourth-cont-seven">
							    			</div>
							    			<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 col-5 fourth-cont-eight">
							    				<p>CCA/USDT
							    				<br/>
							    				<span>102245996622.255954001488</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-4 fourth-cont-nine">
							    				<p class="fourth-cont-ten">0.55692<br/>
							    				<span>$0.55</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 col-2 fourth-cont-eleven">
							    				<p style="color: #77cb02;">+66.45</p>
							    			</div>
							    		</div>
							    	</div>
							    </div>
							    <div class="row">
							    	<div class="col-lg-6 col-md-12 sm-12-full">
							    		<div class="row fourth-cont-five">
							    			<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 col-1 fourth-cont-six">
							    				<img src="{{asset('frontend/img/btc.png')}}" class="fourth-cont-seven">
							    			</div>
							    			<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 col-5 fourth-cont-eight">
							    				<p>CCA/USDT
							    				<br/>
							    				<span>102245996622.255954001488</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-4 fourth-cont-nine">
							    				<p class="fourth-cont-ten">0.55692<br/>
							    				<span>$0.55</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 col-2 fourth-cont-eleven">
							    				<p style="color: #77cb02;">+66.45</p>
							    			</div>
							    		</div>
							    	</div>
							    	<div class="col-lg-6 col-md-12 sm-12-full">
							    		<div class="row fourth-cont-five">
							    			<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 col-1 fourth-cont-six">
							    				<img src="{{asset('frontend/img/btc.png')}}" class="fourth-cont-seven">
							    			</div>
							    			<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 col-5 fourth-cont-eight">
							    				<p>CCA/USDT
							    				<br/>
							    				<span>102245996622.255954001488</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-4 fourth-cont-nine">
							    				<p class="fourth-cont-ten">0.55692<br/>
							    				<span>$0.55</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 col-2 fourth-cont-eleven">
							    				<p style="color: #77cb02;">+66.45</p>
							    			</div>
							    		</div>
							    	</div>
							    </div>
							    <div class="row">
							    	<div class="col-lg-6 col-md-12 sm-12-full">
							    		<div class="row fourth-cont-five">
							    			<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 col-1 fourth-cont-six">
							    				<img src="{{asset('frontend/img/btc.png')}}" class="fourth-cont-seven">
							    			</div>
							    			<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 col-5 fourth-cont-eight">
							    				<p>CCA/USDT
							    				<br/>
							    				<span>102245996622.255954001488</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-4 fourth-cont-nine">
							    				<p class="fourth-cont-ten">0.55692<br/>
							    				<span>$0.55</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 col-2 fourth-cont-eleven">
							    				<p style="color: #77cb02;">+66.45</p>
							    			</div>
							    		</div>
							    	</div>
							    	<div class="col-lg-6 col-md-12 sm-12-full">
							    		<div class="row fourth-cont-five">
							    			<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 col-1 fourth-cont-six">
							    				<img src="{{asset('frontend/img/btc.png')}}" class="fourth-cont-seven">
							    			</div>
							    			<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 col-5 fourth-cont-eight">
							    				<p>CCA/USDT
							    				<br/>
							    				<span>102245996622.255954001488</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-4 fourth-cont-nine">
							    				<p class="fourth-cont-ten">0.55692<br/>
							    				<span>$0.55</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 col-2 fourth-cont-eleven">
							    				<p style="color: #77cb02;">+66.45</p>
							    			</div>
							    		</div>
							    	</div>
							    </div>
							    <div class="row">
							    	<div class="col-lg-6 col-md-12 sm-12-full">
							    		<div class="row fourth-cont-five">
							    			<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 col-1 fourth-cont-six">
							    				<img src="{{asset('frontend/img/btc.png')}}" class="fourth-cont-seven">
							    			</div>
							    			<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 col-5 fourth-cont-eight">
							    				<p>CCA/USDT
							    				<br/>
							    				<span>102245996622.255954001488</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-4 fourth-cont-nine">
							    				<p class="fourth-cont-ten">0.55692<br/>
							    				<span>$0.55</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 col-2 fourth-cont-eleven">
							    				<p style="color: #77cb02;">+66.45</p>
							    			</div>
							    		</div>
							    	</div>
							    	<div class="col-lg-6 col-md-12 sm-12-full">
							    		<div class="row fourth-cont-five">
							    			<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 col-1 fourth-cont-six">
							    				<img src="{{asset('frontend/img/btc.png')}}" class="fourth-cont-seven">
							    			</div>
							    			<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 col-5 fourth-cont-eight">
							    				<p>CCA/USDT
							    				<br/>
							    				<span>102245996622.255954001488</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-4 fourth-cont-nine">
							    				<p class="fourth-cont-ten">0.55692<br/>
							    				<span>$0.55</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 col-2 fourth-cont-eleven">
							    				<p style="color: #77cb02;">+66.45</p>
							    			</div>
							    		</div>
							    	</div>
							    </div>
							  </div>
							  <div id="tab02" class="tab-contents fourth-cont-four">
							    <div class="row">
							    	<div class="col-lg-6 col-md-12 sm-12-full">
							    		<div class="row fourth-cont-five">
							    			<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 col-1 fourth-cont-six">
							    				<img src="{{asset('frontend/img/btc.png')}}" class="fourth-cont-seven">
							    			</div>
							    			<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 col-5 fourth-cont-eight">
							    				<p>CCA/USDT
							    				<br/>
							    				<span>102245996622.255954001488</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-4 fourth-cont-nine">
							    				<p class="fourth-cont-ten">0.55692<br/>
							    				<span>$0.55</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 col-2 fourth-cont-eleven">
							    				<p style="color: #77cb02;">+66.45</p>
							    			</div>
							    		</div>
							    	</div>
							    	<div class="col-lg-6 col-md-12 sm-12-full">
							    		<div class="row fourth-cont-five">
							    			<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 col-1 fourth-cont-six">
							    				<img src="{{asset('frontend/img/btc.png')}}" class="fourth-cont-seven">
							    			</div>
							    			<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 col-5 fourth-cont-eight">
							    				<p>CCA/USDT
							    				<br/>
							    				<span>102245996622.255954001488</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-4 fourth-cont-nine">
							    				<p class="fourth-cont-ten">0.55692<br/>
							    				<span>$0.55</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 col-2 fourth-cont-eleven">
							    				<p style="color: #77cb02;">+66.45</p>
							    			</div>
							    		</div>
							    	</div>
							    </div>
							    <div class="row">
							    	<div class="col-lg-6 col-md-12 sm-12-full">
							    		<div class="row fourth-cont-five">
							    			<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 col-1 fourth-cont-six">
							    				<img src="{{asset('frontend/img/btc.png')}}" class="fourth-cont-seven">
							    			</div>
							    			<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 col-5 fourth-cont-eight">
							    				<p>CCA/USDT
							    				<br/>
							    				<span>102245996622.255954001488</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-4 fourth-cont-nine">
							    				<p class="fourth-cont-ten">0.55692<br/>
							    				<span>$0.55</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 col-2 fourth-cont-eleven">
							    				<p style="color: #77cb02;">+66.45</p>
							    			</div>
							    		</div>
							    	</div>
							    	<div class="col-lg-6 col-md-12 sm-12-full">
							    		<div class="row fourth-cont-five">
							    			<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 col-1 fourth-cont-six">
							    				<img src="{{asset('frontend/img/btc.png')}}" class="fourth-cont-seven">
							    			</div>
							    			<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 col-5 fourth-cont-eight">
							    				<p>CCA/USDT
							    				<br/>
							    				<span>102245996622.255954001488</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-4 fourth-cont-nine">
							    				<p class="fourth-cont-ten">0.55692<br/>
							    				<span>$0.55</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 col-2 fourth-cont-eleven">
							    				<p style="color: #77cb02;">+66.45</p>
							    			</div>
							    		</div>
							    	</div>
							    </div>
							  </div>
							  <div id="tab03" class="tab-contents fourth-cont-four">
							    <div class="row">
							    	<div class="col-lg-6 col-md-12 sm-12-full">
							    		<div class="row fourth-cont-five">
							    			<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 col-1 fourth-cont-six">
							    				<img src="{{asset('frontend/img/btc.png')}}" class="fourth-cont-seven">
							    			</div>
							    			<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 col-5 fourth-cont-eight">
							    				<p>CCA/USDT
							    				<br/>
							    				<span>102245996622.255954001488</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-4 fourth-cont-nine">
							    				<p class="fourth-cont-ten">0.55692<br/>
							    				<span>$0.55</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 col-2 fourth-cont-eleven">
							    				<p style="color: #77cb02;">+66.45</p>
							    			</div>
							    		</div>
							    	</div>
							    	<div class="col-lg-6 col-md-12 sm-12-full">
							    		<div class="row fourth-cont-five">
							    			<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 col-1 fourth-cont-six">
							    				<img src="{{asset('frontend/img/btc.png')}}" class="fourth-cont-seven">
							    			</div>
							    			<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 col-5 fourth-cont-eight">
							    				<p>CCA/USDT
							    				<br/>
							    				<span>102245996622.255954001488</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-4 fourth-cont-nine">
							    				<p class="fourth-cont-ten">0.55692<br/>
							    				<span>$0.55</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 col-2 fourth-cont-eleven">
							    				<p style="color: #77cb02;">+66.45</p>
							    			</div>
							    		</div>
							    	</div>
							    </div>
							    <div class="row">
							    	<div class="col-lg-6 col-md-12 sm-12-full">
							    		<div class="row fourth-cont-five">
							    			<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 col-1 fourth-cont-six">
							    				<img src="{{asset('frontend/img/btc.png')}}" class="fourth-cont-seven">
							    			</div>
							    			<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 col-5 fourth-cont-eight">
							    				<p>CCA/USDT
							    				<br/>
							    				<span>102245996622.255954001488</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-4 fourth-cont-nine">
							    				<p class="fourth-cont-ten">0.55692<br/>
							    				<span>$0.55</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 col-2 fourth-cont-eleven">
							    				<p style="color: #77cb02;">+66.45</p>
							    			</div>
							    		</div>
							    	</div>
							    	<div class="col-lg-6 col-md-12 sm-12-full">
							    		<div class="row fourth-cont-five">
							    			<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 col-1 fourth-cont-six">
							    				<img src="{{asset('frontend/img/btc.png')}}" class="fourth-cont-seven">
							    			</div>
							    			<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 col-5 fourth-cont-eight">
							    				<p>CCA/USDT
							    				<br/>
							    				<span>102245996622.255954001488</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-4 fourth-cont-nine">
							    				<p class="fourth-cont-ten">0.55692<br/>
							    				<span>$0.55</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 col-2 fourth-cont-eleven">
							    				<p style="color: #77cb02;">+66.45</p>
							    			</div>
							    		</div>
							    	</div>
							    </div>
							    <div class="row">
							    	<div class="col-lg-6 col-md-12 sm-12-full">
							    		<div class="row fourth-cont-five">
							    			<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 col-1 fourth-cont-six">
							    				<img src="{{asset('frontend/img/btc.png')}}" class="fourth-cont-seven">
							    			</div>
							    			<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 col-5 fourth-cont-eight">
							    				<p>CCA/USDT
							    				<br/>
							    				<span>102245996622.255954001488</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-4 fourth-cont-nine">
							    				<p class="fourth-cont-ten">0.55692<br/>
							    				<span>$0.55</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 col-2 fourth-cont-eleven">
							    				<p style="color: #77cb02;">+66.45</p>
							    			</div>
							    		</div>
							    	</div>
							    	<div class="col-lg-6 col-md-12 sm-12-full">
							    		<div class="row fourth-cont-five">
							    			<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 col-1 fourth-cont-six">
							    				<img src="{{asset('frontend/img/btc.png')}}" class="fourth-cont-seven">
							    			</div>
							    			<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 col-5 fourth-cont-eight">
							    				<p>CCA/USDT
							    				<br/>
							    				<span>102245996622.255954001488</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-4 fourth-cont-nine">
							    				<p class="fourth-cont-ten">0.55692<br/>
							    				<span>$0.55</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 col-2 fourth-cont-eleven">
							    				<p style="color: #77cb02;">+66.45</p>
							    			</div>
							    		</div>
							    	</div>
							    </div>
							  </div>
							  <div id="tab04" class="tab-contents fourth-cont-four">
							    <div class="row">
							    	<div class="col-lg-6 col-md-12 sm-12-full">
							    		<div class="row fourth-cont-five">
							    			<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 col-1 fourth-cont-six">
							    				<img src="{{asset('frontend/img/btc.png')}}" class="fourth-cont-seven">
							    			</div>
							    			<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 col-5 fourth-cont-eight">
							    				<p>CCA/USDT
							    				<br/>
							    				<span>102245996622.255954001488</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-4 fourth-cont-nine">
							    				<p class="fourth-cont-ten">0.55692<br/>
							    				<span>$0.55</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 col-2 fourth-cont-eleven">
							    				<p style="color: #77cb02;">+66.45</p>
							    			</div>
							    		</div>
							    	</div>
							    	<div class="col-lg-6 col-md-12 sm-12-full">
							    		<div class="row fourth-cont-five">
							    			<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 col-1 fourth-cont-six">
							    				<img src="{{asset('frontend/img/btc.png')}}" class="fourth-cont-seven">
							    			</div>
							    			<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 col-5 fourth-cont-eight">
							    				<p>CCA/USDT
							    				<br/>
							    				<span>102245996622.255954001488</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-4 fourth-cont-nine">
							    				<p class="fourth-cont-ten">0.55692<br/>
							    				<span>$0.55</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 col-2 fourth-cont-eleven">
							    				<p style="color: #77cb02;">+66.45</p>
							    			</div>
							    		</div>
							    	</div>
							    </div>
							    <div class="row">
							    	<div class="col-lg-6 col-md-12 sm-12-full">
							    		<div class="row fourth-cont-five">
							    			<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 col-1 fourth-cont-six">
							    				<img src="{{asset('frontend/img/btc.png')}}" class="fourth-cont-seven">
							    			</div>
							    			<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 col-5 fourth-cont-eight">
							    				<p>CCA/USDT
							    				<br/>
							    				<span>102245996622.255954001488</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-4 fourth-cont-nine">
							    				<p class="fourth-cont-ten">0.55692<br/>
							    				<span>$0.55</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 col-2 fourth-cont-eleven">
							    				<p style="color: #77cb02;">+66.45</p>
							    			</div>
							    		</div>
							    	</div>
							    	<div class="col-lg-6 col-md-12 sm-12-full">
							    		<div class="row fourth-cont-five">
							    			<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 col-1 fourth-cont-six">
							    				<img src="{{asset('frontend/img/btc.png')}}" class="fourth-cont-seven">
							    			</div>
							    			<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 col-5 fourth-cont-eight">
							    				<p>CCA/USDT
							    				<br/>
							    				<span>102245996622.255954001488</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-4 fourth-cont-nine">
							    				<p class="fourth-cont-ten">0.55692<br/>
							    				<span>$0.55</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 col-2 fourth-cont-eleven">
							    				<p style="color: #77cb02;">+66.45</p>
							    			</div>
							    		</div>
							    	</div>
							    </div>
							    <div class="row">
							    	<div class="col-lg-6 col-md-12 sm-12-full">
							    		<div class="row fourth-cont-five">
							    			<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 col-1 fourth-cont-six">
							    				<img src="{{asset('frontend/img/btc.png')}}" class="fourth-cont-seven">
							    			</div>
							    			<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 col-5 fourth-cont-eight">
							    				<p>CCA/USDT
							    				<br/>
							    				<span>102245996622.255954001488</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-4 fourth-cont-nine">
							    				<p class="fourth-cont-ten">0.55692<br/>
							    				<span>$0.55</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 col-2 fourth-cont-eleven">
							    				<p style="color: #77cb02;">+66.45</p>
							    			</div>
							    		</div>
							    	</div>
							    	<div class="col-lg-6 col-md-12 sm-12-full">
							    		<div class="row fourth-cont-five">
							    			<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 col-1 fourth-cont-six">
							    				<img src="{{asset('frontend/img/btc.png')}}" class="fourth-cont-seven">
							    			</div>
							    			<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 col-5 fourth-cont-eight">
							    				<p>CCA/USDT
							    				<br/>
							    				<span>102245996622.255954001488</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-4 fourth-cont-nine">
							    				<p class="fourth-cont-ten">0.55692<br/>
							    				<span>$0.55</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 col-2 fourth-cont-eleven">
							    				<p style="color: #77cb02;">+66.45</p>
							    			</div>
							    		</div>
							    	</div>
							    </div>
							    <div class="row">
							    	<div class="col-lg-6 col-md-12 sm-12-full">
							    		<div class="row fourth-cont-five">
							    			<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 col-1 fourth-cont-six">
							    				<img src="{{asset('frontend/img/btc.png')}}" class="fourth-cont-seven">
							    			</div>
							    			<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 col-5 fourth-cont-eight">
							    				<p>CCA/USDT
							    				<br/>
							    				<span>102245996622.255954001488</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-4 fourth-cont-nine">
							    				<p class="fourth-cont-ten">0.55692<br/>
							    				<span>$0.55</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 col-2 fourth-cont-eleven">
							    				<p style="color: #77cb02;">+66.45</p>
							    			</div>
							    		</div>
							    	</div>
							    	<div class="col-lg-6 col-md-12 sm-12-full">
							    		<div class="row fourth-cont-five">
							    			<div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 col-1 fourth-cont-six">
							    				<img src="{{asset('frontend/img/btc.png')}}" class="fourth-cont-seven">
							    			</div>
							    			<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 col-5 fourth-cont-eight">
							    				<p>CCA/USDT
							    				<br/>
							    				<span>102245996622.255954001488</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 col-4 fourth-cont-nine">
							    				<p class="fourth-cont-ten">0.55692<br/>
							    				<span>$0.55</span>
							    				</p>
							    			</div>
							    			<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 col-2 fourth-cont-eleven">
							    				<p style="color: #77cb02;">+66.45</p>
							    			</div>
							    		</div>
							    	</div>
							    </div>
							  </div>
							</div>
						</div>
					</div>
				</div>
		</div>
		<!-- 4th Screen End -->
		<!-- 5th Screen Start -->
		<div class="why-choose-bg fifth-cont">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 fifth-cont-two">
					</div>
				</div>
				<div class="row">
					
					<div class="col-lg-12 col-md-12">
						<div class="row">
							<div class="col-lg-12 col-md-12">
								{{-- <h2 class="fifth-cont-three">Why Choose Us?</h2> --}}
								<h2 class="fifth-cont-three">{{@$why_choose_us_detail->why_choose_us_title }}</h2>

								{{-- <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam eleifend quam eu eleifend volutpat</p>	 --}}
								<p>{!!@$why_choose_us_detail->why_choose_us_description !!}</p>	

							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 col-md-12 fifth-cont-four">
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 col-md-12">
						<div class="row">
							{{-- <div class="col-lg-3 col-md-3">
								<h5 class="fifth-cont-five">Stable & Secure</h5>
								<p>Mauris lacinia maximus nisl in maximus. Suspendisse nisl massa, eleifend sed pretium nec, tristique sit amet mi. Ut imperdiet sapien a nibh iaculis interdum. Praesent rhoncus sapien at sapien posuere, ut pharetra lectus scelerisque. Cras suscipit diam non porta ornare. Etiam in aliquam erat. Pellentesque egestas eleifend ligula</p>	
							</div>
							<div class="col-lg-3 col-md-3">
								<h5 class="fifth-cont-five">Stable & Secure</h5>
								<p>Mauris lacinia maximus nisl in maximus. Suspendisse nisl massa, eleifend sed pretium nec, tristique sit amet mi. Ut imperdiet sapien a nibh iaculis interdum. Praesent rhoncus sapien at sapien posuere, ut pharetra lectus scelerisque. Cras suscipit diam non porta ornare. Etiam in aliquam erat. Pellentesque egestas eleifend ligula</p>	
							</div>
							<div class="col-lg-3 col-md-3">
								<h5 class="fifth-cont-five">Stable & Secure</h5>
								<p>Mauris lacinia maximus nisl in maximus. Suspendisse nisl massa, eleifend sed pretium nec, tristique sit amet mi. Ut imperdiet sapien a nibh iaculis interdum. Praesent rhoncus sapien at sapien posuere, ut pharetra lectus scelerisque. Cras suscipit diam non porta ornare. Etiam in aliquam erat. Pellentesque egestas eleifend ligula</p>	
							</div> --}}
							<div class="col-lg-3 col-md-3">
								<h5 class="fifth-cont-five">{{@$why_choose_us_detail->section1_title}}</h5>
								<p>{!!@$why_choose_us_detail->section1_description !!}</p>	
							</div>
							<div class="col-lg-3 col-md-3">
								<h5 class="fifth-cont-five">{{@$why_choose_us_detail->section2_title}}</h5>
								<p>{!!@$why_choose_us_detail->section2_description !!}</p>	
							</div>
							<div class="col-lg-3 col-md-3">
								<h5 class="fifth-cont-five">{{@$why_choose_us_detail->section3_title}}</h5>
								<p>{!!@$why_choose_us_detail->section3_description !!}</p>	
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
		<!-- 5th Screen End -->
		<!-- 6th Screen Start -->
		<div class="mobile-bg sixth-cont">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 sixth-cont-two">
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 col-md-12">
						<h2 class="sixth-cont-three">Maecenas nisl odio, placerat a vestibulum tempor</h2>
						<form>
							<div class="row sixth-cont-four">
								<div class="col-md-5">
									<span>For</span><br/>
									<input type="text" name="usd" class="sixth-cont-five" /><span class="sixth-cont-six">USD</span>	
								</div>
								<div class="col-md-5">
									<span>You get</span><br/>
									<input type="text" name="btc" class="sixth-cont-five" /><span class="sixth-cont-six">BTC</span>	
								</div>
								<div class="col-md-2">
									<br/>
									<button type="button" class="btn btn-primary sixth-cont-seven">Buy ETC</button>
									<br/>
									<br/>
								</div>
								
							</div>
						</form>
						<div class="col-lg-12 col-md-12 powered-ico">
							<p style="float: right;padding-top: 50px;">Powered by: <img style="width: 60px;" src="{{asset('frontend/img/visa.png')}}"><img class="sixth-cont-eight" src="{{asset('frontend/img/mastercard.png')}}"></p>
						</div>
					</div>
					
				</div>
			</div>
		</div>	
		<!-- 6th Screen End -->

		<!-- 7th Screen Start -->
		<div class="bg-bitcoin seventh-cont">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 seventh-cont-two">
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 col-md-12">
						<h2 class="seventh-cont-three">Explore our News</h2>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 col-md-12 fifth-cont-four">
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 col-md-12">
						<div class="row">
							@foreach ($news as $item)
								<div class="col-lg-4 col-md-4 fifth-cont-four">
									<div class="seventh-cont-four">
										<img class="img-fluid px-4" src="{{@$item->image_path}}">
										<p class="seventh-cont-six px-4 line-clamp">{{@$item->description}} </p><a style="margin-left: 20px;" href="#">More...</a>
									</div>
								</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- 7th Screen End -->

		<!-- 8th Screen Start -->
		<div class="platform-bg eighth-cont">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 col-md-12 eighth-cont-one">
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12 col-md-12">
						<div class="row">
							<div class="col-lg-5 col-md-5 custom-md-6">
								<h1 class="eighth-cont-two"> {!!@$last_section->last_section_description!!}</h1>
								<a href="{{@$last_section->ios_link}}" target="_blank"><img src="{{asset('frontend/img/appstore.png')}}" class="eighth-cont-three"></a>
								<a href="{{@$last_section->android_link}}" target="_blank"><img src="{{asset('frontend/img/google_play.png')}}" class="eighth-cont-three"> </a>
							</div>
							<div class="col-lg-7 col-md-7 custom-md-6" style="float: right;">
								<img src="{{asset('frontend/img/phone.png')}}" class="eighth-cont-four">	
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- 8th Screen End -->

		@include('frontend.partials.footer')
		</div>
@endsection