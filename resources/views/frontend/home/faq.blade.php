@extends('frontend.layouts.app')
@push('styles')
<link href="{{asset('frontend/css/custom-faq.css')}}" rel="stylesheet">

<title>{{@$FrontPage->title}}</title>

<meta name="title" content="{{@$FrontPage->meta_title}}">
<meta name="keywords" content="{{@$FrontPage->meta_keyword}}">
<meta name="description" content="{{@$FrontPage->meta_description}}">

@endpush
@section('content')
@include('frontend.partials.darklight')
<div class="container-fluid pd-0">
	<div class="faq-bg">
		@include('frontend.partials.navbar')
		
	</div>
	<div class="faq section-row faq-section-bg" id="faq-section-row">
		<div class="container">
			<h2>FAQ</h2>
		<div class="the-main-con">
			<div class="tab">


				@php($k = 1)
				<?php
				$html = '';
				$faq_count = 1;
				?>
				@if(!empty(@$data) && @$data != null)
				@foreach(@$data as $v)

				<button class="tablinks" onclick="openCity(event, 'FAQ{{ @$v->id }}')" id="defaultOpen{{ $k }}">{{ $v->name }}</button>
				<?php
				$html .= '<div id="FAQ' . @$v->id . '" class="tabcontent" style="';
				if ($k != 1) {
					$html .= 'display:none';
				}
				$html .= '" >';
				$html .= '<h2>' . $v->name . '</h2>';
				foreach (@$v->faqsFrontendActive as $v2) {

					$html .= '
							<div class="card">
								<div class="card-header" id="headingOne' . $faq_count . '">
									<h5 class="mb-0 questionclickToggle">
									<button class="btn btn-link">
										' . $v2->question . '
										</button>
									</h5>
								</div>

								<div class="answerdetail" style="display:none">
									<div class="card-body">
										' . $v2->answer . '
									</div>
								</div>
							</div>	
							';
					$faq_count++;
				}
				$html .= '</div>';
				?>
				@php($k++)
				@endforeach
				@endif


			</div>
		
			<?= @$html ?>

		</div>
		</div>

	</div>


	

		

@include('frontend.partials.footer')
</div>
<script>
function firstpill() {
	var i, tablinks;
	tablinks = document.getElementsByClassName("tablinks");
	for (i = 0; i < tablinks.length; i++) {
		if(tablinks[0])
		{
			tablinks[0].className = 'tablinks active';
		}
	}
}
window.onload = firstpill;

function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
// document.getElementById("defaultOpen").click();

</script>


@endsection
@push('scripts')
	<script>
		$(document).on('click','.questionclickToggle',function(){
			$(this).parents('div.card').find('.answerdetail').slideToggle(300);
			$(this).parents('div.card').find('button.btn.btn-link').toggleClass('collapsed');
		})
	</script>
@endpush