@extends('frontend.layouts.app')
@push('styles')
<title>{{@$termsAndServices->title}}</title>
<meta name="title" content="{{@$termsAndServices->meta_title}}">
<meta name="keywords" content="{{@$termsAndServices->meta_keyword}}">
<meta name="description" content="{{@$termsAndServices->meta_description}}">
<link href="{{asset('frontend/css/custom-services.css')}}" rel="stylesheet">
@endpush
@section('content')
@include('frontend.partials.darklight')
<div class="container-fluid pd-0" style="background-color:#000e14;">
    <div class="main-bg">
    @include('frontend.partials.navbar')		
        <div class="container">
            <div class="row termsAndServices-row">
                <div class="col-md-12">
                    {{-- <h1 class="title">Terms and Services</h1> --}}
                    <h1 class="title">{{@$content->main_title}}</h1>
                </div>
                <div class="col-md-12 pd-bottom-80">
                    {{-- <p class="text-white">The following terms and disclaimers cover all (including but not limited to market data from different exchanges, fundamental data such as financial reports, analysis data, corporate actions, news and etc.) provided on Vtrader mobile device applications, Vtrader websites and any other products and services provided by Webull and its affiliates.</p>
                    <p class="text-white text-justify">The market data and news information are provided by third party service providers, Webull Technologies Limited has not involved in preparation, adoption or editing of third party content and does not explicitly or implicitly endorse or approve such content. The third party content providers do not explicitly or implicitly endorse or approve the third party content, nor do they give investment advice, or advocate the purchase or sale of any security or investment. Webull Technologies Limited and third party data service providers do not guarantee the accuracy and reliability. Webull Technologies Limited shall not bear any legal liability to the user for any loss or damages arising from information delay, error or omission of such market data.</p>
                    <p class="text-white text-justify">While Webull makes every attempt to provide accurate and timely information to serve the needs of users, but does not guarantee the accuracy, completeness, timeliness or applicability of usages. Information provided thereby does not constitute any investment advices. The user shall be solely responsible for any risk or consequence arising from behaviors based on reading these contents and Webull Technologies Limited shall not bear any legal liability.</p>
                    <p class="text-white text-justify">For malfunction of the user’s own network and devices, or for any delay, suspension or interruption of the market data disseminated by securities exchanges or third party service providers result in information or record loss, Webull Technologies Limited shall not bear any legal liability.</p>
                    <p class="text-white text-justify">For relevant service interruption or loss to the user, due to “force majeure”, Vtrader shall not bear any legal liability.</p>
                    <p class="text-white text-justify">For normal service interruption caused by system maintenance and update, Vtrader will make all reasonable efforts to notify users in advance and retain the right to suspend or terminate partial or whole network services without prior notice to users; for any loss arising from service suspension or termination, Webull Technologies Limited shall not bear any legal liability.</p>
                    <p class="text-white text-justify">Third party information provided in Webull product features does not reflect the views of Webull. The Webull product features are designed for informational purposes only and are not intended to serve as recommendations to customers to buy or sell any securities in their self-directed account.</p>
                    <p class="text-white text-justify">All investments involve risk, and the past performance of a security or financial product does not guarantee future results or returns. Keep in mind that while diversification may help spread risk it does not assure a profit or protect against loss. There is always the potential of losing money when you invest in securities, or other financial products. Investors should consider their investment objectives and risks carefully before investing.</p> --}}
                    <div class="text-white text-justify">{!!@$content->main_description!!}</div>
                </div>
            </div>  
        </div>
    </div>
@include('frontend.partials.footer')
</div>
@endsection
