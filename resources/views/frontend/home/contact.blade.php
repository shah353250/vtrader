@extends('frontend.layouts.app')
@push('styles')
<meta name="title" content="{{@$FrontPage->meta_title}}">
<meta name="keywords" content="{{@$FrontPage->meta_keyword}}">
<meta name="description" content="{{@$FrontPage->meta_description}}">
<title>{{@$FrontPage->title}}</title>
<link href="{{asset('frontend/css/custom-contact.css')}}" rel="stylesheet">
@endpush
@section('content')
@include('frontend.partials.darklight')
<div class="container-fluid pd-0" style="background-color:#000e14;">
    <div class="contact-bg">
    @include('frontend.partials.navbar')		
        <div class="container">
            <div class="row contact-row">
                <div class="col-md-12">
                    <h1 class="contact-us-title">Contact Us</h1>
                </div>
                <div class="col-md-6">
                    <form action="">
						<!-- <label for="name">Name</label> -->
					    <input class="contact-name" type="text" id="fname" name="firstname" placeholder="Name">

					    <!-- <label for="country">Email</label> -->
					    <input class="contact-email" type="email" id="email" placeholder="Email Address">

					    <!-- <label for="subject">Message</label> -->
					    <textarea class="contact-message" name="message" id="message" placeholder="Message" style="height:200px"></textarea>

					    <input class="contact-send" type="submit" value="Send" id="submit_btn">

                        <div class="success-msg" style="display: none;">
                          <i class="fa fa-check"></i>
                          <span id="success_message"></span>
                        </div>

                        <div class="error-msg" style="display: none;">
                          <i class="fa fa-times-circle"></i>
                          <span id="error_message"></span>

                        </div>
					</form>
                </div>
            </div>  
        </div>
    </div>
@include('frontend.partials.footer')
</div>
@endsection
@push('scripts')
    <script>
        $(document).ready(function(){  
            $('p').addClass('text-white');
        });

        $("#submit_btn").click(function (e) { 
            e.preventDefault();
            let name = $('#fname').val();
            let email = $('#email').val();
            let message = $('#message').val();
            if(name !='' && email !='' && message !='')
            {
                if(ValidateEmail(email))
                {
                    $("#submit_btn").attr("disabled", true);
                    $.ajax({
                        type: "POST",
                        url: "{{route('AjaxCallForContactUsSubmitAndEmail')}}",
                        data: {
                            _token: '{{ csrf_token() }}',
                            name: name,
                            email: email,
                            message: message,
                        },
                        success: function (response) {
                            if(response['status'])
                            {
                                $("#success_message").text(response['message']);
                                $('#fname').val("");
                                $('#email').val("");
                                $('#message').val("");
                                $(".success-msg").show();
                            }
                            else{
                                $("#error_message").text(response['message']);
                                $(".error-msg").show();
                            }
                            $("#submit_btn").attr("disabled", false);
                            $(".success-msg").delay(5000).fadeOut();
                            $(".error-msg").delay(5000).fadeOut();

                        }
                    });
                }
                else{
                    $("#error_message").text("Invalid Email Address!");
                    $(".error-msg").show();

                    $(".error-msg").delay(5000).fadeOut();
                }
                
            }
            else
            {
                $("#error_message").text("Some fields are empty!");
                $(".error-msg").show();

                $(".error-msg").delay(5000).fadeOut();
            }
        });

        function ValidateEmail(str) 
        {
            if (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(str))
            {
                return (true)
            }
            else{
                return (false)
            }
        }

    </script>
@endpush