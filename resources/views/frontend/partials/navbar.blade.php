<!-- Navigation Start -->
<div class="row" style="margin-right: 0px;">
    <div class="bs-example" style="width: 100%;">
        <nav class="navbar navbar-expand-md navbar-dark">
            <a href="{{route('home.index')}}" class="navbar-brand">
                <img src="{{asset('frontend/img/logo.png')}}" style="width: 100px;">
            </a>
            <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarCollapse">
                <div class="navbar-nav">
                    <a href="{{route('home.index')}}" class="nav-item nav-link active nav-a-margin">HOME</a>
                    <a href="{{route('home.market')}}" class="nav-item nav-link nav-a-margin">MARKET</a>
                    <a href="{{config('url.app_trade')}}" target="_blank" class="nav-item nav-link nav-a-margin">TRADE</a>
                    <a href="{{route('home.pricing')}}" class="nav-item nav-link nav-a-margin" tabindex="-1">PRICING</a>
                    <a href="{{route('home.faq')}}" class="nav-item nav-link nav-a-margin" tabindex="-1">HELP</a>
                </div>
                <div class="navbar-nav ml-auto pt-3">
                    <span class="input-group-append ">
                        <div class="input-group-text bg-transparent search-field">
                            <input class="bg-transparent" type="text" placeholder="Search" aria-label="Search">
                            <i class="fa fa-search"></i>
                        </div>
                    </span>
                    <a href="{{config('url.app_login')}}" target="_blank" class="nav-item nav-link hide-sm">LOGIN</a>
                    <span class="dot"></span>
                    <a href="{{config('url.app_register')}}" target="_blank" class="nav-item nav-link hide-sm">REGISTER</a>
                </div>
            </div>
            <div class="nav-right-padding">
            </div>
        </nav>
    </div>
</div>
<!-- Navigation End -->