<!-- Navigation Start -->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">

        <button type="button" id="sidebarCollapse" class="navbar-btn">
            <span></span>
            <span></span>
            <span></span>
        </button>
        <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <i class="fas fa-align-justify"></i>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="nav navbar-nav ml-auto">
                <li class="nav-item active">
                    <a href="{{route('home.index')}}" class="nav-item nav-link active nav-a-margin">HOME</a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-item nav-link nav-a-margin">TRADE</a>
                </li>
                <li class="nav-item">
                    <a href="{{route('home.pricing')}}" class="nav-item nav-link nav-a-margin" tabindex="-1">PRICING</a>
                </li>
                <li class="nav-item">
                    <a href="{{route('home.faq')}}" class="nav-item nav-link nav-a-margin" tabindex="-1">HELP</a>
                </li>
                <div class="navbar-nav ml-auto">
        <span class="input-group-append ">
            <div class="input-group-text bg-transparent search-field">
                <input class="bg-transparent" type="text" placeholder="Search" aria-label="Search">
                <i class="fa fa-search"></i>
            </div>
        </span>
        <a href="#" class="nav-item nav-link hide-sm">LOGIN</a>
        <span class="dot"></span>
        <a href="#" class="nav-item nav-link hide-sm">REGISTER</a>
    </div>
            </ul>
        </div>
    </div>
</nav>
<!-- Navigation End -->