<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',"Site\HomeController@index")->name('home.index');
Route::get('/market',"Site\HomeController@market")->name('home.market');
Route::get('/about-us',"Site\HomeController@aboutUs")->name('home.aboutus');
Route::get('/pricing',"Site\HomeController@pricing")->name('home.pricing');
Route::get('/faq',"Site\HomeController@faq")->name('home.faq');
Route::get('/terms-of-service',"Site\HomeController@termsandservices")->name('home.termsandservices');
Route::get('/privacy-policy',"Site\HomeController@privacypolicy")->name('home.privacypolicy');
Route::get('/profile',"Site\HomeController@profile")->name('home.profile');
Route::get('/anti-money-laundering',"Site\HomeController@antimoney")->name('home.antimoney');
Route::get('/login',"Site\HomeController@login")->name('home.login');
Route::get('/signup',"Site\HomeController@signup")->name('home.signup');
Route::get('/contact-us',"Site\HomeController@contact")->name('home.contact');
Route::post('AjaxCallForContactUsSubmitAndEmail',"Site\HomeController@AjaxCallForContactUsSubmitAndEmail")->name('AjaxCallForContactUsSubmitAndEmail');

Route::post('/AjaxCallForSaveThemeInCookie',"Site\HomeController@AjaxCallForSaveThemeInCookie")->name('AjaxCallForSaveThemeInCookie');
Route::get('/AjaxCallForGetThemeInCookie',"Site\HomeController@AjaxCallForGetThemeInCookie")->name('AjaxCallForGetThemeInCookie');




// =========== Admin Auth Routes ==============//
Route::prefix('admin')->group(function () {

    // =========== Admin Login Routes ==============//
    Route::middleware('guest:admin')->group(function () {
        Route::get('/login',"Admin\AuthController@loginShow")->name('admin.login');
        Route::post('/login',"Admin\AuthController@loginPost")->name('admin.login.store');

        Route::get('/forgotPassword',"Admin\AuthController@EmailResetPasswordPage")->name('forgotPassword.email'); // Forgot Password send email 

        Route::post('/AjaxCallForForgotPasswordEmail',"Admin\AuthController@AjaxCallForForgotPasswordEmail")->name('AjaxCallForForgotPasswordEmail'); // Forgot Password send email 

        Route::get('/resetPassword/{email_encode}',"Admin\AuthController@ResetPassword")->name('resetPassword'); // Forgot Password send email 

        Route::post('/SetNewPassword',"Admin\AuthController@SetNewPassword")->name('SetNewPassword'); // Set New Password 


    });
    // =========== Admin Authenticate Routes ==============//
    Route::middleware('auth:admin')->group(function () {
        
        Route::get('home',"Admin\HomeController@index")->name('home');
        Route::get('/logout',"Admin\AuthController@logout")->name('admin.logout');
        // =========== Admin CMS Routes ==============//
        Route::resource('news',"Admin\CMS\NewsController");
        Route::resource('widget',"Admin\CMS\WidgetController");
        // Route::resource('about-us',"Admin\CMS\AboutUsController");

        Route::post('about-us/store',"Admin\CMS\CMSController@AboutUsCreateOrUpdate")->name('about-us.store');

        Route::get('about-us/create',"Admin\CMS\CMSController@AboutUs")->name('about-us.create');


        Route::post('home/store',"Admin\CMS\CMSController@HomeCreateOrUpdate")->name('home.store');

        Route::get('home/create',"Admin\CMS\CMSController@Home")->name('home.create');

        Route::get('pricing/create',"Admin\CMS\CMSController@Pricing")->name('pricing.create');
        //Faq
        Route::group(['prefix' => 'faq'], function () {
            Route::get('add/{id?}', 'Admin\CMS\FaqController@add')->name('faq.add');
            Route::post('post', 'Admin\CMS\FaqController@post')->name('faq.post');
            Route::get('/', 'Admin\CMS\FaqController@index')->name('faq.view');
            Route::get('/delete/{id}', 'Admin\CMS\FaqController@delete')->name('faq.delete');
            Route::post('AjaxCallForSaveFaqMetaData','Admin\CMS\CMSController@AjaxCallForSaveFaqMetaData')->name('AjaxCallForSaveFaqMetaData');

        });

        Route::post('pricing/store',"Admin\CMS\CMSController@PricingCreateOrUpdate")->name('pricing.store');

        Route::get('antiMoneyLaundring/create',"Admin\CMS\CMSController@AntiMoneyLaundring")->name('antiMoneyLaundring.create');
        Route::post('antiMoneyLaundring/store',"Admin\CMS\CMSController@AntiMoneyLaundringCreateOrUpdate")->name('antiMoneyLaundring.store');

        Route::get('privacyPolicy/create',"Admin\CMS\CMSController@PrivacyPolicy")->name('privacyPolicy.create');
        Route::post('privacyPolicy/store',"Admin\CMS\CMSController@PrivacyPolicyCreateOrUpdate")->name('privacyPolicy.store');

        
        Route::get('termsAndServices/create',"Admin\CMS\CMSController@TermsAndServices")->name('termsAndServices.create');
        Route::post('termsAndServices/store',"Admin\CMS\CMSController@TermsAndServicesCreateOrUpdate")->name('termsAndServices.store');

        Route::get('setting','Admin\SettitngController@index')->name('setting');
        Route::post('ChangePassword','Admin\SettitngController@ChangePassword')->name('ChangePassword');

        Route::get('contactUs',"Admin\CMS\CMSController@ContactUs")->name('contactUs.index');
        Route::post('AjaxCallForViewContactUs',"Admin\CMS\CMSController@AjaxCallForViewContactUs")->name('AjaxCallForViewContactUs');
        
        Route::post('ContactUsDelete',"Admin\CMS\CMSController@ContactUsDelete")->name('ContactU.delete');

        Route::post('AjaxCallForSaveContactUsMetaData','Admin\CMS\CMSController@AjaxCallForSaveContactUsMetaData')->name('AjaxCallForSaveContactUsMetaData');
        
    });
});


