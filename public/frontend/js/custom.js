$(function() {
  var $tabButtonItem = $('#tab-button li'),
      $tabSelect = $('#tab-select'),
      $tabContents = $('.tab-contents'),
      activeClass = 'is-active';

  $tabButtonItem.first().addClass(activeClass);
  $tabContents.not(':first').hide();

  $tabButtonItem.find('a').on('click', function(e) {
    var target = $(this).attr('href');

    $tabButtonItem.removeClass(activeClass);
    $(this).parent().addClass(activeClass);
    $tabSelect.val(target);
    $tabContents.hide();
    $(target).show();
    e.preventDefault();
  });

  $tabSelect.on('change', function() {
    var target = $(this).val(),
        targetSelectNum = $(this).prop('selectedIndex');

    $tabButtonItem.removeClass(activeClass);
    $tabButtonItem.eq(targetSelectNum).addClass(activeClass);
    $tabContents.hide();
    $(target).show();
  });
});

  $(document).ready(function(){
  $(".tabcontent h3").click(function(){
//     jQuery(this).next().find(".details").slideToggle("slow");
//     jQuery(".details").slideToggle("slow");
    $(this).next('p').slideToggle("slow");
  });
});

$(document).ready(function(){
    
    $('nav.navbar.navbar-expand-md.navbar-dark .nav-bar a').removeClass('active');
    
});


// $(document).ready(function(){
//     $('nav.navbar.navbar-expand-md.navbar-dark .nav-bar a').click(function(){
//     $('nav.navbar.navbar-expand-md.navbar-dark .nav-bar a').removeClass('activee');
//     $('nav.navbar.navbar-expand-md.navbar-dark .nav-bar a').addClass('active2e');
//     console.log("working");
// });
// });

$(document).ready(function() {
    $("[href]").each(function() {
        if (this.href == window.location.href) {
            $(this).removeClass("active2");
            $(this).addClass("active2");
        }
    });
});